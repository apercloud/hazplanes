<?php
pageAccessControl();

if(!empty($d["coordenadas"])) {
    $d["coordenadas"] = trim($d["coordenadas"],"\(\)");
    $coord = explode(",",$d["coordenadas"]);
} else {
    $coord = array("","");
}
?>
    <script>
        $(document).ready(function() {
            initView('<?php echo $coord[0]; ?>','<?php echo $coord[1]; ?>');
        });
    </script>
    <section id='content'>
        <div class="grid">
            <section id='info2'>
                <h2>
                    <?php echo $d["nombre"]; ?>
                    <span class="score right">
                        <?php
                        $stars = ceil(!empty($d["numvotos"])?$d["puntos"]/$d["numvotos"]:0);
                        for($a = 0; $a < $stars; $a++) {
                            echo '<img src="img/interface/starF-24.png" alt="" />';
                        }
                        for($b = 5; $b > $a; $b--) {
                            echo '<img src="img/interface/starE-24.png" alt="" />';
                        }
                        echo " (<span id='numvotos'>".(!empty($d["numvotos"])?$d["numvotos"]:0)."</span>)";
                        ?>
                    </span>
                </h2>
                <div id="info-izq">
                    <?php
                    $ampliar = "";
                    if(!empty($d["imagen"]) && file_exists($d["imagen"])) {
                        $ampliar = "<div class='ampliar'>+</div>";
                        echo "<div id='poster'>";
                        echo $ampliar;
                        echo "  <img src='".$d["imagen"]."' id='posterimg' itemprop='photo' alt='Foto del pabellón'/>";
                        echo "</div>";
                    } else {
                        $d["imagen"] = "img/interface/null-lugar.png";
                        echo "<div id='poster'>";
                        echo "  <img src='".$d["imagen"]."' id='posterimg' itemprop='photo' alt='Sin foto'/>";
                        echo "</div>";
                    }
                    ?>
                    <br>
                    <div id="map_canvas"></div>
                    <span class="nota">
                        <label for='form-coordenadas'>Coordenadas (Lat,Long)</label><br>
                        <div id="puntosPoly"><?php echo "(".$d["coordenadas"].")"; ?></div>
                    </span>
                    <h3>Puntuar sitio</h3>
                    <span id="vote" class="score">
                        <span style="display:none;" id="i"><?php echo $id; ?>-pabellones</span>
                        <?php
                        for($a = 1; $a < 6; $a++) {
                            echo '<img src="img/interface/starE-24.png" data-value="'.$a.'" alt="Votar con '.$a.' estrellas" />';
                        }
                        ?>
                    </span>
                    <?php if(!empty($d["menciones"])) { ?>
                        <h3>Créditos / Agradecimientos</h3>
                        <div id='info-credits'>
                            <p style='padding: 0 10px 0;'>
                                <?php echo htmlspecialchars_decode ($d["menciones"]); ?>
                            </p>
                        </div>
                    <?php
                    }
                    echo $publiFicha;
                    ?>
                    <span id='last-update'>
                        Última actualización:
                        <?php
                        if(!empty($d["actualizado"])){
                            echo $d["actualizado"];
                        } else {
                            echo $d["creado"];
                        }
                        ?>
                    </span>
                </div>
                <div id="info-der">
                    <h3>Ficha del pabellon <span class="ocultar">Ocultar</span></h3>
                    <div id='info-ficha'>
                        <?php
                        $direccion = "";
                        if(!empty($d["direccion"])) {
                            $direccion .= $d["direccion"];
                        }
                        if(!empty($d["codigopostal"])) {
                            if(!empty($direccion)) {
                                $direccion .= " - ";
                            }
                            $direccion .= $d["codigopostal"];
                        }
                        if(!empty($d["localidad"])) {
                            if(!empty($direccion)) {
                                if(!empty($d["codigopostal"])) {
                                    $direccion .= " ";
                                } else {
                                    $direccion .= " - ";
                                }
                            }
                            $direccion .= $d["localidad"];
                        }
                        if(!empty($d["provincia"])) {
                            if(!empty($direccion)) {
                                $direccion .= " (";
                            }
                            $direccion .= $d["provincia"].")";
                        }
                        if(!empty($direccion)) {
                            echo "<div id='direccion'><img src='img/interface/directions-24.png' alt='Dirección'>".$direccion."</div>";
                        }
                        if(!empty($d["telefono"])) {
                            echo "<div id='telefono'><img src='img/interface/phone-24.png' alt='Teléfono'>".$d["telefono"]."</div>";
                        }
                        if(!empty($d["email"])) {
                            echo "<div id='email'><img src='img/interface/email-26.png' alt='Email'>".$d["email"]."</div>";
                        }
                        if(!empty($d["web"])) {
                            echo "<div id='web'><img src='img/interface/link-24.png' alt='Web'><a href='".$d["web"]."' target='_blank'>".$d["web"]."</a></div>";
                        }
                        if(!empty($d["twitter"])) {
                            echo "<div id='twitter'><img src='img/interface/twitter-24.png' alt='Twitter'><a href='".$d["twitter"]."' target='_blank'>".$d["twitter"]."</a></div>";
                        }
                        if(!empty($d["facebook"])) {
                            echo "<div id='facebook'><img src='img/interface/facebook-24.png' alt='Facebook'><a href='".$d["facebook"]."' target='_blank'>".$d["facebook"]."</a></div>";
                        }
                        if(!empty($d["google"])) {
                            echo "<div id='google'><img src='img/interface/google_plus-24.png' alt='Google+'><a href='".$d["google"]."' target='_blank'>".$d["google"]."</a></div>";
                        }
                        if(!empty($d["otrasocial"])) {
                            echo "<div id='otrasocial'><img src='img/interface/link-24.png' alt='Otra red social'><a href='".$d["otrasocial"]."' target='_blank'>".$d["otrasocial"]."</a></div>";
                        }
                        if(!empty($d["notas"])) {
                            echo "<h3>Notas</h3><div id='notas'><p>";
                            echo $d["notas"]."</p></div>";
                        }
                        if(!empty($d["descripcion"])) {
                            echo "<h3>Descripción</h3><div id='descripcion'><p>";
                            echo $d["descripcion"]."</p></div>";
                        }
                        ?>
                    </div>
                    <h3>Próximos eventos <span class="ocultar">Ocultar</span></h3>
                    <div id='info-eventos-prox'>
                        <div class="grid">
                        <?php
                        lugarNextEvents($d, "pabellon", "idpabellon", false);
                        ?>
                        </div>
                    </div>
                    <h3>Eventos pasados (24 últimos) <span class="ocultar">Ocultar</span></h3>
                    <div id='info-eventos-pasados'>
                        <div class="grid">
                        <?php
                        lugarNextEvents($d, "pabellon", "idpabellon", true);
                        ?>
                        </div>
                    </div>
                <div>
            </section>
        </div>
    </section>
