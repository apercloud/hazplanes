<?php
pageAccessControl();

if(isset($_POST) && !empty($_POST)) {
    $asunto = "Nuevo evento: ".$_POST["titulo"];
    $mensaje = "";
	unset($_POST["enviar"]);
    foreach($_POST as $key => $data) {
        $mensaje .= "<b>".ucfirst($key).":</b> ".str_replace("\n","<br>",$data)."<br><br>";
    }
    $headers = "From: info@hazplanes.com\n";
    $headers .= "Reply-To: ".$_POST["email"]."\n";
    $headers .= "MIME-Version: 1.0\n";
    $headers .= "Content-Type: multipart/mixed; boundary=\"MIME_BOUNDRY\"\n";
    $headers .= "This is a multi-part message in MIME format.\n";

    if(isset($_FILES['poster']["tmp_name"]) && $_FILES['poster']["size"] > 0) {
        $fp = fopen(($_FILES['poster']['tmp_name']),"r");
        $str = fread($fp, filesize($_FILES['poster']['tmp_name']));
        $str = chunk_split(base64_encode($str));
        $fp = fclose($fp);
    }


    $message = "--MIME_BOUNDRY\n";
    $message .= "Content-Type: text/html; charset=\"utf-8\"\n";
    $message .= "Content-Transfer-Encoding: quoted-printable\n";
    $message .= "\n";
    $message .= "$mensaje";
    $message .= "\n";

    if(isset($str) && !empty($str)) {
        $message .= "--MIME_BOUNDRY\n";
        $message .= "Content-Type: image/jpeg; name=\"poster.jpg\"\n";
        $message .= "Content-disposition: attachment\n";
        $message .= "Content-Transfer-Encoding: base64\n";
        $message .= "\n";
        $message .= "$str\n";
        $message .= "\n";
        $message .= "--MIME_BOUNDRY--\n";
    }

    if($err = mail($events_email, $asunto, $message, $headers)) {
        $msg = "Evento enviado!";
    } else {
        $msg = "Error al enviar el evento. Vuelva a intentarlo. ".$err;
    }
}
?>
    <section id='content'>
        <div class="grid">
            <section id="info" class="formulario">
                <h2><?php echo $page_title; ?></h2>
                <?php
                if(isset($msg)) {
                    echo "<h3>".$msg."</h3>";
                }
                ?>
                <div id="infotodo" style='text-align: center;'>
                    <p>Formulario de envío de eventos, para ser publicados después de pasar nuestro filtro personal. Deja tus datos junto con el mensaje para poder agradecertelo.</p>
                    <form id="form-contacto" action="upload" method="post" enctype="multipart/form-data" style='text-align: left;'>
                        <ul style='height: 700px;'>
                            <li class="field" style='margin-bottom: 0;'></li>
                            <li class="field">
                                <label class="field_label" for="form-name">Nombre</label>
                                <input type="text" name="name" id="form-name" class="field_input" required="required" placeholder="Como te llamas…" />
                            </li>
                            <li class="field">
                                <label class="field_label" for="form-email">Email</label>
                                <input type="text" name="email" id="form-email" class="field_input" required="required" placeholder="Tu dirección de correo…" />
                            </li>
                            <li class="field">
                                <label class="field_label" for="form-titulo">Evento</label>
                                <input type="text" name="titulo" id="form-titulo" class="field_input" required="required" placeholder="Nombre del evento…" />
                            </li>
                            <li class="field">
                                <label class="field_label" for="form-genero">Género</label>
                                <input type="text" name="genero" id="form-genero" class="field_input" required="required" placeholder="Tipo de evento…" />
                            </li>
                            <li class="field">
                                <label class="field_label" for="form-fecha">Fecha y hora (Formato: aaaa/mm/dd hh:mm)</label>
                                <input type="text" name="fecha" id="form-fecha" class="field_input" required="required" placeholder="Fecha y hora del evento…" />
                            </li>
                            <li class="field">
                                <label class="field_label" for="form-local">Lugar</label>
                                <input type="text" name="local" id="form-local" class="field_input" required="required" placeholder="Nombre del local/cine/lugar…" />
                            </li>
                            <li class="field">
                                <label class="field_label" for="form-direccion">Dirección</label>
                                <input type="text" name="direccion" id="form-direccion" class="field_input" required="required" placeholder="Donde se celebra…" />
                            </li>
                            <li class="field">
                                <label class="field_label" for="form-poster">Cartel</label>
                                <input type="file" name="poster" id="form-poster" class="field_input" style="top:40px; margin-top: -40px;position: relative;" placeholder="Imágen del evento…" />
                            </li>
                            <li class="field">
                                <label class="field_label" for="form-notas">Notas</label>
                                <textarea name="notas" id="form-notas" class="field_input" required="required" placeholder="Otros datos de interés…"></textarea>
                            </li>
                        </ul>
                        <br><br>
                        <p style='text-align: center;'>Todos los campos son obligatorios, salvo el cartel y otros datos de interés.</p>
                        <div class='botones'>
                            <button type="submit" name="enviar" class='boton'>Enviar</button>
                            <button type="reset" name="reset" class='boton'>Reset</button>
                        </div>
                    </form>
                </div>
            </section>
        </div>
    </section>
