<?php
pageAccessControl();

if(!$nofecha) {
    $displayTypeTitle = "style='display:none;'";
} else {
    $displayTypeTitle = "";
}
?>
    <section id='contentMap'>
            <script>
                $(document).ready(function() {
                    publicInitialize(13);
                    <?php
                    $tomorrow = 0;
                    if($filtroFecha == "+0" || $filtroFecha == date("d-m-Y", strtotime("now"))) {
                        if(date("G", strtotime("now")) < 6) {
                            $now = date("Y-m-d 0:i:s", strtotime("now -1 day"));
                            $fin = date("Y-m-d 05:59:59", strtotime("today -1 day"));
                            $options = array(
                                "filter" => "fecha between '".$now."' and '".$fin."' and publicado = 1",
                                "agendaMap" => 1
                                );
                        } else {
                            $now = date("Y-m-d H:i:s", strtotime("now"));
                            $fin = date("Y-m-d 23:59:59", strtotime("today"));
                            $options = array(
                                "filter" => "(((fecha between '".$now."' and '".$fin."') or (date(fecha) = date('".$now."') and (time(fecha) < '06:00:00' or todoeldia = 1)))) and publicado = 1",
                                "agendaMap" => 1
                                );
                        }
                        $general = new GeneralController($options,$db);
                        $data = $general->readAction();
                        if(count($data) < 1) {
                            $tomorrow = 1;
                            if(date("G", strtotime("now")) < 6) {
                                $now = date("Y-m-d H:i:s", strtotime("now"));
                                $fin = date("Y-m-d 23:59:59", strtotime("today"));
                            } else {
                                $now = date("Y-m-d 00:00:00", strtotime("today +1 day"));
                                $fin = date("Y-m-d 23:59:59", strtotime("today +1 day"));
                            }
                            $options = array(
                                "filter" => "(((fecha between '".$now."' and '".$fin."') or (date(fecha) = date('".$now."') and (time(fecha) < '06:00:00' or todoeldia = 1)))) and publicado = 1",
                                "agendaMap" => 1
                                );
                            $general = new GeneralController($options,$db);
                            $data = $general->readAction();
                        }
                    } else {
                        $ini = date("Y-m-d 00:00:00",strtotime($filtroFecha));
                        $fin = date("Y-m-d 23:59:59",strtotime($filtroFecha));
                        $options = array(
                            "filter" => "(((fecha between '".$ini."' and '".$fin."') or (date(fecha) = date('".$ini."') and (time(fecha) < '06:00:00' or todoeldia = 1)))) and publicado = 1",
                            "agendaMap" => 1
                            );
                        $general = new GeneralController($options,$db);
                        $data = $general->readAction();
                    }
                    $ids = array();
                    $temp = array();
                    foreach($data as $k => $d) {
                        if(date("G",strtotime($d["fecha"])) < 6 && (date("H:i:s",strtotime($d["fecha"])) != "00:00:00" || $d["tipoevento"] == "exposiciones" || $d["tipoevento"] == "peliculas")) {
                            array_push($temp, $d);
                            unset($data[$k]);
                        }
                    }
                    $data = array_merge($data, $temp);
                    foreach($data as $k => $d) {
                        if(in_array($d["nombre"],$ids)) {
                            continue;
                        }
                        $ids[] = $d["nombre"];
                        $img = "";
                        $type = $d["tipoevento"];
                        if($type == "deportes") {
                            $img = "img/deportes/tipos/mix.png";
                            if(!empty($d["genero"])) {
                                $options2 = array("read" => "datatipodeporte", "idtipodeporte" => $d["genero"]);
                                $tipodeporte = new Deportes($options2,$db);
                                $d2 = $tipodeporte->readAction();
                                if(!empty($d2)) {
                                    $d2 = $d2[0];
                                    if(!empty($d2["poster"])) {
                                        $img = "img/deportes/tipos/".$d2["poster"];
                                    }
                                    $d["genero"] = $d2["nombre"];
                                } else {
                                    $d["genero"] = "Sin especificar";
                                }
                            } else {
                                $d["genero"] = "Sin especificar";
                            }
                        }
                        if(!empty($d["poster"])) {
                            if(strpos($d["poster"],'/') !== false) {
                                $img = explode("/",$d["poster"]);
                                $img[4] = $img[3];
                                $img[3] = "thumbs";
                                $img = implode("/",$img);
                            }
                        }
                        $datetime1 = date_create($d["fecha"]);
                        if(date("G",strtotime($d["fecha"])) < 6) {
                            $datetime2 = date_create("now -1 day");
                        } else {
                            $datetime2 = date_create("now");
                        }
                        $interval = date_diff($datetime1, $datetime2);
                        $faltaFormat = '%Im';
                        if($interval->format('%h') > 0) {
                            $faltaFormat = '%hh %Im';
                        }
                        if($interval->format('%d') > 0) {
                            $faltaFormat = '%dd %hh %Im';
                        }
                        $imgnull = "";
                        switch($type) {
                            case "peliculas":
                                $class = "film";
                                $imgnull = "null-film.png";
                                $type = "PELICULA";
                                $link = "pelicula/";
                            break;
                            case "conciertos":
                                $class = "concert";
                                $imgnull = "null-music.png";
                                $type = "CONCIERTO";
                                $link = "concierto/";
                            break;
                            case "obrasteatro":
                                $class = "theater";
                                $imgnull = "null-theatre.png";
                                $type = "TEATRO";
                                $link = "obra/";
                            break;
                            case "eventos":
                                $class = "event";
                                $imgnull = "null-calendar.png";
                                $type = "EVENTO";
                                $link = "evento/";
                            break;
                            case "exposiciones":
                                $class = "expo";
                                $imgnull = "null-museum.png";
                                $type = "EXPOSICIÓN";
                                $d["hora"] = "En horario del museo";
                                $link = "exposicion/";
                            break;
                            case "deportes":
                                $class = "sport";
                                $imgnull = "null-sports.png";
                                $type = "DEPORTE";
                                $link = "competicion/";
                            break;
                            case "formacion":
                            case "cursos":
                                $class = "course";
                                $imgnull = "null-cursos.png";
                                $type = "FORMACIÓN";
                                $link = "curso/";
                            break;
                        }
                        if($type == "PELICULA") {
                            if($filtroFecha == "+0" || $filtroFecha == date("d-m-Y", strtotime("now"))) {
                                $d["hora"] = "Próx. sesión: ".date("H:i",strtotime($d["fecha"]))." (En: ".$interval->format($faltaFormat).")";
                            } else {
                                $d["hora"] = "Primera sesión: ".date("H:i",strtotime($d["fecha"]));
                            }
                        } elseif($type != "exposiciones") {
                            if(strtotime($d["fecha"]) > strtotime("now")) {
                                $d["hora"] = "Hora: ".date("H:i",strtotime($d["fecha"]))." (En: ".$interval->format($faltaFormat).")";
                            } else {
                                $d["hora"] = "Lleva: ".$interval->format($faltaFormat);
                            }
                        }
                        if(date("H:i",strtotime($d["fecha"])) == "00:00" && $type != "PELICULA") {
                            $d["hora"] = "Hora sin determinar";
                        }
                        if($d["todoeldia"] && $type != "PELICULA") {
                            if($d["hora"] == "Hora sin determinar") {
                                $d["hora"] = "Todo el día";
                            } else {
                                $d["hora"] .= " (Todo el día)";
                            }
                        }
                        if(empty($img) || !file_exists($img)) {
                            $img = "img/interface/".$imgnull;
                        }
                        $infoBox = "<div style='width:280px; height: 280px; position: relative;'><div class='".$display." ".$class."' style='position: absolute; background: rgba(58, 58, 58, 0.9) url(".$img.") no-repeat center'>";
                        $infostatus = "";
                        if($d["cancelado"]) {
                            $infoBox .= "<div class='ribbon-wrapper'><div class='ribbon-cancelado'>CANCELADO</div></div>";
                            $infostatus = "cancelado";
                        } else {
                           if($d["agotado"]) {
                               $infoBox .= "<div class='ribbon-wrapper'><div class='ribbon-agotado'>AGOTADO</div></div>";
                               $infostatus = "agotado";
                           } else {
                               if($d["infantil"]) {
                                   $infoBox .= "<div class='ribbon-wrapper'><div class='ribbon-infantil'>NIÑOS</div></div>";
                                   $infostatus = "infantil";
                                   if($d["reserva"]) {
                                       $infoBox .= "<div class='ribbon-wrapper-2'><div class='ribbon-reserva'>RESERVAR</div></div>";
                                   }
                               } else {
                                   if($d["reserva"]) {
                                       $infoBox .= "<div class='ribbon-wrapper'><div class='ribbon-reserva'>RESERVAR</div></div>";
                                       $infostatus = "reserva";
                                   }
                               }
                           }
                        }
                        $infoBox .= "        <div class='grid-info ".$infostatus."'><h2><a href='".$link.$d["id"]."-".urlAmigable($d["nombre"])."'>".$d["nombre"]."</a></h2><br><span class='genre'>".$d["genero"]."</span><br><span class='hour'>".$d["hora"]."</span><span class='type'>".$type."</span></div></div></div>";
                        $coord = explode(",",str_replace("(","",str_replace(")","",$d["coordenadas"])));
                        echo "setPublicPoints('".trim($coord[0])."','".trim($coord[1])."','".$type."','".$d["nombre"]."',\"".str_replace('"','\"',$infoBox)."\",'".str_replace('"','\"',$infostatus)."');\n";
                    }
                    ?>
                });
            </script>
            <script>
                function success(position) {
                    var s = document.querySelector('#status');
                    if (s.className == 'success') {
                        // not sure why we're hitting this twice in FF, I think it's to do with a cached result coming back
                        return;
                    }
                    s.innerHTML = "Encontrado";
                    s.className = 'success';
                    var latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                    var marker = new google.maps.Marker({
                        position: latlng,
                        map: map,
                        title:"Tu posición (con un margen de error de "+position.coords.accuracy+" metros)"
                    });
                }

                function error(msg) {
                    var s = document.querySelector('#status');
                    s.innerHTML = typeof msg == 'string' ? msg : "Fallo";
                    s.className = 'fail';
                }

                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(success, error);
                } else {
                    error('No soportado');
                }
            </script>
            <span id="status" style='display: none;'>Comprobando...</span>
            <div id="map_canvas" style='min-height: calc(100% - 35px);'></div>
    </section>
