<?php
pageAccessControl(1);

// Validate what page num to show in list
if(isset($_GET["pag"])) {
    $pag = $_GET["pag"];
} else {
    $pag = 0;
}

//Validate what order to apply to list
if(isset($_GET["order"])) {
    $orde = $_GET["order"];
    if(substr($orde,0,2) == "az") {
        $order = substr($orde,2).", nombre";
    } else {
        $order = substr($orde,2)." desc, nombre";
    }
} else {
    $order = "nombre";
    $orde = "";
}
$start = $pag * 50;
$options = array("limit" => 50, "start" => $start, "order" => $order);
$teatros = new Teatros($options,$db);
$data = $teatros->readAction();
$cont = count($data);
?>
    <section id='content'>
        <section id='datos'>
            <div class="header-list">
                <h2>Auditorios <span id="optTitle"><button type="button" onclick="location.href='index.php?p=adminformtheater'">Nuevo auditorio</button></span></h2>
                <div id="listOptions">
                    <button type="button" c='Teatros' id='publish'>Publicar</button>
                    <button type="button" c='Teatros' id='unpublish'>No publicar</button>
                    <button type="button" c='Teatros' id='delete'>Eliminar</button>
                </div>
            </div>
            <?php paginacion($pag,$cont,"adminlistteatro",$orde); ?>
            <input type="hidden" name="type" id="type" value="teatros"/>
            <table id="list" class="tabla">
                <thead>
                    <th class='listCheckCell'><input type="checkbox" name="checkall" id="" class="" title="Seleccionar/Deseleccionar todo"/></th>
                    <th class='listPublishedCell'><a href="index.php?p=adminlisttheater&pag=<?php echo $pag; ?>&order=<?php echo $orde == "azpublicado"?"zapublicado":"azpublicado"; ?>">Publicado</a></th>
                    <th><a href="index.php?p=adminlisttheater&pag=<?php echo $pag; ?>&order=<?php echo $orde == "aznombre"?"zanombre":"aznombre"; ?>">Nombre</a></th>
                    <th><a href="index.php?p=adminlisttheater&pag=<?php echo $pag; ?>&order=<?php echo $orde == "azdireccion"?"zadireccion":"azdireccion"; ?>">Dirección</a></th>
                    <th><a href="index.php?p=adminlisttheater&pag=<?php echo $pag; ?>&order=<?php echo $orde == "azlocalidad"?"zalocalidad":"azlocalidad"; ?>">Localidad</a></th>
                    <th><a href="index.php?p=adminlisttheater&pag=<?php echo $pag; ?>&order=<?php echo $orde == "aztipolocal"?"zatipolocal":"aztipolocal"; ?>">Tipo</a></th>
                    <th>Puntos</th>
                    <th><a href="index.php?p=adminlisttheater&pag=<?php echo $pag; ?>&order=<?php echo $orde == "azimagen"?"zaimagen":"azimagen"; ?>">Poster</a></th>
                    <th>Enlace</th>
                </thead>
                <tbody id="listrows">
                <?php
                foreach($data as $k => $d) {
                    $link = "auditorio/";
                    $enlace = BASE_URL.$link.$d["idteatro"]."-".urlAmigable($d["nombre"]);
                    echo "<tr class='row'>
                        <td><input type='checkbox' name='checkListItem' id='".$d["idteatro"]."' class='' title='Seleccionar/Deseleccionar'/></td>
                        <td class='listPublishedCell'>".($d["publicado"]?"Si":"No")."</td>
                        <td><a href='index.php?p=adminformtheater&i=".$d["idteatro"]."'>".$d["nombre"]."</a></td>
                        <td>".$d["direccion"]."</td>
                        <td>".$d["localidad"]."</td>
                        <td>".$d["tipolocal"]."</td>
                        <td>".number_format((!empty($d["numvotos"])?$d["puntos"]/$d["numvotos"]:0),2)."</td>
                        <td>".(empty($d["imagen"])?"No":"Si")."</td>
                        <td><a href='".$enlace."'>".$enlace."</a></td>
                    </tr>";
                }
                ?>
                </tbody>
            </table>
            <?php paginacion($pag,$cont,"adminlistteatro",$orde); ?>
        </section>
    </section>
