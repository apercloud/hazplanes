<?php
pageAccessControl();

$style = "info2";
if(isset($d["fanart"]) && !empty($d["fanart"])) {
    echo "<style>
        html {
            background: black url(".$d["fanart"].") no-repeat center center fixed;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }
    </style>";
	$style = "info";
}
?>
    <section id='content'>
        <div class="grid">
            <section id='<?php echo $style; ?>' itemscope itemtype="http://data-vocabulary.org/Event">
                <h2>
                    <?php
                    echo "<span itemprop='name'>".$d["nombre"]."</span><span itemprop='summary' style='display:none;'>".$d["nombre"]."</span>";
                    if($d["infantil"] == 1) {
                        echo "<span class='h2-infantil'>Para niños</span>";
                    }
                    ?>
                    <span itemprop="eventType" style='display:none;'>Evento</span>
                </h2>
                <div id="info-izq">
                    <?php
                    if(isset($d["reserva"]) && !empty($d["reserva"])) {
                        if(!empty($d["limitereserva"]) && $d["limitereserva"] > "2000-01-01") {
                            echo "<div id='reserva'>Reservar antes del ".date("d-m-Y H:i",strtotime($d["limitereserva"]))."</div>";
                        } else {
                            echo "<div id='reserva'>Reserva necesaria</div>";
                        }
                    }
                    $ampliar = "";
                    if(empty($d["poster"]) || !file_exists($d["poster"])) {
                        $d["poster"] = "img/interface/null-calendar.png";
                    } else {
                        $ampliar = "<div class='ampliar'>+</div>";
                    }
                    echo "<div id='poster'>";
                    echo $ampliar;
                    echo "  <img src='".$d["poster"]."' id='posterimg' itemprop='photo' alt='Poster del evento'/>";
                    echo "</div>";
                    echo "<br>";
                    if(!empty($d["menciones"])) { ?>
                        <h3>Créditos / Agradecimientos</h3>
                        <div id='info-credits'>
                            <p style='padding: 0 10px 0;'>
                                <?php echo htmlspecialchars_decode ($d["menciones"]); ?>
                            </p>
                        </div>
                    <?php
                    }
                    echo $publiFicha;
                    ?>
                    <span id='last-update'>
                        Última actualización:
                        <?php
                        if(!empty($d["actualizado"])){
                            echo $d["actualizado"];
                        } else {
                            echo $d["creado"];
                        }
                        ?>
                    </span>
                </div>
                <div id="info-der">
                    <h3>Ficha técnica <span class="ocultar">Ocultar</span></h3>
                    <div id='info-ficha'>
                        <?php if(!empty($d["tipo"])) { ?>
                        <div id='tipo'>
                            <h4>Tipo</h4>
                            <?php echo $d["tipo"]; ?>
                        </div>
                        <?php
                        }
                        if(!empty($d["web"])) {
                            echo "<div id='web'><h4>Página web</h4> <a href='".$d["web"]."' target='_blank'>".$d["web"]."</a></div>";
                        }
                        ?>
                        <div class='share-links'>
                            <!-- Place this tag where you want the share button to render. -->
                            <div class="g-plus" data-action="share" data-annotation="bubble" data-href="<?php echo $ogurl; ?>"></div>
                            <div class="fb-share-button" data-href="<?php echo $ogurl; ?>" data-type="button_count"></div>
                            <a href="https://twitter.com/share" class="twitter-share-button" data-url="<?php echo $ogurl; ?>"  data-via="HazPlanes" data-lang="es">Twittear</a>
                        </div>
                        <?php
                        if(!empty($d["descripcion"])) {
                        ?>
                        <h3>Descripción</h3>
                        <div id='descripcion'>
                            <?php echo "<p itemprop='description'>".htmlspecialchars_decode($d["descripcion"])."</p>"; ?>
                        </div>
                        <?php
                        }
                        if(!empty($d["notas"])) {
                        ?>
                        <h3>Notas</h3>
                        <div id='notas'>
                            <?php echo "<p>".htmlspecialchars_decode($d["notas"])."</p>"; ?>
                        </div>
                        <?php } ?>
                    </div>
                    <?php
                    infoHoras($d);
                    infoMapLugares($d);
                    ?>
                </div>
            </section>
        </div>
    </section>
