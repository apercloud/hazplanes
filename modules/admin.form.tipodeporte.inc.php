<?php
pageAccessControl(1);

try {
    $act = "";
    if (isset($_POST['a'])) { // Forms
        $action = $_POST['a'];
    } else {
        $action = NULL;
    }
    $cancel = "index.php?p=adminlisttiposdeporte";
    if(isset($action) && !empty($action)) {
        $newtipodeporte = $_POST;
        unset($newtipodeporte["submit"],$newtipodeporte["a"]);
        if($action == "a") {
            unset($newtipodeporte["idtipodeporte"]);
            $newtipodeporte["tipos"] = "create";
            $tipodeportes = new Deportes($newtipodeporte,$db);
            $result = $tipodeportes->createAction();
            if($result) {
                $newtipodeporte["idtipodeporte"] = $result;
                $id = $result;
            }
        } elseif($action == "m") {
            $id = $newtipodeporte["idtipodeporte"];
            $newtipodeporte["tipos"] = "update";
            $tipodeportes = new Deportes($newtipodeporte,$db);
            $result = $tipodeportes->updateAction();
        }
        $msg['success'] = true;
        $botonesTitulo = "";
    } else {
        $botonesTitulo = '<span id="optTitle"><button type="submit" name="submit" form="newtipodeporte">Guardar</button>
                    <button type="button" name="cancel" ng-click="cancel(\''.$cancel.'\')">Cancelar</button></span>';
        if(isset($id) && !empty($id)) {
            $options = array("read" => "datatipodeporte", "idtipodeporte" => $id);
            $tipodeporte = new Deportes($options,$db);
            $d = $tipodeporte->readAction();
            $d = $d[0];
            $act = "m";
        } else {
            $d = array(
                "idtipodeporte" => "",
                "nombre" => "",
                "poster" => "",
                );
            $act = "a";
        }
    }
} catch( Exception $e ) {
    //catch any exceptions and report the problem
    $msg = array();
    $msg['success'] = false;
    $msg['errormsg'] = $e->getMessage();
}
?>
    <section id='content' ng-app>
        <section id='datos' ng-controller="Controller">
            <?php
            if(isset($msg["errormsg"])) {
                echo $msg["errormsg"];
                exit;
            }
            ?>
            <div class="header-form">
                <?php
                if(isset($id)) {
                    echo "<h2>Modificar tipo ".$botonesTitulo."</h2>";
                } else {
                    echo "<h2>Nuevo tipo ".$botonesTitulo."</h2>";
                }
                ?>
            </div>
            <?php if(!isset($msg['success'])) { ?>
            <form name="newtipodeporte" id="newtipodeporte" class="form" action="index.php?p=adminformtipodeporte" method="post" enctype="multipart/form-data">
                <input type="hidden" name="a" value="<?php echo $act; ?>"/>
                <input type="hidden" name="idtipodeporte" value="<?php echo $d["idtipodeporte"]; ?>"/>
                <div class='divsmall'>
                    <label for="form-nombre">Nombre</label>
                    <input type="text" name="nombre" id="form-nombre" class="campo" required="required" value="<?php echo $d["nombre"]; ?>" />
                </div>
                <div class='divbig'>
                    <label for="form-poster">Poster</label>
                    <input type="text" name="poster" id="form-poster" class="campo" value='<?php echo $d["poster"]; ?>' />
                    <?php
                    if(!empty($d["poster"])) {
                        echo "<p><img src='img/deportes/tipos/".$d["poster"]."?".strtotime("now")."' alt='Imagen del deporte' style='max-width: 300px; max-height: 300px;'/></p>";
                    }
                    ?>
                </div>
            </form>
            <?php } else { ?>
            <div class="form">
                <?php
                if($action == "a") {
                    echo "<p>Tipo de deporte creado con éxito.</p>";
                } elseif($action == "m") {
                    echo "<p>Tipo de deporte modificado con éxito.</p>";
                }
                echo '<p><a href="index.php?p=adminformtipodeporte&i='.$id.'">Modificar</a></p>';
                ?>
                <p>
                    <a href="index.php?p=adminformtipodeporte">Crear uno nuevo</a>
                </p>
                <a href="<?php echo $cancel; ?>"><< Volver al listado</a>
            </div>
            <?php } ?>
        </section>
    </section>