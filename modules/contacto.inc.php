<?php
pageAccessControl();

if(isset($_POST) && !empty($_POST)) {
    // Para enviar un correo HTML mail, la cabecera Content-type debe fijarse
    $cabeceras  = 'MIME-Version: 1.0' . "\r\n";
    $cabeceras .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

    // Cabeceras adicionales
    $cabeceras .= 'To: HazPlanes <'.$contact_email.'>' . "\r\n";
    $cabeceras .= 'From: <'.$_POST["email"].'>' . "\r\n";

	if(isset($_POST["mensaje"])) {
		$_POST["mensaje"] = str_replace("\n","<br>",$_POST["mensaje"]);
	} else {
		$_POST["mensaje"] = "No hay mensaje";
	}

    if($err = mail($contact_email,"Formulario de contacto - ".$_POST["subject"],$_POST["mensaje"],$cabeceras)) {
        $msg = "Mensaje enviado!";
    } else {
        $msg = "Error al enviar el mensaje. Vuelva a intentarlo. ".$err;
    }
}
?>
    <section id='content'>
        <div class="grid">
            <section id="info" class="formulario">
                <h2><?php echo $page_title; ?></h2>
                <?php
                if(isset($msg)) {
                    echo "<h3>".$msg."</h3>";
                }
                ?>
                <div id="infotodo" style='text-align: center;'>
                    <p>Para ponerte en contacto con nosotros, puedes rellenar el formulario que hay a continuación o bien mandarnos un mensaje a traves de cualquiera de nuestras páginas en redes sociales…</p>
                    <p>
                        <a href='http://www.google.com/+HazplanesWeb' target='_blank'><img src='img/interface/logogp-29.png' alt='Logo Google+' width='29' height='29'/></a>
                        <a href='https://www.facebook.com/hazplanesweb' target='_blank'><img src='img/interface/logofb-29.png' alt='Logo Facebook' width='29' height='29'/></a>
                        <a href='https://twitter.com/HazPlanes' target='_blank'><img src='img/interface/logotw-29.png' alt='Logo Twitter' width='29' height='29'/></a>
                    </p>
                    <form id="form-contacto" action="index.php?p=contacto" method="post" style='text-align: left;'>
                        <ul>
                            <li class="field" style='margin-bottom: 0;'></li>
                            <li class="field">
                                <label class="field_label" for="form-email">Email</label>
                                <input type="text" name="email" id="form-email" class="field_input" required="required" placeholder="Dirección de correo…" />
                            </li>
                            <li class="field">
                                <label class="field_label" for="form-subject">Asunto</label>
                                <input type="text" name="subject" id="form-subject" class="field_input" required="required" placeholder="Asunto…" />
                            </li>
                            <li class="field">
                                <label class="field_label" for="form-msg">Mensaje</label>
                                <textarea name="mensaje" id="form-msg" class="field_input" required="required" placeholder="Mensaje a enviar…"></textarea>
                            </li>
                        </ul>
                        <br><br>
                        <div class='botones'>
                            <button type="submit" name="enviar" class='boton'>Enviar</button>
                            <button type="reset" name="reset" class='boton'>Reset</button>
                        </div>
                    </form>
                </div>
            </section>
        </div>
    </section>
