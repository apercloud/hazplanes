<?php
pageAccessControl(1);

$local2 = file_get_contents("temp/results.json");
$json = json_decode($local2, true);
?>
    <section id='content' ng-app>
        <div class="grid">
            <section id='datos' ng-controller="Controller">
                <h2><?php echo $page_title; ?></h2>
                <div style='background: rgba(58, 58, 58, 0.9); color: #f2f2f2; padding: 10px;'>
                    <button type="button" id="scraper_servinova_launch" ng-click="scraperLaunch('servinova')">Lanzar Servinova</button>
                    <?php
                    if(isset($json["servinova"]["last_update"])) {
                        echo tiempo_transcurrido($json["servinova"]["last_update"]);
                    }
                    ?>
                    <br><br>
                    <button type="button" id="scraper_yelmo_launch" ng-click="scraperLaunch('yelmo')">Lanzar Yelmo Cines</button>
                    <?php
                    if(isset($json["yelmo"]["last_update"])) {
                        echo tiempo_transcurrido($json["yelmo"]["last_update"]);
                    }
                    ?>
                    <br><br>
                    <button type="button" id="scraper_plazae_launch" ng-click="scraperLaunch('plazae')">Lanzar Plaza Elíptica Cines</button>
                    <?php
                    if(isset($json["plazae"]["last_update"])) {
                        echo tiempo_transcurrido($json["plazae"]["last_update"]);
                    }
                    ?>
                    <br><br>
                    <button type="button" id="scraper_granvia_launch" ng-click="scraperLaunch('granvia')">Lanzar Gran Vía Cines</button>
                    <?php
                    if(isset($json["granvia"]["last_update"])) {
                        echo tiempo_transcurrido($json["granvia"]["last_update"]);
                    }
                    ?>
                    <br><br>
                    <button type="button" id="scraper_multinorte_launch" ng-click="scraperLaunch('multinorte')">Lanzar Multicines Norte</button>
                    <?php
                    if(isset($json["multinorte"]["last_update"])) {
                        echo tiempo_transcurrido($json["multinorte"]["last_update"]);
                    }
                    ?>
                    <br><br>
                    <button type="button" id="scraper_yelmoestrenos_launch" ng-click="scraperLaunch('yelmoestrenos')">Lanzar Estrenos Yelmo</button>
                    <br><br>
                    <div id="result_scraperDIV" ng-bind-html="scraper_launch_result"></div>
                </div>
            </section>
        </div>
    </section>