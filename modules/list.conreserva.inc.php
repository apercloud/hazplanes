<?php
pageAccessControl();
?>
    <section id='content'>
        <div class="grid">
            <section id="main">
                <ul id="celdas">
                    <?php
                    $options = array(
                        "reserva" => 1,
                        "limit" => 0
                        );
                    $general = new GeneralController($options,$db);
                    $data = $general->readAction();
                    echo "<li class='linea fecha'>Próximos eventos con reserva previa</li>";
                    $publi = 0;
                    $countCells = 0;
                    $ids = array();
                    foreach($data as $k => $d) {
                        if($d["limitereserva"] != "0000-00-00 00:00:00") {
                            $data[$k]["fecha"] = $d["limitereserva"];
                        }
                    }
                    usort($data, function($a, $b) {
                        return strtotime($a['fecha'])>strtotime($b['fecha']);
                    });
                    foreach($data as $k => $d) {
                        $countCells++;
                        if($countCells == 9) {
                            echo $celdaPubli;
                            $publi++;
                        }
                        $ids[] = $d["nombre"];
                        $img = "";
                        $type = $d["tipoevento"];
                        if($type == "deportes") {
                            if(!empty($d["genero"])) {
                                $options2 = array("read" => "datatipodeporte", "idtipodeporte" => $d["genero"]);
                                $tipodeporte = new Deportes($options2,$db);
                                $d2 = $tipodeporte->readAction();
                                if(!empty($d2)) {
                                    $d2 = $d2[0];
                                    $d["genero"] = $d2["nombre"];
                                } else {
                                    $d["genero"] = "Sin especificar";
                                }
                            } else {
                                $d["genero"] = "Sin especificar";
                            }
                        }
                        $imgnull = "";
                        switch($type) {
                            case "peliculas":
                                $class = "film";
                                $imgnull = "null-film.png";
                                $type = "PELICULA";
                                $link = "pelicula/";
                                break;
                            case "conciertos":
                                $class = "concert";
                                $imgnull = "null-music.png";
                                $type = "CONCIERTO";
                                $link = "concierto/";
                                break;
                            case "obrasteatro":
                                $class = "theater";
                                $imgnull = "null-theatre.png";
                                $type = "TEATRO";
                                $link = "obra/";
                                break;
                            case "eventos":
                                $class = "event";
                                $imgnull = "null-calendar.png";
                                $type = "EVENTO";
                                $link = "evento/";
                                break;
                            case "exposiciones":
                                $class = "expo";
                                $imgnull = "null-museum.png";
                                $type = "EXPOSICIÓN";
                                $link = "exposicion/";
                                break;
                            case "deportes":
                                $class = "sport";
                                $imgnull = "null-sports.png";
                                $type = "DEPORTE";
                                $link = "competicion/";
                                break;
                            case "formacion":
                            case "cursos":
                                $class = "course";
                                $imgnull = "null-cursos.png";
                                $type = "FORMACIÓN";
                                $link = "curso/";
                                break;
                        }
                        if(empty($img) || !file_exists($img)) {
                            $img = "img/interface/".$imgnull;
                        }
                        echo "<li class='linea ".$class."'>";
                        $infostatus = "";
                        if($d["cancelado"]) {
                            echo "<div class='ribbon-wrapper'><div class='ribbon-cancelado'>CANCELADO</div></div>";
                            $infostatus = "cancelado";
                        } else {
                           if($d["agotado"]) {
                               echo "<div class='ribbon-wrapper'><div class='ribbon-agotado'>AGOTADO</div></div>";
                               $infostatus = "agotado";
                           } else {
                               if($d["infantil"]) {
                                   echo "<div class='ribbon-wrapper'><div class='ribbon-infantil'>NIÑOS</div></div>";
                                   $infostatus = "infantil";
                               }
                           }
                        }
                        if($d["limitereserva"] != "0000-00-00 00:00:00") {
                            $limite = date("d-m-Y H:i",strtotime($d["limitereserva"]));
                        } else {
                            $limite = date("d-m-Y H:i",strtotime($d["fecha"]));
                        }
                        echo "  <div class='grid-info ".$infostatus."'>
                                    <a href='".$link.$d["id"]."-".urlAmigable($d["nombre"])."'>".$d["nombre"]."</a>
                                    <br>
                                    <span class='genre'>".$d["genero"]."</span>
                                    <br>
                                    <span class='hour'>Limite ".$limite."</span>
                                    <span class='type'>".$type."</span>
                                </div>
                            </li>";
                    }
                    echo $celdaPubli;
                    ?>
                </ul>
            </section>
        </div>
    </section>
