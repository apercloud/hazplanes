<?php
pageAccessControl(1);

try {
    $act = "";
    if (isset($_POST['a'])) { // Forms
        $action = $_POST['a'];
    } else {
        $action = NULL;
    }
    $cancel = "index.php?p=adminlistdeportes";
    if(isset($action) && !empty($action)) {
        $newitem = $_POST;
        $id = formAction($newitem, $action, "iddeporte");
        $msg['success'] = true;
        $botonesTitulo = "";
    } else {
        $botonesTitulo = '<span id="optTitle"><button type="submit" name="submit" form="newdeporte">Guardar</button>
                    <button type="button" name="cancel" ng-click="cancel(\''.$cancel.'\')">Cancelar</button></span>';
        $tipo = array();
        $tipo = array_pad($tipo,38,"");
        if(isset($id) && !empty($id)) {
            $options = array("iddeporte" => $id);
            $deporte = new Deportes($options,$db);
            $d = $deporte->readAction();
            $d = $d[0];
            $act = "m";
            $d["publicado"] == 0? $d["publicado"] = "" : $d["publicado"] = "checked";
            $d["infantil"] == 0? $d["infantil"] = "" : $d["infantil"] = "checked";
            $d["reserva"] == 0? $d["reserva"] = "" : $d["reserva"] = "checked";
            //Convertimos los horarios de la BD en json para angularjs
            $horarios = horariosToJson($d);
        } else {
            $d = array(
                "iddeporte" => "",
                "nombre" => "",
                "tipo" => "",
                "web" => "",
                "descripcion" => "",
                "notas" => "",
                "coordenadas" => "",
                "imagen" => "",
                "menciones" => "",
                "publicado" => "checked",
                "infantil" => "",
                "reserva" => "",
                "limitereserva" => ""
                );
            $act = "a";
        }
    }
} catch( Exception $e ) {
    //catch any exceptions and report the problem
    $msg = array();
    $msg['success'] = false;
    $msg['errormsg'] = $e->getMessage();
}
?>
    <script>
        $(document).ready(function() {
            initialize();
        });
    </script>
    <section id='content' ng-app>
        <section id='datos' ng-controller="Controller">
            <?php
            if(isset($msg["errormsg"])) {
                echo $msg["errormsg"];
                exit;
            }
            ?>
            <div class="header-form">
                <?php
                if(isset($id)) {
                    echo "<h2>Modificar competición ".$botonesTitulo."</h2>";
                } else {
                    echo "<h2>Nuevo competición ".$botonesTitulo."</h2>";
                }
                ?>
            </div>
            <?php if(!isset($msg['success'])) { ?>
            <form name="newdeporte" id="newdeporte" class="form" action="index.php?p=adminformdeporte" method="post" enctype="multipart/form-data">
                <input type="hidden" name="a" value="<?php echo $act; ?>"/>
                <input type="hidden" name="iddeporte" value="<?php echo $d["iddeporte"]; ?>"/>
                <div class='divsmall'>
                    <label for="form-nombre">Nombre</label>
                    <input type="text" name="nombre" id="form-nombre" class="campo" required="required" value="<?php echo htmlspecialchars($d["nombre"]); ?>" />
                </div>
                <div class='divsmall'>
                    <label for="form-tipo">Tipo</label>
                    <select name="tipo" id="form-tipo" class="campo" >
                        <?php
                        if($d["tipo"] == 0) {
                            $selected = "selected";
                        } else {
                            $selected = "";
                        }
                        echo "<option ".$selected." value='0'>Sin especificar</option>";
                        $options = array("read" => "tiposdeporte");
                        $tiposdeportes = new Deportes($options,$db);
                        $data2 = $tiposdeportes->readAction();
                        foreach($data2 as $k2 => $d2) {
                            if($d["tipo"] == $d2["idtipodeporte"]) {
                                $selected = "selected";
                                $img = $d2["poster"];
                            } else {
                                $selected = "";
                            }
                            echo "<option ".$selected." value='".$d2["idtipodeporte"]."'>".$d2["nombre"]."</option>";
                        }
                        ?>
                    </select>
                </div>
                <div class='divsmall'>
                    <label for="form-web">Web</label>
                    <input type="text" name="web" id="form-web" class="campo" value="<?php echo $d["web"]; ?>" />
                </div>
                <div class='divbig'>
                    <div style='display: inline-block;'>
                        <label for="form-publicado">Publicado</label>
                        <span class="checkbox">
                            <input type="checkbox" id="form-publicado" name="publicado" <?php echo $d["publicado"]; ?> />
                            <label class="check" for="form-publicado"></label>
                        </span>
                    </div>
                    <div style='display: inline-block;'>
                        <label for="form-infantil">Infantil</label>
                        <span class="checkbox">
                            <input type="checkbox" id="form-infantil" name="infantil" <?php echo $d["infantil"]; ?> />
                            <label class="check" for="form-infantil"></label>
                        </span>
                    </div>
                    <div style='display: inline-block;'>
                        <label for="form-reserva">Reservar</label>
                        <span class="checkbox">
                            <input type="checkbox" id="form-reserva" name="reserva" <?php echo $d["reserva"]; ?> />
                            <label class="check" for="form-reserva"></label>
                        </span>
                        <input type='datetime' id='form-limitereserva' name='limitereserva' class="campo field-limitereserva" value="<?php echo $d["limitereserva"]; ?>" />
                    </div>
                </div>
                <div class='divbig'>
                    <label for="form-descripcion">Descripción</label>
                    <textarea name="descripcion" id="form-descripcion" class="campo"><?php echo $d["descripcion"]; ?></textarea>
                </div>
                <div class='divbig'>
                    <label for="form-notas">Notas</label>
                    <textarea name="notas" id="form-notas" class="campo"><?php echo $d["notas"]; ?></textarea>
                </div>
                <div class='divbig'>
                    <label for="form-menciones">Menciones / Agradecimientos / Créditos</label>
                    <textarea name="menciones" id="form-menciones" class="campo"><?php echo $d["menciones"]; ?></textarea>
                </div>
                <div class='divbig'>
                    <label for="form-posterURL">Poster desde URL</label>
                    <input type="url" name="posterURL" id="form-posterURL" class="campo" />
                    <input type="file" name="poster" style='display:none;' id="form-poster" class="campo" />
                    <button type="button" onclick='$("#form-poster").click();'>Subir</button>
                    <?php
                    if(!empty($d["poster"])) {
                        echo "<p style='margin: auto; text-align:center;'><img src='".$d["poster"]."?".strtotime("now")."' alt='Poster de la competición' style='max-width: 300px; max-height: 200px;'/><br><button type='button' name='delImg' onclick='deleteImage(this,".$d["iddeporte"].",\"".$d["poster"]."\",\"Deportes\")'>Eliminar</button></p>";
                    } else {
                        if(isset($img) && !empty($img)) {
                            echo "<p style='margin: auto; text-align:center;'><img src='img/deportes/tipos/".$img."?".strtotime("now")."' alt='Imagen de la competición' style='max-width: 300px; max-height: 200px;'/></p>";
                        }
                    }
                    ?>
                </div>
                <br>
                <?php
                formHorariosPabellones("deporte");
                echo "<br>";
                formHorariosLocales("deporte");
                echo "<br>";
                formHorariosAuditorios("deporte");
                echo "<br>";
                formHorariosCines("deporte");
                echo "<br>";
                formHorariosMuseos("deporte");
                echo "<br>";
                formHorariosLugares("deporte");
                ?>
            </form>
            <?php } else { ?>
            <div class="form">
                <?php
                if($action == "a") {
                    echo "<p>Competición creada con éxito.</p>";
                } elseif($action == "m") {
                    echo "<p>Competición modificada con éxito.</p>";
                }
                echo '<p><a href="index.php?p=adminformdeporte&i='.$id.'">Modificar</a></p>';
                ?>
                <p>
                    <a href="index.php?p=adminformdeporte">Crear una nueva</a>
                </p>
                <a href="<?php echo $cancel; ?>"><< Volver al listado</a>
            </div>
            <?php } ?>
        </section>
    </section>
    <script>
        lugares = <?php if(!empty($horarios["horasLugar"])) echo $horarios["horasLugar"]; else echo "[]"; ?>;
        locales = <?php if(!empty($horarios["horasLocal"])) echo $horarios["horasLocal"]; else echo "[]"; ?>;
        cines = <?php if(!empty($horarios["horasCine"])) echo $horarios["horasCine"]; else echo "[]"; ?>;
        auditorios = <?php if(!empty($horarios["horasAuditorio"])) echo $horarios["horasAuditorio"]; else echo "[]"; ?>;
        museos = <?php if(!empty($horarios["horasMuseo"])) echo $horarios["horasMuseo"]; else echo "[]"; ?>;
        pabellones = <?php if(!empty($horarios["horasPabellon"])) echo $horarios["horasPabellon"]; else echo "[]"; ?>;
    </script>