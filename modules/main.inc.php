<?php
pageAccessControl();

if(!$nofecha) {
    $displayTypeTitle = "style='display:none;'";
} else {
    $displayTypeTitle = "";
}
?>
    <section id='content' class='<?php echo $displayContent; ?>'>
        <div class="grid">
        	<section id="main">
	            <ul id="celdas">
	                <?php
	                $tomorrow = 0;
	                if($filtroFecha == "+0" || $filtroFecha == date("d-m-Y", strtotime("now"))) {
	                    if(date("G", strtotime("now")) < 6) {
	                        $now = date("Y-m-d 0:i:s", strtotime("now -1 day"));
	                        $fin = date("Y-m-d 05:59:59", strtotime("today -1 day"));
	                        $options = array(
	                            "filter" => "fecha between '".$now."' and '".$fin."' and publicado = 1",
	                            "agenda" => 1
	                            );
	                    } else {
	                        $now = date("Y-m-d H:i:s", strtotime("now"));
	                        $fin = date("Y-m-d 23:59:59", strtotime("today"));
	                        $options = array(
	                            "filter" => "(((fecha between '".$now."' and '".$fin."') or (date(fecha) = date('".$now."') and (time(fecha) < '06:00:00' or todoeldia = 1)))) and publicado = 1",
	                            "agenda" => 1
	                            );
	                    }
						$general = new GeneralController($options,$db);
	                    $data = $general->readAction();
	                    if(count($data) < 1) {
	                        $tomorrow = 1;
	                        if(date("G", strtotime("now")) < 6) {
	                            $now = date("Y-m-d H:i:s", strtotime("now"));
	                            $fin = date("Y-m-d 23:59:59", strtotime("today"));
	                        } else {
	                            $now = date("Y-m-d 00:00:00", strtotime("today +1 day"));
	                            $fin = date("Y-m-d 23:59:59", strtotime("today +1 day"));
	                        }
	                        $options = array(
	                            "filter" => "(((fecha between '".$now."' and '".$fin."') or (date(fecha) = date('".$now."') and (time(fecha) < '06:00:00' or todoeldia = 1)))) and publicado = 1",
	                            "agenda" => 1
	                            );
	                        $general = new GeneralController($options,$db);
	                        $data = $general->readAction();
	                        if(count($data) > 0) {
	                            if(date("G", strtotime("now")) < 6) {
	                                echo "<li class='".$display." fecha' ".($nofecha?"style='display:none;'":"").">Hoy, ".$dias[date("N",strtotime($now))-1]." ".date("d",strtotime($now))." de ".$meses[date("n",strtotime($now))-1]." de ".date("Y",strtotime($now))." (".date("d-m-Y",strtotime($now)).")</li>";
	                            } else {
	                                echo "<li class='".$display." fecha' ".($nofecha?"style='display:none;'":"").">Mañana (".date("d-m-Y",strtotime("+1 day")).")</li>";
	                            }
	                        } else {
	                            echo "<li class='".$display." noclick'>
	                                <div class='grid-info no-events'>
	                                    <br><br>No hay eventos para hoy ni para mañana.
	                                </div>
	                            </li>";
	                        }
	                    } else {
	                        echo "<li class='".$display." fecha' ".($nofecha?"style='display:none;'":"").">Hoy, ".$dias[date("N",strtotime($now))-1]." ".date("d",strtotime($now))." de ".$meses[date("n",strtotime($now))-1]." de ".date("Y",strtotime($now))." (".date("d-m-Y",strtotime($now)).")</li>";
	                    }
	                } else {
	                    $ini = date("Y-m-d 00:00:00",strtotime($filtroFecha));
	                    $fin = date("Y-m-d 23:59:59",strtotime($filtroFecha));
	                    $options = array(
	                        "filter" => "(((fecha between '".$ini."' and '".$fin."') or (date(fecha) = date('".$ini."') and (time(fecha) < '06:00:00' or todoeldia = 1)))) and publicado = 1",
	                        "agenda" => 1
	                        );
	                    $general = new GeneralController($options,$db);
	                    $data = $general->readAction();
	                    if(count($data) > 0) {
	                        if($filtroFecha == date("Y-m-d", strtotime("now"))) {
	                            $f = "Hoy";
	                        } elseif($filtroFecha == date("Y-m-d", strtotime("+1 day"))) {
	                            $f = "Mañana";
	                        } else {
	                            $f = date("d-m-Y", strtotime($filtroFecha));
	                        }
	                        $f = $dias[date("N",strtotime($filtroFecha))-1]." ".date("d",strtotime($filtroFecha))." de ".$meses[date("n",strtotime($filtroFecha))-1]." de ".date("Y",strtotime($filtroFecha))." (".$f.")";
	                        echo "<li class='".$display." fecha' ".($nofecha?"style='display:none;'":"").">".$f."</li>";
	                    } else {
	                        echo "<li class='".$display." noclick'>
	                            <div class='grid-info no-events'>
	                                <br><br>No hay eventos para este día.
	                            </div>
	                        </li>";
	                    }
	                }
	                $publi = 0;
	                $countCells = 1;
	                $ids = array();
	                //Cuadro con eventos con inscripción con fecha cercana y botón de ver más…
	                ?>
	                <li class="<?php echo $display; ?> conreserva" data-category='0conreserva'>
	                    <div><h2>¡Acuerdate de reservar!</h2></div>
	                    <ul>
	                    <?php
	                    $options2 = array(
	                        "reserva" => 1,
	                        "limit" => 0
	                        );
	                    $general2 = new GeneralController($options2,$db);
	                    $data2 = $general2->readAction();
	                    foreach($data2 as $k => $d2) {
	                        if($d2["limitereserva"] != "0000-00-00 00:00:00") {
	                            $data2[$k]["fecha"] = $d2["limitereserva"];
	                        }
	                    }
	                    usort($data2, function($a, $b) {
	                        return strtotime($a['fecha'])>strtotime($b['fecha']);
	                    });
	                    $contreserva = 1;
	                    foreach($data2 as $k => $d) {
	                        if(!$d["agotado"] && !$d["cancelado"]) {
	                            if($d["limitereserva"] != "0000-00-00 00:00:00") {
	                                $limite = date("d-m-Y",strtotime($d["limitereserva"]));
	                            } else {
	                                $limite = date("d-m-Y",strtotime($d["fecha"]));
	                            }
	                            switch($d["tipoevento"]) {
	                                case "peliculas":
	                                    $link = "pelicula/";
	                                break;
	                                case "conciertos":
	                                    $link = "concierto/";
	                                break;
	                                case "obrasteatro":
	                                    $link = "obra/";
	                                break;
	                                case "eventos":
	                                    $link = "evento/";
	                                break;
	                                case "exposiciones":
	                                    $link = "exposicion/";
	                                break;
	                                case "deportes":
	                                    $link = "competicion/";
	                                break;
	                                case "formacion":
	                                case "cursos":
	                                    $link = "curso/";
	                                break;
	                            }
	                            echo "<li>".$limite." - <a href='".$link.$d["id"]."-".urlAmigable($d["nombre"])."'>".$d["nombre"]."</a></li>";
	                            if($contreserva == 5) {
	                                break;
	                            }
	                            $contreserva++;
	                        }
	                    }
	                    ?>
	                    </ul>
	                    <div class="vermas"><a href='<?php echo BASE_URL; ?>conreserva'>Ver más…</a></div>
	                </li>
	                <?php
	                //Fin cuadro de eventos con reserva previa
	                $temp = array();
	                foreach($data as $k => $d) {
	                    if(date("G",strtotime($d["fecha"])) < 6 && (date("H:i:s",strtotime($d["fecha"])) != "00:00:00" || $d["tipoevento"] == "exposiciones" || $d["tipoevento"] == "peliculas")) {
	                        array_push($temp, $d);
	                        unset($data[$k]);
	                    }
	                }
	                $data = array_merge($data, $temp);
	                foreach($data as $k => $d) {
	                    if(in_array($d["nombre"],$ids)) {
	                        continue;
	                    }
	                    $countCells++;
	                    if($countCells == 3) {
	                        echo $celdaPubli;
	                        $publi++;
	                    }
	                    $ids[] = $d["nombre"];
	                    $img = "";
	                    $type = $d["tipoevento"];
	                    if($type == "deportes") {
	                        $img = "img/deportes/tipos/mix.png";
	                        if(!empty($d["genero"])) {
	                            $options2 = array("read" => "datatipodeporte", "idtipodeporte" => $d["genero"]);
	                            $tipodeporte = new Deportes($options2,$db);
	                            $d2 = $tipodeporte->readAction();
	                            if(!empty($d2)) {
	                                $d2 = $d2[0];
	                                if(!empty($d2["poster"])) {
	                                    $img = "img/deportes/tipos/".$d2["poster"];
	                                }
	                                $d["genero"] = $d2["nombre"];
	                            } else {
	                                $d["genero"] = "Sin especificar";
	                            }
	                        } else {
	                            $d["genero"] = "Sin especificar";
	                        }
	                    }
	                    if(!empty($d["poster"])) {
	                        if(strpos($d["poster"],'/') !== false) {
	                            $img = explode("/",$d["poster"]);
	                            $img[4] = $img[3];
	                            $img[3] = "thumbs";
	                            $img = implode("/",$img);
	                        }
	                    }
	                    $datetime1 = date_create($d["fecha"]);
	                    if(date("G",strtotime($d["fecha"])) < 6) {
	                        $datetime2 = date_create("now -1 day");
	                    } else {
	                        $datetime2 = date_create("now");
	                    }
	                    $interval = date_diff($datetime1, $datetime2);
	                    $faltaFormat = '%Im';
	                    if($interval->format('%h') > 0) {
	                        $faltaFormat = '%hh %Im';
	                    }
	                    if($interval->format('%d') > 0) {
	                        $faltaFormat = '%dd %hh %Im';
	                    }
	                    $imgnull = "";
	                    $hide = "";
	                    switch($type) {
	                        case "peliculas":
	                            $class = "film";
	                            $imgnull = "null-film.png";
	                            $type = "PELICULA";
	                            $link = "pelicula/";
								if(isset($_COOKIE["filter-film"]) && $_COOKIE["filter-film"] == 0) {
									$hide = " display:none;";
								}
	                        break;
	                        case "conciertos":
	                            $class = "concert";
	                            $imgnull = "null-music.png";
	                            $type = "CONCIERTO";
	                            $link = "concierto/";
								if(isset($_COOKIE["filter-concert"]) && $_COOKIE["filter-concert"] == 0) {
									$hide = " display:none;";
								}
	                        break;
	                        case "obrasteatro":
	                            $class = "theater";
	                            $imgnull = "null-theatre.png";
	                            $type = "TEATRO";
	                            $link = "obra/";
								if(isset($_COOKIE["filter-theater"]) && $_COOKIE["filter-theater"] == 0) {
									$hide = " display:none;";
								}
	                        break;
	                        case "eventos":
	                            $class = "event";
	                            $imgnull = "null-calendar.png";
	                            $type = "EVENTO";
	                            $link = "evento/";
								if(isset($_COOKIE["filter-event"]) && $_COOKIE["filter-event"] == 0) {
									$hide = " display:none;";
								}
	                        break;
	                        case "exposiciones":
	                            $class = "expo";
	                            $imgnull = "null-museum.png";
	                            $type = "EXPOSICIÓN";
	                            $d["hora"] = "En horario del museo";
	                            $link = "exposicion/";
								if(isset($_COOKIE["filter-expo"]) && $_COOKIE["filter-expo"] == 0) {
									$hide = " display:none;";
								}
	                        break;
	                        case "deportes":
	                            $class = "sport";
	                            $imgnull = "null-sports.png";
	                            $type = "DEPORTE";
	                            $link = "competicion/";
	                            if(isset($_COOKIE["filter-sport"]) && $_COOKIE["filter-sport"] == 0) {
									$hide = " display:none;";
								}
	                        break;
	                        case "formacion":
	                        case "cursos":
	                            $class = "course";
	                            $imgnull = "null-cursos.png";
	                            $type = "FORMACIÓN";
	                            $link = "curso/";
	                            if(isset($_COOKIE["filter-course"]) && $_COOKIE["filter-course"] == 0) {
									$hide = " display:none;";
								}
	                        break;
	                    }
	                    if($type == "PELICULA") {
	                        if($filtroFecha == "+0" || $filtroFecha == date("d-m-Y", strtotime("now"))) {
	                            $d["hora"] = "Próx. sesión: ".date("H:i",strtotime($d["fecha"]))." (En: ".$interval->format($faltaFormat).")";
	                        } else {
	                            $d["hora"] = "Primera sesión: ".date("H:i",strtotime($d["fecha"]));
	                        }
	                    } elseif($type != "EXPOSICIÓN") {
	                        if(strtotime($d["fecha"]) > strtotime("now")) {
	                            $d["hora"] = "Hora: ".date("H:i",strtotime($d["fecha"]))." (En: ".$interval->format($faltaFormat).")";
	                        } else {
	                            $d["hora"] = "Lleva: ".$interval->format($faltaFormat);
	                        }
	                    }
	                    if(date("H:i",strtotime($d["fecha"])) == "00:00" && $type != "PELICULA") {
	                        $d["hora"] = "Hora sin determinar";
	                    }
	                    if($d["todoeldia"] && $type != "PELICULA" && $type != "EXPOSICIÓN") {
	                        if($d["hora"] == "Hora sin determinar") {
	                            $d["hora"] = "Todo el día";
	                        } else {
	                            $d["hora"] .= " (Todo el día)";
	                        }
	                    }
	                    if(empty($img) || !file_exists($img)) {
	                        $img = "img/interface/".$imgnull;
	                    }
	                    echo "<li class='".$display." ".$class."' data-category='".$class."' style='background: url(".$img.") no-repeat center;".$hide."'>";
	                    $infostatus = "";
	                    if($d["cancelado"]) {
	                        echo "<div class='ribbon-wrapper'><div class='ribbon-cancelado'>CANCELADO</div></div>";
	                        $infostatus = "cancelado";
	                    } else {
	                       if($d["agotado"]) {
	                           echo "<div class='ribbon-wrapper'><div class='ribbon-agotado'>AGOTADO</div></div>";
	                           $infostatus = "agotado";
	                       } else {
	                           if($d["infantil"]) {
	                               echo "<div class='ribbon-wrapper'><div class='ribbon-infantil'>NIÑOS</div></div>";
	                               $infostatus = "infantil";
	                               if($d["reserva"]) {
	                                   echo "<div class='ribbon-wrapper-2'><div class='ribbon-reserva'>RESERVAR</div></div>";
	                               }
	                           } else {
	                               if($d["reserva"]) {
	                                   echo "<div class='ribbon-wrapper'><div class='ribbon-reserva'>RESERVAR</div></div>";
	                                   $infostatus = "reserva";
	                               }
	                           }
	                       }
	                    }
	                    echo "        <div class='grid-info ".$infostatus."'>
	                                <h2><a href='".$link.$d["id"]."-".urlAmigable($d["nombre"])."'>".$d["nombre"]."</a></h2>
	                                <br>
	                                <span class='genre'>".$d["genero"]."</span>
	                                <br>
	                                <span class='hour'>".$d["localidad"].". ".$d["hora"]." </span>
	                                <span class='type'>".$type."</span>
	                            </div>
	                        </li>";
	                }
	                if($publi < 2) {
	                    echo $celdaPubli;
	                    if($countCells < 2) {
	                        echo $celdaPubli;
	                    }
	                }
	                ?>
	                </ul>
	        	</div>
	    	</div>
        </div>
        <div class="grid">
        	<section id="main">
	            <ul id="celdas">
	                <?php
	                if($filtroFecha == "+0" || $filtroFecha == date("d-m-Y", strtotime("now"))) {
	                    if(date("G", strtotime("now")) < 6 && empty($tomorrow)) {
	                        $now = date("Y-m-d H:i:s", strtotime("now -1 day"));
	                        $ini = date("Y-m-d 06:00:00", strtotime("today -1 day"));
	                        $options = array(
	                            "filter" => "date(fecha) = date('".$now."') and not fecha between '".$now."' and '".$ini."' and publicado = 1",
	                            "order" => "time(fecha) desc, nombre",
	                            "agenda" => 1
	                            );
	                    } else {
	                        $now = date("Y-m-d H:i:s", strtotime("now"));
	                        $ini = date("Y-m-d 06:00:00", strtotime("today"));
	                        $options = array(
	                            "filter" => "fecha between '".$ini."' and '".$now."' and publicado = 1",
	                            "order" => "time(fecha) desc, nombre",
	                            "agenda" => 1
	                            );
	                    }
	                    $general = new GeneralController($options,$db);
	                    $data = $general->readAction();
	                    if(count($data) > 0) {
	                        echo "<li class='".$display." fecha' ".($nofecha?"style='display:none;'":"").">En proceso o ya terminados</li>";
	                        $countCells = 0;
	                        foreach($data as $k => $d) {
	                            if(in_array($d["nombre"],$ids) || $d["tipoevento"] == "exposiciones") {
	                                continue;
	                            }
	                            $ids[] = $d["nombre"];
	                            $countCells++;
	                            if($countCells == 2) {
	                                echo $celdaPubli;
	                            }
	                            $img = "";
	                            $type = $d["tipoevento"];
	                            if($type == "deportes") {
	                                $img = "img/deportes/tipos/mix.png";
	                                if(!empty($d["genero"])) {
	                                    $options2 = array("read" => "datatipodeporte", "idtipodeporte" => $d["genero"]);
	                                    $tipodeporte = new Deportes($options2,$db);
	                                    $d2 = $tipodeporte->readAction();
	                                    if(!empty($d2)) {
	                                        $d2 = $d2[0];
	                                        if(!empty($d2["poster"])) {
	                                            $img = "img/deportes/tipos/".$d2["poster"];
	                                        }
	                                        $d["genero"] = $d2["nombre"];
	                                    } else {
	                                        $d["genero"] = "Sin especificar";
	                                    }
	                                } else {
	                                    $d["genero"] = "Sin especificar";
	                                }
	                            }
	                            if(!empty($d["poster"])) {
	                                if(strpos($d["poster"],'/') !== false) {
	                                    $img = explode("/",$d["poster"]);
	                                    $img[4] = $img[3];
	                                    $img[3] = "thumbs";
	                                    $img = implode("/",$img);
	                                }
	                            }
	                            switch($type) {
	                                case "peliculas":
	                                    $class = "film";
	                                    $imgnull = "null-film.png";
	                                    $type = "PELÍCULA";
	                                    $link = "pelicula/";
	                                    break;
	                                case "conciertos":
	                                    $class = "concert";
	                                    $imgnull = "null-music.png";
	                                    $type = "CONCIERTO";
	                                    $link = "concierto/";
	                                    break;
	                                case "obrasteatro":
	                                    $class = "theater";
	                                    $imgnull = "null-theatre.png";
	                                    $type = "TEATRO";
	                                    $link = "obra/";
	                                    break;
	                                case "eventos":
	                                    $class = "event";
	                                    $imgnull = "null-calendar.png";
	                                    $type = "EVENTO";
	                                    $link = "evento/";
	                                    break;
	                                case "expos":
	                                    $class = "expo";
	                                    $imgnull = "null-expos.png";
	                                    $type = "EXPOSICIÓN";
	                                    $link = "exposicion/";
	                                    break;
	                                case "deportes":
	                                    $class = "sport";
	                                    $imgnull = "null-sports.png";
	                                    $type = "DEPORTE";
	                                    $link = "competicion/";
	                                    break;
	                                case "formacion":
	                                    $class = "course";
	                                    $imgnull = "null-cursos.png";
	                                    $type = "FORMACIÓN";
	                                    $link = "curso/";
	                                    break;
	                            }
	                            if(empty($img) || !file_exists($img)) {
	                                $img = "img/interface/".$imgnull;
	                            }
	                            $datetime1 = date_create($d["fecha"]);
	                            $datetime2 = date_create("now");
	                            $interval = date_diff($datetime1, $datetime2);
	                            echo "<li class='".$display." ".$class."' data-category='".$class."' style='background: url(".$img.") no-repeat center'>";
	                            $infostatus = "";
	                            if($d["cancelado"]) {
	                                echo "<div class='ribbon-wrapper'><div class='ribbon-cancelado'>CANCELADO</div></div>";
	                                $infostatus = "cancelado";
	                            } else {
	                               if($d["agotado"]) {
	                                   echo "<div class='ribbon-wrapper'><div class='ribbon-agotado'>AGOTADO</div></div>";
	                                   $infostatus = "agotado";
	                               } else {
	                                   if($d["infantil"]) {
	                                       echo "<div class='ribbon-wrapper'><div class='ribbon-infantil'>NIÑOS</div></div>";
	                                       $infostatus = "infantil";
	                                       if($d["reserva"]) {
	                                           echo "<div class='ribbon-wrapper-2'><div class='ribbon-reserva'>RESERVAR</div></div>";
	                                       }
	                                   } else {
	                                       if($d["reserva"]) {
	                                           echo "<div class='ribbon-wrapper'><div class='ribbon-reserva'>RESERVAR</div></div>";
	                                           $infostatus = "reserva";
	                                       }
	                                   }
	                               }
	                            }
	                            $faltaFormat = '%Im';
	                            if($interval->format('%h') > 0) {
	                                $faltaFormat = '%hh %Im';
	                            }
	                            echo "        <div class='grid-info ".$infostatus."'>
	                                        <h2><a href='".$link.$d["id"]."-".urlAmigable($d["nombre"])."'>".$d["nombre"]."</a></h2>
	                                        <br>
	                                        <span class='genre'>".$d["genero"]."</span>
	                                        <br>
	                                        <span class='hour'>Lleva: ".$interval->format($faltaFormat);
	                                        if($d["todoeldia"]) {
	                                            echo " (Todo el día)";
	                                        }
	                                        echo "</span>
	                                        <span class='type'>".$type."</span>
	                                    </div>
	                                </li>";
	                        }
	                    }
	                }
	                ?>
	            </ul>
	    	</div>
        </div>
    </section>
