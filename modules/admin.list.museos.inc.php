<?php
pageAccessControl(1);

// Validate what page num to show in list
if(isset($_GET["pag"])) {
    $pag = $_GET["pag"];
} else {
    $pag = 0;
}

//Validate what order to apply to list
if(isset($_GET["order"])) {
    $orde = $_GET["order"];
    if(substr($orde,0,2) == "az") {
        $order = substr($orde,2).", nombre";
    } else {
        $order = substr($orde,2)." desc, nombre";
    }
} else {
    $order = "nombre";
    $orde = "";
}
$start = $pag * 50;
$options = array("limit" => 50, "start" => $start, "order" => $order);
$museos = new Museos($options,$db);
$data = $museos->readAction();
$cont = count($data);
?>
    <section id='content'>
        <section id='datos'>
            <div class="header-list">
                <h2>Museos <span id="optTitle"><button type="button" onclick="location.href='index.php?p=adminformmuseo'">Nuevo museo</button></span></h2>
                <div id="listOptions">
                    <button type="button" c='Museos' id='publish'>Publicar</button>
                    <button type="button" c='Museos' id='unpublish'>No publicar</button>
                    <button type="button" c='Museos' id='delete'>Eliminar</button>
                </div>
            </div>
            <?php paginacion($pag,$cont,"adminlistmuseos",$orde); ?>
            <input type="hidden" name="type" id="type" value="museos"/>
            <table id="list" class="tabla">
                <thead>
                    <th class='listCheckCell'><input type="checkbox" name="checkall" id="" class="" title="Seleccionar/Deseleccionar todo"/></th>
                    <th class='listPublishedCell'><a href="index.php?p=adminlistmuseos&pag=<?php echo $pag; ?>&order=<?php echo $orde == "azpublicado"?"zapublicado":"azpublicado"; ?>">Publicado</a></th>
                    <th><a href="index.php?p=adminlistmuseos&pag=<?php echo $pag; ?>&order=<?php echo $orde == "aznombre"?"zanombre":"aznombre"; ?>">Nombre</a></th>
                    <th><a href="index.php?p=adminlistmuseos&pag=<?php echo $pag; ?>&order=<?php echo $orde == "azdireccion"?"zadireccion":"azdireccion"; ?>">Dirección</a></th>
                    <th><a href="index.php?p=adminlistmuseos&pag=<?php echo $pag; ?>&order=<?php echo $orde == "azlocalidad"?"zalocalidad":"azlocalidad"; ?>">Localidad</a></th>
                    <th><a href="index.php?p=adminlistmuseos&pag=<?php echo $pag; ?>&order=<?php echo $orde == "aztipolocal"?"zatipolocal":"aztipolocal"; ?>">Tipo</a></th>
                    <th>Puntos</th>
                    <th><a href="index.php?p=adminlistmuseos&pag=<?php echo $pag; ?>&order=<?php echo $orde == "azimagen"?"zaimagen":"azimagen"; ?>">Poster</a></th>
                    <th>Enlace</th>
                </thead>
                <tbody id="listrows">
                <?php
                foreach($data as $k => $d) {
                    $link = "museo/";
                    $enlace = BASE_URL.$link.$d["idmuseo"]."-".urlAmigable($d["nombre"]);
                    echo "<tr class='row'>
                        <td><input type='checkbox' name='checkListItem' id='".$d["idmuseo"]."' class='' title='Seleccionar/Deseleccionar'/></td>
                        <td class='listPublishedCell'>".($d["publicado"]?"Si":"No")."</td>
                        <td><a href='index.php?p=adminformmuseo&i=".$d["idmuseo"]."'>".$d["nombre"]."</a></td>
                        <td>".$d["direccion"]."</td>
                        <td>".$d["localidad"]."</td>
                        <td>".$d["tipolocal"]."</td>
                        <td>".number_format((!empty($d["numvotos"])?$d["puntos"]/$d["numvotos"]:0),2)."</td>
                        <td>".(empty($d["imagen"])?"No":"Si")."</td>
                        <td><a href='".$enlace."'>".$enlace."</a></td>
                    </tr>";
                }
                ?>
                </tbody>
            </table>
            <?php paginacion($pag,$cont,"adminlistmuseos",$orde); ?>
        </section>
    </section>
