<?php
pageAccessControl(1);

// Validate what page num to show in list
if(isset($_GET["pag"])) {
    $pag = $_GET["pag"];
} else {
    $pag = 0;
}

//Validate what order to apply to list
if(isset($_GET["order"])) {
    $orde = $_GET["order"];
    if(substr($orde,0,2) == "az") {
        $order = substr($orde,2).", fechaestreno desc";
    } else {
        $order = substr($orde,2)." desc, fechaestreno desc";
    }
} else {
    $order = "fechaestreno desc, nombre";
    $orde = "";
}
$start = $pag * 50;
$options = array("limit" => 50, "start" => $start, "order" => $order);
$peliculas = new Peliculas($options,$db);
$data = $peliculas->readAction();
$cont = count($data);
?>
    <section id='content'>
        <section id='datos'>
            <div class="header-list">
                <h2>Películas <span id="optTitle"><button type="button" onclick="location.href='index.php?p=adminformfilm'">Nueva película</button></span></h2>
                <div id="listOptions">
                    <button type="button" c='Peliculas' id='publish'>Publicar</button>
                    <button type="button" c='Peliculas' id='unpublish'>No publicar</button>
                    <button type="button" c='Peliculas' id='delete'>Eliminar</button>
                </div>
            </div>
            <?php paginacion($pag,$cont,"adminlistfilm",$orde); ?>
            <input type="hidden" name="type" id="type" value="peliculas"/>
            <table id="list" class="tabla">
                <thead>
                    <th class='listCheckCell'><input type="checkbox" name="checkall" id="" class="" title="Seleccionar/Deseleccionar todo"/></th>
                    <th class='listPublishedCell'><a href="index.php?p=adminlistfilm&pag=<?php echo $pag; ?>&order=<?php echo $orde == "azpublicado"?"zapublicado":"azpublicado"; ?>">Publicado</a></th>
                    <th><a href="index.php?p=adminlistfilm&pag=<?php echo $pag; ?>&order=<?php echo $orde == "aznombre"?"zanombre":"aznombre"; ?>">Nombre</a></th>
                    <th><a href="index.php?p=adminlistfilm&pag=<?php echo $pag; ?>&order=<?php echo $orde == "azgenero"?"zagenero":"azgenero"; ?>">Género</a></th>
                    <th class='col-fecha-list'><a href="index.php?p=adminlistfilm&pag=<?php echo $pag; ?>&order=<?php echo $orde == "azfechaestreno"?"zafechaestreno":"azfechaestreno"; ?>">Estreno</a></th>
                    <th><a href="index.php?p=adminlistfilm&pag=<?php echo $pag; ?>&order=<?php echo $orde == "azposter"?"zaposter":"azposter"; ?>">Poster</a></th>
                    <th>Enlace</th>
                </thead>
                <tbody id="listrows">
                <?php
                foreach($data as $k => $d) {
                    $link = "pelicula/";
                    $enlace = BASE_URL.$link.$d["idpelicula"]."-".urlAmigable($d["nombre"]);
                    echo "<tr class='row'>
                        <td><input type='checkbox' name='checkListItem' id='".$d["idpelicula"]."' class='' title='Seleccionar/Deseleccionar'/></td>
                        <td>".($d["publicado"]?"Si":"No")."</td>
                        <td><a href='index.php?p=adminformfilm&i=".$d["idpelicula"]."'>".$d["nombre"]." (".$d["original"].")</a></td>
                        <td>".$d["genero"]."</td>
                        <td>".(empty($d["fechaestreno"])?"":date("d-m-Y",strtotime($d["fechaestreno"])))."</td>
                        <td>".(empty($d["poster"])?"No":"Si")."</td>
                        <td><a href='".$enlace."'>".$enlace."</a></td>
                    </tr>";
                }
                ?>
                </tbody>
            </table>
            <?php paginacion($pag,$cont,"adminlistfilm",$orde); ?>
        </section>
    </section>
