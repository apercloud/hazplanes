<?php
/*
 * Scraper de Servinova
 * Modificado el 27-10-2014
 */

include_once("../includes/api.inc.php");
include_once("../includes/scrapers.inc.php");

date_default_timezone_set("Europe/Madrid");

$sleep = "2";
$mesesInv = array("enero" => "01",
        "febrero" => "02",
        "marzo" => "03",
        "abril" => "04",
        "mayo" => "05",
        "junio" => "06",
        "julio" => "07",
        "agosto" => "08",
        "septiembre" => "09",
        "octubre" => "10",
        "noviembre" => "11",
        "diciembre" => "12");
$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");

$enlaces = array();
$repeatMonths = array();
$n = date("n",strtotime("now"));
$repeatMonths[] = $meses[$n-1];
$year = date("Y",strtotime("now"));
if($n == 12) {
    $n = 0;
    $year++;
}
$repeatMonths[] = $meses[$n];
$repeatMonths[] = $meses[$n+1];
foreach($repeatMonths as $m) {
    $eventos = array();
    $page = 0;
    do {
        $page++;
        $url = "http://taquilla.servinova.com/ventaentradas/es/agenda?city=vigo&month=".strtolower($m)."&year=".$year."&module=servinova_agenda&fc=module&p=".$page;
        $output = file_get_contents_curl($url);

        preg_match_all('/itemprop=\"url\" href=\"(.*?)\" title=\"(.*?)\">/s', $output, $eventos);
        foreach($eventos[1] as $href) {
            $enlaces[] = $href;
        }
    } while(count($eventos[1]) >= 10); //Mientras tenga 10 eventos la pagina se seguirá a la siguiente página
}

$r = getAllUrlcompra("servinova");
$count = 0;
while($d = $db->fetch($r)) {
    $pos = array_search($d["urlcompra"], $enlaces);
    if($pos !== false) {
        unset($enlaces[$pos]);
        $count++;
    }
}
echo "<br>Total: ".($count + count($enlaces))." - Existen: ".$count." | Nuevos: ".count($enlaces)."<br>";

foreach($enlaces as $url) {
    $output = file_get_contents_curl($url);

    preg_match_all('/itemprop=\"name\">(.*?)<\/span>/s', $output, $name);
    preg_match_all('/itemprop=\"lowPrice\">(.*?)<\/strong>/s', $output, $price);
    preg_match_all('/time datetime=\"(.*?)\">/s', $output, $dateHead);
    preg_match_all('/itemprop=\"description\">(.*?)<\/div>/s', $output, $description);
    preg_match_all('/itemprop=\"image\" src=\"(.*?)\"\s*title=/s', $output, $image);
    preg_match_all('/dtstart\">(.*?)<\/span>/s', $output, $sessions);
    preg_match_all('/class=\"center\">\s*([0-9]{2}\.[0-9]{2})\s*/s', $output, $sessionsH);

    if(isset($dateHead[1][0])) {
        $fechaHead = str_replace("T"," ",$dateHead[1][0]);
    }
    $titulo = ucwords($name[1][0]);
    $lugar = $name[1][1];
    $precio = $price[1][0];
    $descrip = $description[1][0];
    $imagen = str_replace("-home/","-thickbox/",$image[1][0]);
    $titulo = normalize_title($titulo);
    $t = explode("/",$url);
    $tipo = $t[5];

    for($i = 0; $i < count($sessions[1]); $i++) {
        $existe = false;
        $fecha = $sessions[1][$i];
        $hora = $sessionsH[1][$i];
        if(isset($json_total[$tipo][$titulo][$lugar][strtotime($fecha)])) {
            foreach($json_total[$tipo][$titulo][$lugar][strtotime($fecha)] as $horaexistente) {
                if($horaexistente["hora"] == $hora) {
                    $existe = true;
                    break;
                }
            }
        }
        if(!$existe) {
            $json_total[$tipo][$titulo][$lugar][strtotime($fecha)][] = array(
                    "descripcion" => $descrip,
                    "hora" => $hora,
                    "url" => $url,
                    "image" => "http:".$imagen,
                    "precio" => $precio
                );
        }
    }
    sleep($sleep);
}

$json_total["servinova"]["last_update"] = date("Y-m-d H:i", strtotime("now"));
$json_total["servinova"]["events_new"] = count($enlaces);
$json_total["servinova"]["events_exists"] = $count;
$json_total["servinova"]["events_total"] = $count + count($enlaces);

$fp = fopen('../temp/servinova.json', 'w');
fwrite($fp, json_encode($json_total));
fclose($fp);
?>
<h1>Terminado!</h1>