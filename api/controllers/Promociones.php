<?php
/**
 * Clase controlador de promociones
 *
 * @package API
 * @author Trasno
 */
class Promociones {
    private $params;
    private $general;
    private $promocionItem;
    
    /**
     * Constructor
     *
     * @return void
     * @author trasno 
     */
    public function __construct($params, Db $db) {
        $this->params = $params;
        $this->general = new General($db);
        $this->promocionItem = new PromocionItem($db);
    }
    
    /**
     * Destructor
     *
     * @return void
     * @author trasno 
     */
    public function __destruct() {
        $this->promocionItem = null;
    }
    
    /**
     * Crear promocion
     *
     * @return int
     */
    public function createAction() {
        $promocion = array();
        foreach($this->params as $key => $value){
            $promocion[$key] = $value; 
        }
        $promocion["publicado"] = array_key_exists("publicado",$promocion)? 1:0;
        
        $result = $this->promocionItem->addPromocion($promocion);
        
        return $result;
    }
    
    /**
     * Recuperar promocion(s)
     *
     * @return array
     */
    public function readAction() {
        if(isset($this->params["idpromocion"]) && !empty($this->params["idpromocion"])) {
            $data = $this->promocionItem->dataPromocion($this->params["idpromocion"]);
        } else {
            if(isset($this->params["idcine"]) && !empty($this->params["idcine"])) {
                $data = $this->promocionItem->listPromociones($this->params["idcine"]);
            } else {
                $data = $this->promocionItem->listPromociones();
            }
        }
        
        return $data;
    }
    
    /**
     * Actualizar promocion
     *
     * @return boolean
     */
    public function updateAction() {
        if(isset($this->params["op"])) {
            if(strtolower($this->params["op"]) == "publish") {
                $result = $this->promocionItem->publishPromocion($this->params["ids"]);
            } elseif(strtolower($this->params["op"]) == "unpublish") {
                $result = $this->promocionItem->unpublishPromocion($this->params["ids"]);
            } else {
                $result = false;
            }
        } else {
            $promocion = array();
            foreach($this->params as $key => $value){
                $promocion[$key] = $value; 
            }
            $promocion["publicado"] = array_key_exists("publicado",$promocion)? 1:0;
            
            $result = $this->promocionItem->updatePromocion($promocion);
        }
        
        return $result;
    }
    
    /**
     * Eliminar promocion
     *
     * @return boolean
     */
    public function deleteAction() {
        if(isset($this->params["image"])) {
            $result = $this->general->deleteImage($this->params["id"], $this->params["image"]);
        } else {
            $result = $this->promocionItem->deletePromocion($this->params["id"]);
        }
        
        return $result;
    }
}