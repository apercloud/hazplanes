<?php
/**
 * Clase controlador de cursos
 *
 * @package API
 * @author Trasno
 */
class Cursos {
    private $params;
    private $general;
    private $cursosItem;

    /**
     * Constructor
     *
     * @return void
     * @author trasno
     */
    public function __construct($params, Db $db) {
        $this->params = $params;
        $this->general = new General($db);
        $this->cursosItem = new CursoItem($db);
    }

    /**
     * Destructor
     *
     * @return void
     * @author trasno
     */
    public function __destruct() {
        $this->cursosItem = null;
    }

    /**
     * Crear cursos
     *
     * @return int
     */
    public function createAction() {
        $cursos = array();
        foreach($this->params as $key => $value){
            $cursos[$key] = $value;
        }

        $horarios = array();
        $tipohorarios = array("locales","auditorios","cines","lugar","museos");
        foreach($tipohorarios as $t) {
            if(isset($cursos[$t])) {
                $horarios[$t] = $cursos[$t];
            }
            unset($cursos[$t]);
        }

        $cursos["publicado"] = array_key_exists("publicado",$cursos)? 1:0;
        $cursos["infantil"] = array_key_exists("infantil",$cursos)? 1:0;
        $cursos["reserva"] = array_key_exists("reserva",$cursos)? 1:0;

        $result = $this->cursosItem->addCursos($cursos);

        if($result) {
            //Insertar los horarios
            foreach($horarios as $tipo => $horario) {
                if(isset($horario[0][0]["fecha"])) {
                    switch($tipo) {
                        case "locales":
                            $idtipo = "idlocal";
                            $addtipo = "local";
                            break;
                        case "auditorios":
                            $idtipo = "idteatro";
                            $addtipo = "teatro";
                            break;
                        case "cines":
                            $idtipo = "idcine";
                            $addtipo = "cine";
                            break;
                        case "lugar":
                            $idtipo = "";
                            $addtipo = "lugar";
                            break;
                        case "museos":
                            $idtipo = "idmuseo";
                            $addtipo = "museo";
                            break;
                    }
                    foreach($horario as $key => $local) {
                        foreach($local as $k => $hora) {
                            if(isset($local[0][$idtipo])) {
                                $id = $local[0][$idtipo];
                            } else {
                                $id = "";
                            }
                            $pase = array(
                                "idlugar" => $id,
                                "idcurso" => $result,
                                "fecha" => $hora["fecha"],
                                "precio" => $hora["precio"],
                                "agotado" => array_key_exists("agotado",$hora)? 1:0,
                                "cancelado" => array_key_exists("cancelado",$hora)? 1:0,
                                "todoeldia" => array_key_exists("tododia",$hora)? 1:0
                                );
                            if($tipo == "cines") {
                                $pase["idsala"] = $hora["idsala"];
                                $pase["3d"] = array_key_exists("3d",$hora)? 1:0;
                                $pase["vo"] = array_key_exists("vo",$hora)? 1:0;
                            } else {
                                $pase["precioanticipada"] = $hora["precioanticipada"];
                            }
                            if($tipo == "lugar") {
                                $pase["coordenadas"] = $hora["coordenadas"];
                                $pase["lugar"] = $hora["lugar"];
                            } else {
                                $pase["urlcompra"] = $hora["urlcompra"];
                            }
                            $this->cursosItem->addHora($pase,$addtipo);
                        }
                    }
                }
            }
        }

        return $result;
    }

    /**
     * Recuperar curso(s)
     *
     * @return array
     */
    public function readAction() {
        if(isset($this->params["agenda"])) {
            $options = array(
                "limit" => 0,
                "start" => 0,
                "filter" => "fecha >= NOW()",
                "order" => "fecha, c.nombre",
                );
            $options = array_merge($options,$this->params);
            $data = $this->cursosItem->listCursos($options);
        } else {
            if(isset($this->params["idcurso"]) && !empty($this->params["idcurso"])) {
                $data = $this->cursosItem->dataCursos($this->params["idcurso"]);
                $horas = $this->cursosItem->listHoras($data[0]);
                $data[0]["horasLocal"] = $horas["horasLocal"];
                $data[0]["horasAuditorio"] = $horas["horasAuditorio"];
                $data[0]["horasCine"] = $horas["horasCine"];
                $data[0]["horasLugar"] = $horas["horasLugar"];
                $data[0]["horasMuseo"] = $horas["horasMuseo"];
            } else {
                $options = array(
                    "limit" => 0,
                    "start" => 0,
                    "filter" => "",
                    "order" => "fecha desc",
                    );
                if(!empty($this->params)) {
                    $options = array_merge($options,$this->params);
                }
                $data = $this->cursosItem->listCursos($options);
            }
        }

        return $data;
    }

    /**
     * Actualizar cursos
     *
     * @return boolean
     */
    public function updateAction() {
        if(isset($this->params["op"])) {
            //Publicar o no publicar
            if(strtolower($this->params["op"]) == "publish") {
                $result = $this->cursosItem->publishCursos($this->params["ids"]);
            } elseif(strtolower($this->params["op"]) == "unpublish") {
                $result = $this->cursosItem->unpublishCursos($this->params["ids"]);
            } else {
                $result = false;
            }
        } else {
            $cursos = array();
            foreach($this->params as $key => $value){
                $cursos[$key] = $value;
            }

            //Horarios
            $bdhorarios = $this->cursosItem->listHoras(array("idcurso" => $cursos["idcurso"]));
            $tipohorarios = array("horasLocal", "horasAuditorio", "horasCine", "horasLugar", "horasMuseo");
            foreach($tipohorarios as $th) {
                $bdhoras = $bdhorarios[$th];
                switch($th) {
                    case "horasLocal":
                        $formfield = "locales";
                        $idtipo = "idlocal";
                        $addtipo = "local";
                        break;
                    case "horasAuditorio":
                        $formfield = "auditorios";
                        $idtipo = "idteatro";
                        $addtipo = "teatro";
                        break;
                    case "horasCine":
                        $formfield = "cines";
                        $idtipo = "idcine";
                        $addtipo = "cine";
                        break;
                    case "horasLugar":
                        $formfield = "lugar";
                        $idtipo = "";
                        $addtipo = "lugar";
                        break;
                    case "horasMuseo":
                        $formfield = "museos";
                        $idtipo = "idmuseo";
                        $addtipo = "museo";
                        break;
                }
                if(isset($cursos[$formfield])) {
                    $horarios = $cursos[$formfield];
                } else {
                    $horarios = array();
                }

                foreach($horarios as $key => $local) {
                    foreach($local as $k => $hora) {
                        if(isset($local[0][$idtipo])) {
                            $id = $local[0][$idtipo];
                        } else {
                            $id = "";
                        }
                        if(isset($hora["fecha"])) {
                            $pase = array(
                                "idlugar" => $id,
                                "idcurso" => $cursos["idcurso"],
                                "fecha" => date("Y-m-d H:i",strtotime($hora["fecha"])),
                                "precio" => $hora["precio"],
                                "agotado" => array_key_exists("agotado",$hora)? 1:0,
                                "cancelado" => array_key_exists("cancelado",$hora)? 1:0,
                                "todoeldia" => array_key_exists("tododia",$hora)? 1:0
                                );
                            if($addtipo == "cine") {
                                $pase["idsala"] = $hora["idsala"];
                                $pase["3d"] = array_key_exists("3d",$hora)? 1:0;
                                $pase["vo"] = array_key_exists("vo",$hora)? 1:0;
                            } else {
                                $pase["precioanticipada"] = $hora["precioanticipada"];
                            }
                            if($addtipo == "lugar") {
                                $pase["coordenadas"] = $hora["coordenadas"];
                                $pase["lugar"] = $hora["lugar"];
                            } else {
                                $pase["urlcompra"] = $hora["urlcompra"];
                            }
                            $existe = false;
                            //Comprobamos si existe en la BD para modificarlo, en vez de darlo de alta de nuevo
                            foreach($bdhoras as $ke => $val) {
                                if(isset($val[$idtipo])) {
                                    if($val[$idtipo] == $pase["idlugar"] && $val["idcurso"] == $pase["idcurso"] && date("Y-m-d H:i",strtotime($val["fecha"])) == $pase["fecha"]) {
                                        $existe = true;
                                        unset($bdhoras[$ke]);
                                        break;
                                    }
                                } else {
                                    if($val["idcurso"] == $pase["idcurso"] && date("Y-m-d H:i",strtotime($val["fecha"])) == date("Y-m-d H:i",strtotime($pase["fecha"]))) {
                                        $existe = true;
                                        unset($bdhoras[$ke]);
                                        break;
                                    }
                                }
                            }
                            if($existe) {
                                $this->cursosItem->updateHora($pase,$addtipo);
                            } else {
                                $this->cursosItem->addHora($pase,$addtipo);
                            }
                        }
                    }
                }
                foreach($bdhoras as $delHora) {
                    if(isset($delHora[$idtipo])) {
                        $delHora["idlugar"] = $delHora[$idtipo];
                    }
                    $this->cursosItem->deleteHora($delHora,$addtipo);
                }
                unset($cursos[$formfield]);
            }

            $cursos["publicado"] = array_key_exists("publicado",$cursos)? 1:0;
            $cursos["infantil"] = array_key_exists("infantil",$cursos)? 1:0;
            $cursos["reserva"] = array_key_exists("reserva",$cursos)? 1:0;

            $result = $this->cursosItem->updateCursos($cursos);
        }

        return $result;
    }

    /**
     * Eliminar cursos
     *
     * @return boolean
     */
    public function deleteAction() {
        if(isset($this->params["image"])) {
            $result = $this->general->deleteImage($this->params["id"], $this->params["image"]);
        } else {
            $result = $this->cursosItem->deleteCursos($this->params["id"]);
        }

        return $result;
    }
}