<?php
/**
 * Clase controlador de servicios
 *
 * @package API
 * @author Trasno
 */
class Servicios {
    private $params;
    private $serviciosItem;
    
    /**
     * Constructor
     *
     * @return void
     * @author trasno 
     */
    public function __construct($params, Db $db) {
        $this->params = $params;
        $this->serviciosItem = new ServiciosItem($db);
    }
    
    /**
     * Destructor
     *
     * @return void
     * @author trasno 
     */
    public function __destruct() {
        $this->serviciosItem = null;
    }
    
    /**
     * Crear servicios
     *
     * @return int
     */
    public function createAction() {
        $servicios = array();
        foreach($this->params as $key => $value){
            $servicios[$key] = $value; 
        }
        
        $result = $this->serviciosItem->addServicios($servicios);
        
        return $result;
    }
    
    /**
     * Recuperar servicios(s)
     *
     * @return array
     */
    public function readAction() {
        if(isset($this->params["idcine"]) && !empty($this->params["idcine"])) {
            $data = $this->serviciosItem->dataServicios($this->params["idcine"]);
        } else {
            $data = $this->serviciosItem->listServicios();
        }
        
        return $data;
    }
    
    /**
     * Actualizar servicios
     *
     * @return boolean
     */
    public function updateAction() {
        $servicios = array();
        foreach($this->params as $key => $value){
            $servicios[$key] = $value; 
        }
        
        $result = $this->serviciosItem->updateServicios($servicios);
        
        return $result;
    }
    
    /**
     * Eliminar servicios
     *
     * @return boolean
     */
    public function deleteAction() {
        $result = $this->serviciosItem->deleteServicios($this->params["id"]);
        
        return $result;
    }
}