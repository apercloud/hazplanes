<?php
/**
 * Clase con metodos para operaciones con eventos
 *
 * @package API
 * @author Trasno
 */
class EventoItem {
    protected $db;

    /**
     * Constructor
     *
     * @return void
     * @author trasno
     */
    public function __construct(Db $db) {
        $this->db = $db;
    }

    /**
     * Listado de eventos
     *
     * @param array $options
     * @param int $options["limit"] (opcional) cuantos registros devolver
     * @param int $options["start"] (opcional) a partir de donde (posicion) se devuelve el listado
     * @param string $options["filter"] (opcional) cadena de filtro
     * @param string $options["order"] (opcional) campo por el que ordenar el listado.
     * @return array
     */
    public function listEventos($options) {
        $limit = "";
        if(!empty($options["limit"])) {
            $limit = "limit ".$options["start"].",".$options["limit"];
        }
        if(!empty($options["filter"])) {
            $options["filter"] = "where ".$options["filter"];
        }
        $query = sprintf(
        "select e.ideventos, e.nombre, le.nombre as lugar, tipo, poster, fecha, infantil, agotado, cancelado, todoeldia, publicado from %seventos e left join (
            select ideventos, l.nombre, fecha, agotado, cancelado, todoeldia from %slocales_has_eventos lhc left join %slocales l on l.idlocal = lhc.idlocal union
            select ideventos, t.nombre, fecha, agotado, cancelado, todoeldia from %steatros_has_eventos thc left join %steatros t on t.idteatro = thc.idteatro union
            select ideventos, c.nombre, fecha, agotado, cancelado, todoeldia from %scines_has_eventos chc left join %scines c on c.idcine = chc.idcine union
            select ideventos, lugar, fecha, agotado, cancelado, todoeldia from %slugares_has_eventos union
            select ideventos, m.nombre, fecha, agotado, cancelado, todoeldia from %smuseos_has_eventos mhc left join %smuseos m on m.idmuseo = mhc.idmuseo
        ) le on le.ideventos = e.ideventos %s group by date(fecha), e.nombre order by %s %s",
        BDPREFIX, BDPREFIX, BDPREFIX, BDPREFIX, BDPREFIX, BDPREFIX, BDPREFIX, BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"], $options["order"],$this->db->secure_field($limit));
        $r = $this->db->query($query);

        $result = array();
        while($evento = $this->db->fetch($r)) {
            $result[] = $evento;
        }

        return $result;
    }

    /**
     * Coge una evento y todos sus datos (locales, horarios, info...)
     *
     * @param int $ideventos id de el evento
     * @return array|false
     */
    public function dataEvento($idevento) {
        if(!empty($idevento)) {
            $query = sprintf("select c.* from %seventos c where c.ideventos = %d", BDPREFIX, $this->db->secure_field($idevento));
            $r = $this->db->query($query);
            if($this->db->count($r) > 0) {
                return array(0 => $this->db->fetch($r));
            } else {
                return false;
            }
        } else {
            throw new Exception("Parametro incorrecto", 1);
        }
    }

    /*
     * Eliminar una evento
     *
     * @param int $ideventos id de el evento
     * @return boolean
     */
    public function deleteEvento($idevento) {
        if(!empty($idevento)) {
            $query = sprintf("delete from %slocales_has_eventos where ideventos = %d",BDPREFIX, $this->db->secure_field($idevento));
            $r = $this->db->execute($query);
            $query = sprintf("delete from %steatros_has_eventos where ideventos = %d",BDPREFIX, $this->db->secure_field($idevento));
            $r = $this->db->execute($query);
            $query = sprintf("delete from %scines_has_eventos where ideventos = %d",BDPREFIX, $this->db->secure_field($idevento));
            $r = $this->db->execute($query);
            $query = sprintf("delete from %slugares_has_eventos where ideventos = %d",BDPREFIX, $this->db->secure_field($idevento));
            $r = $this->db->execute($query);
            $query = sprintf("delete from %smuseos_has_eventos where ideventos = %d",BDPREFIX, $this->db->secure_field($idevento));
            $r = $this->db->execute($query);
            if($r) {
                $query = sprintf("delete from %seventos where ideventos = %d",BDPREFIX, $this->db->secure_field($idevento));
                $r = $this->db->execute($query);
                deleteAllExtImages("img/eventos/poster/".$idevento);
                deleteAllExtImages("img/eventos/poster/thumbs/".$idevento);
                deleteAllExtImages("img/eventos/poster/original/".$idevento);
                deleteAllExtImages("img/eventos/fanart/".$idevento);
                return true;
            }
        }
        return false;
    }

    /*
     * Insertar una evento
     *
     * @param array $evento datos de el evento
     * @param string $evento['nombre'] nombre de el evento
     * @param int $evento['duracion'] duracion en minutos
     * @param string $evento['sinopsis']
     * @param string $evento['genero']
     * @param string $evento['video']
     * @param string $evento['poster']
     * @param string $evento['fanart']
     * @return int
     */
    public function addEvento($evento) {
        if(!$this->checkEvento("nombre", $evento["nombre"])) {
            $fields = "";
            $values = "";
            foreach($evento as $key => $value) {
                if(!empty($fields)) {
                    $fields = $fields.",";
                }
                $fields = $fields.$key;
                if(!empty($values)) {
                    $values = $values.",";
                }
                $values = $values."'".$this->db->secure_field($value)."'";
            }
            $query = sprintf("insert into %seventos (%s, creado) VALUES (%s, NOW())", BDPREFIX, $fields, $values);
            $r = $this->db->execute($query);
            if($r) {
                $id = $this->db->last_id();
                return $id;
            } else {
                throw new Exception("[addEvento] Error en la query: ".$query, 1);
            }
        } else {
            throw new Exception("[addEvento] Ya existe el evento.", 1);
        }
    }

    /*
     * Actualiza los campos de una evento
     *
     * @param array $evento datos de el evento
     * @param int $evento['idevento'] identificador de el evento
     * @param string $evento['nombre'] nombre de el evento
     * @param int $evento['duracion'] duracion en minutos
     * @param string $evento['sinopsis']
     * @param string $evento['genero']
     * @param string $evento['video']
     * @param string $evento['poster']
     * @param string $evento['fanart']
     * @return boolean
     */
    public function updateEvento($evento) {
        if($this->checkEvento("ideventos", $evento["ideventos"])) {
            $fields = "";
            foreach($evento as $key => $value) {
                if($key != "ideventos") {
                    if(!empty($fields)) {
                        $fields = $fields.",";
                    }
                    $fields = $fields.$key."='".$this->db->secure_field($value)."'";
                }
            }
            $query = sprintf("update %seventos set %s, actualizado = NOW() where ideventos = %d", BDPREFIX, $fields, $this->db->secure_field($evento["ideventos"]));
            $r = $this->db->execute($query);
            if($r) {
                return true;
            } else {
                throw new Exception("[updateEvento] Error en la query: ".$query, 1);
            }
        } else {
            throw new Exception("[updateEvento] No existe el evento.", 1);
        }
    }

    /*
     * Publicar eventos para que sean visibles
     *
     * @param int $ids ids de los eventos
     * @return boolean
     */
    public function publishEvento($ids) {
        $query = sprintf("update %seventos set publicado=1 where ideventos in (%s)", BDPREFIX, $this->db->secure_field($ids));
        $r = $this->db->execute($query);
        if($r) {
            return true;
        } else {
            throw new Exception("[publishEvento] Error en la query: ".$query, 1);
        }
    }

    /*
     * Despublicar eventos para que no sean visibles
     *
     * @param int $ids ids de los eventos
     * @return boolean
     */
    public function unpublishEvento($ids) {
        $query = sprintf("update %seventos set publicado=0 where ideventos in (%s)", BDPREFIX, $this->db->secure_field($ids));
        $r = $this->db->execute($query);
        if($r) {
            return true;
        } else {
            throw new Exception("[unpublishEvento] Error en la query: ".$query, 1);
        }
    }

    /*
     * Relacionar una evento con un local
     *
     * @param array $evento
     * @param int $evento['idlugar']
     * @param int $evento['ideventos']
     * @param date $evento['fecha']
     * @param string $evento['precio']
     * @param string $evento['precioadelantada']
     * @param int $evento['duracion'] solo si es en un lugar
     * @param string $evento['lugar'] solo si es en un lugar
     * @param string $evento['coordenadas'] solo si es en un lugar
     * @return boolean
     */
    public function addHora($evento, $type) {
        if(!$this->checkRelLugarEvento($evento,$type)) {
            switch($type){
                case "local":
                    $tabla = "locales_has_eventos";
                    $idlugar = "idlocal";
                    break;
                case "teatro":
                    $tabla = "teatros_has_eventos";
                    $idlugar = "idteatro";
                    break;
                case "cine":
                    $tabla = "cines_has_eventos";
                    $idlugar = "idcine";
                    break;
                case "lugar":
                    $tabla = "lugares_has_eventos";
                    unset($evento["idlugar"], $evento["duracion"]);
                    break;
                case "museo":
                    $tabla = "museos_has_eventos";
                    $idlugar = "idmuseo";
                    break;
            }
            $fields = "";
            $values = "";
            foreach($evento as $key => $value) {
                if(!empty($fields)) {
                    $fields = $fields.",";
                }
                if($key == "idlugar") {
                    $key = $idlugar;
                }
                $fields = $fields.$key;
                if(!empty($values)) {
                    $values = $values.",";
                }
                $values = $values."'".$this->db->secure_field($value)."'";
            }
            $query = sprintf("insert into %s (%s) VALUES (%s)", BDPREFIX.$tabla, $fields, $values);
            $r = $this->db->execute($query);
            if($r) {
                return true;
            } else {
                throw new Exception("[addHora] Error en la query: ".$query, 1);
            }
        } else {
            throw new Exception("[addHora] Ya existe la relacion.", 1);
        }
    }

    /*
     * Coger las horas de una evento (opcionalmente) con una sala y con un local
     *
     * @param array $evento
     * @param int $evento['idlugar']
     * @param int (opcional) $evento['ideventos']
     * @return array
     */
    public function listHoras($evento) {
        $result = array("horasLocal" => array(), "horasAuditorio" => array(), "horasCine" => array(), "horasLugar" => array(), "horasMuseo" => array());

        //Locales
        $query = sprintf("select lc.*, l.nombre, l.direccion, l.localidad, l.coordenadas from %slocales_has_eventos lc, %slocales l where lc.ideventos = %d and l.idlocal = lc.idlocal", BDPREFIX, BDPREFIX, $this->db->secure_field($evento["ideventos"]));
        if(isset($evento["idlugar"]) && !empty($evento["idlugar"])) {
            $query .= " and lc.idlocal = ".$this->db->secure_field($evento["idlugar"]);
        }
        $query .= " order by lc.idlocal, lc.fecha";
        $r = $this->db->query($query);
        if($r) {
            while($hora = $this->db->fetch($r)) {
                $result["horasLocal"][] = $hora;
            }
        } else {
            throw new Exception("[listHoras] Error en la query: ".$query, 1);
        }
        //Auditorios
        $query = sprintf("select tc.*, t.nombre, t.direccion, t.localidad, t.coordenadas from %steatros_has_eventos tc, %steatros t where tc.ideventos = %d and t.idteatro = tc.idteatro", BDPREFIX, BDPREFIX, $this->db->secure_field($evento["ideventos"]));
        if(isset($evento["idlugar"]) && !empty($evento["idlugar"])) {
            $query .= " and lc.idteatro = ".$this->db->secure_field($evento["idlugar"]);
        }
        $query .= " order by tc.idteatro, tc.fecha";
        $r = $this->db->query($query);
        if($r) {
            while($hora = $this->db->fetch($r)) {
                $result["horasAuditorio"][] = $hora;
            }
        } else {
            throw new Exception("[listHoras] Error en la query: ".$query, 1);
        }
        //Cines
        $query = sprintf("select cc.*, c.nombre, s.numero, c.direccion, c.localidad, c.coordenadas from %scines_has_eventos cc, %scines c, %ssalas s where cc.ideventos = %d and c.idcine = cc.idcine and s.idsala = cc.idsala", BDPREFIX, BDPREFIX, BDPREFIX, $this->db->secure_field($evento["ideventos"]));
        if(isset($evento["idsala"]) && !empty($evento["idsala"])) {
            $query .= " and cc.idsala = ".$this->db->secure_field($evento["idsala"]);
        }
        if(isset($evento["idlugar"]) && !empty($evento["idlugar"])) {
            $query .= " and cc.idcine = ".$this->db->secure_field($evento["idlugar"]);
        }
        $query .= " order by cc.idcine, cc.fecha";
        $r = $this->db->query($query);
        if($r) {
            while($hora = $this->db->fetch($r)) {
                $result["horasCine"][] = $hora;
            }
        } else {
            throw new Exception("[listHoras] Error en la query: ".$query, 1);
        }
        //Lugar
        $query = sprintf("select le.* from %slugares_has_eventos le where le.ideventos = %d order by le.fecha", BDPREFIX, $this->db->secure_field($evento["ideventos"]));
        $r = $this->db->query($query);
        if($r) {
            while($hora = $this->db->fetch($r)) {
                $result["horasLugar"][] = $hora;
            }
        } else {
            throw new Exception("[listHoras] Error en la query: ".$query, 1);
        }
        //Museos
        $query = sprintf("select mc.*, m.nombre, m.direccion, m.localidad, m.coordenadas from %smuseos_has_eventos mc, %smuseos m where mc.ideventos = %d and m.idmuseo = mc.idmuseo", BDPREFIX, BDPREFIX, $this->db->secure_field($evento["ideventos"]));
        if(isset($evento["idlugar"]) && !empty($evento["idlugar"])) {
            $query .= " and mc.idmuseo = ".$this->db->secure_field($evento["idlugar"]);
        }
        $query .= " order by mc.idmuseo, mc.fecha";
        $r = $this->db->query($query);
        if($r) {
            while($hora = $this->db->fetch($r)) {
                $result["horasMuseo"][] = $hora;
            }
        } else {
            throw new Exception("[listHoras] Error en la query: ".$query, 1);
        }

        return $result;
    }

    /*
     * Eliminar la relacion de una evento con un local
     *
     * @param array $evento
     * @param int $evento['idlugar']
     * @param int $evento['ideventos']
     * @param datetime $evento['fecha']
     * @param string $type especifica con que tipo de local se relaciona ese horario
     * @return boolean
     */
    public function deleteHora($evento,$type) {
        if($this->checkRelLugarEvento($evento,$type)) {
            switch($type){
                case "local":
                    $tabla = "locales_has_eventos";
                    $idlugar = "idlocal = ".$this->db->secure_field($evento["idlugar"]);
                    break;
                case "teatro":
                    $tabla = "teatros_has_eventos";
                    $idlugar = "idteatro = ".$this->db->secure_field($evento["idlugar"]);
                    break;
                case "cine":
                    $tabla = "cines_has_eventos";
                    $idlugar = "idcine = ".$this->db->secure_field($evento["idlugar"]);
                    break;
                case "lugar":
                    $tabla = "lugares_has_eventos";
                    $idlugar = "1";
                    break;
                case "museo":
                    $tabla = "museos_has_eventos";
                    $idlugar = "idmuseo = ".$this->db->secure_field($evento["idlugar"]);
                    break;
            }
            $query = sprintf("delete from %s where ideventos = %d and %s and fecha = '%s'", BDPREFIX.$tabla, $this->db->secure_field($evento["ideventos"]), $idlugar, $this->db->secure_field($evento["fecha"]));
            $r = $this->db->execute($query);
            if($r) {
                return true;
            } else {
                throw new Exception("[delHora] Error en la query: ".$query, 1);
            }
        } else {
            throw new Exception("[delHora] No existe la relacion.", 1);
        }
    }

    /*
     * Actualizar una relacion de una evento con un local/teatro
     *
     * @param array $evento
     * @param int $evento['idlugar']
     * @param int $evento['ideventos']
     * @param date $evento['fecha']
     * @param string $evento['precio']
     * @param string $evento['precioanticipada']
     * @param int $evento['duracion'] solo si es en un lugar
     * @param string $evento['lugar'] solo si es en un lugar
     * @param string $evento['coordenadas'] solo si es en un lugar
     * @param string $type especifica con que tipo de local se relaciona ese horario
     * @return boolean
     */
    public function updateHora($evento, $type) {
        if($this->checkRelLugarEvento($evento, $type)) {
            switch($type){
                case "local":
                    $tabla = "locales_has_eventos";
                    $idlugar = "idlocal = ".$this->db->secure_field($evento["idlugar"]);
                    break;
                case "teatro":
                    $tabla = "teatros_has_eventos";
                    $idlugar = "idteatro = ".$this->db->secure_field($evento["idlugar"]);
                    break;
                case "cine":
                    $tabla = "cines_has_eventos";
                    $idlugar = "idcine = ".$this->db->secure_field($evento["idlugar"]);
                    break;
                case "lugar":
                    $tabla = "lugares_has_eventos";
                    $idlugar = "1";
                    unset($evento["duracion"]);
                    break;
                case "museo":
                    $tabla = "museos_has_eventos";
                    $idlugar = "idmuseo = ".$this->db->secure_field($evento["idlugar"]);
                    break;
            }
            $fields = "";
            $exclude = array("idlugar","ideventos","fecha");
            foreach($evento as $key => $value) {
                if(!in_array($key, $exclude)) {
                    if(!empty($fields)) {
                        $fields = $fields.",";
                    }
                    $fields = $fields.$key."='".$this->db->secure_field($value)."'";
                }
            }
            $query = sprintf("update %s set %s where ideventos = %d and %s and fecha = '%s'", BDPREFIX.$tabla, $fields, $this->db->secure_field($evento["ideventos"]), $idlugar, $this->db->secure_field($evento["fecha"]));
            if(isset($evento["idsala"])) {
                $query .= " and idsala = ".$this->db->secure_field($evento["idsala"]);
            }
            $r = $this->db->execute($query);
            if($r) {
                return true;
            } else {
                throw new Exception("[updateHora] Error en la query: ".$query, 1);
            }
        } else {
            throw new Exception("[updateHora] Ya existe la relacion.", 1);
        }
    }

    /*
     * Comprueba si existe el evento
     *
     * @param string $field campo por el que comprobar
     * @param string $value valor que se va a comprobar en el campo
     * @return int|false
     */
    private function checkEvento($field, $value) {
        $query = sprintf("select c.* from %seventos c where c.%s = '%s'", BDPREFIX, $this->db->secure_field($field), $this->db->secure_field($value));
        $r = $this->db->query($query);

        if($this->db->count() > 0) {
            return $this->db->row(0);
        } else {
            return false;
        }
    }

    /*
     * Comprueba si existe el evento en un horario y local/teatro determinados
     *
     * @param array $evento
     * @param int $evento['idlugar']
     * @param int $evento['idevento']
     * @param date $evento['fecha']
     * @param string $type especifica con que tipo de local se relaciona ese horario
     * @return int|false
     */
    private function checkRelLugarEvento($evento, $type) {
        switch($type){
            case "local":
                $tabla = "locales_has_eventos";
                $idlugar = "idlocal = ".$this->db->secure_field($evento["idlugar"]);
                break;
            case "teatro":
                $tabla = "teatros_has_eventos";
                $idlugar = "idteatro = ".$this->db->secure_field($evento["idlugar"]);
                break;
            case "cine":
                $tabla = "cines_has_eventos";
                $idlugar = "idcine = ".$this->db->secure_field($evento["idlugar"]);
                break;
            case "lugar":
                $tabla = "lugares_has_eventos";
                $idlugar = "1";
                break;
            case "museo":
                $tabla = "museos_has_eventos";
                $idlugar = "idmuseo = ".$this->db->secure_field($evento["idlugar"]);
                break;
        }
        $query = sprintf("select * from %s where ideventos = %d and %s and fecha = '%s'", BDPREFIX.$tabla, $this->db->secure_field($evento["ideventos"]), $idlugar, $this->db->secure_field($evento["fecha"]));
        $r = $this->db->query($query);

        if($this->db->count() > 0) {
            return $this->db->row(0);
        } else {
            return false;
        }
    }
}
// END