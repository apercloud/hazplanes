<?php
/**
 * Clase con metodos para operaciones con exposiciones
 *
 * @package API
 * @author Trasno
 */
class ExposicionItem {
    protected $db;

    /**
     * Constructor
     *
     * @return void
     * @author trasno
     */
    public function __construct(Db $db) {
        $this->db = $db;
    }

    /**
     * Coge todas las exposiciones
     *
     * @param array $options
     * @param int $options["limit"] (opcional) cuantos registros devolver
     * @param int $options["start"] (opcional) a partir de donde (posicion) se devuelve el listado
     * @param string $options["filter"] (opcional) cadena de filtro
     * @param string $options["order"] (opcional) campo por el que ordenar el listado.
     * @return array
     */
    public function listExposiciones($options) {
        $limit = "";
        if(!empty($options["limit"])) {
            $limit = "limit ".$options["start"].",".$options["limit"];
        }
        if(!empty($options["filter"])) {
            $options["filter"] = "where ".$options["filter"];
        }
        $query = sprintf(
        "select e.idexposicion, nombre, genero, poster, me.inicio, me.fin, me.permanente, me.idmuseo, cerrado, infantil, agotado, cancelado, publicado from %sexposiciones e left join (
            select idexposicion, inicio, fin, permanente, idmuseo, h.cerrado, agotado, cancelado from %smuseos_has_exposiciones me, %shorarios h where h.idlugar=me.idmuseo and h.tipolugar='museo' union
             select idexposicion, inicio, fin, permanente, idlocal, '', agotado, cancelado from %slocales_has_exposiciones le union
             select idexposicion, inicio, fin, permanente, idteatro, '', agotado, cancelado from %steatros_has_exposiciones le union
             select idexposicion, inicio, fin, permanente, '', '', agotado, cancelado from %slugares_has_exposiciones le
            
        ) as me on me.idexposicion = e.idexposicion %s order by me.permanente, %s %s", 
        BDPREFIX, BDPREFIX, BDPREFIX, BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"], $options["order"],$this->db->secure_field($limit));
        $r = $this->db->query($query);

        $result = array();
        while($exposicion = $this->db->fetch($r)) {
            $result[] = $exposicion;
        }

        return $result;
    }

    /**
     * Coge una exposicion y todos sus datos (museos, finrios, info...)
     *
     * @param int $idexposicion id de la exposicion
     * @return array|false
     */
    public function dataExposicion($idexposicion) {
        if(!empty($idexposicion)) {
            $query = sprintf("select e.* from %sexposiciones e where e.idexposicion = %d", BDPREFIX, $this->db->secure_field($idexposicion));
            $r = $this->db->query($query);
            if($this->db->count($r) > 0) {
                return array(0 => $this->db->fetch($r));
            } else {
                return false;
            }
        } else {
            throw new Exception("Parametro incorrecto", 1);
        }
    }

    /*
     * Eliminar una exposicion
     *
     * @param int $idexposicion id de la exposicion
     * @return boolean
     */
    public function deleteExposicion($idexposicion) {
        if(!empty($idexposicion)) {
            $query = sprintf("delete from %smuseos_has_exposiciones where idexposicion = %d",BDPREFIX, $this->db->secure_field($idexposicion));
            $r = $this->db->execute($query);
            $query = sprintf("delete from %slocales_has_exposiciones where idexposicion = %d",BDPREFIX, $this->db->secure_field($idexposicion));
            $r = $this->db->execute($query);
            $query = sprintf("delete from %steatros_has_exposiciones where idexposicion = %d",BDPREFIX, $this->db->secure_field($idexposicion));
            $r = $this->db->execute($query);
            if($r) {
                $query = sprintf("delete from %sexposiciones where idexposicion = %d",BDPREFIX, $this->db->secure_field($idexposicion));
                $r = $this->db->execute($query);
                deleteAllExtImages("img/expos/poster/".$idexposicion);
                deleteAllExtImages("img/expos/poster/thumbs/".$idexposicion);
                deleteAllExtImages("img/expos/poster/original/".$idexposicion);
                deleteAllExtImages("img/expos/fanart/".$idexposicion);
                return true;
            }
        }
        return false;
    }

    /*
     * Insertar una exposicion
     *
     * @param array $exposicion datos de la exposicion
     * @param string $exposicion['nombre'] nombre de la exposicion
     * @param string $exposicion['descripcion']
     * @param string $exposicion['genero']
     * @param string $exposicion['poster']
     * @param string $exposicion['fanart']
     * @param string $exposicion['publicado']
     * @return int
     */
    public function addExposicion($exposicion) {
        if(!$this->checkExposicion("nombre", $exposicion["nombre"])) {
            $fields = "";
            $values = "";
            foreach($exposicion as $key => $value) {
                if(!empty($fields)) {
                    $fields = $fields.",";
                }
                $fields = $fields.$key;
                if(!empty($values)) {
                    $values = $values.",";
                }
                $values = $values."'".$this->db->secure_field($value)."'";
            }
            $query = sprintf("insert into %sexposiciones (%s, creado) VALUES (%s, NOW())", BDPREFIX, $fields, $values);
            $r = $this->db->execute($query);
            if($r) {
                $id = $this->db->last_id();
                return $id;
            } else {
                throw new Exception("[addExposicion] Error en la query: ".$query, 1);
            }
        } else {
            throw new Exception("[addExposicion] Ya existe la exposicion.", 1);
        }
    }

    /*
     * Actualiza los campos de una exposicion
     *
     * @param array $exposicion datos de la exposicion
     * @param int $exposicion['idexposicion'] identificador de la exposicion
     * @param string $exposicion['nombre'] nombre de la exposicion
     * @param string $exposicion['descripcion']
     * @param string $exposicion['genero']
     * @param string $exposicion['poster']
     * @param string $exposicion['fanart']
     * @param string $exposicion['publicado']
     * @return boolean
     */
    public function updateExposicion($exposicion) {
        if($this->checkExposicion("idexposicion", $exposicion["idexposicion"])) {
            $fields = "";
            foreach($exposicion as $key => $value) {
                if($key != "idexposicion") {
                    if(!empty($fields)) {
                        $fields = $fields.",";
                    }
                    $fields = $fields.$key."='".$this->db->secure_field($value)."'";
                }
            }
            $query = sprintf("update %sexposiciones set %s, actualizado = NOW() where idexposicion = %d", BDPREFIX, $fields, $this->db->secure_field($exposicion["idexposicion"]));
            $r = $this->db->execute($query);
            if($r) {
                return true;
            } else {
                throw new Exception("[updateExposicion] Error en la query: ".$query, 1);
            }
        } else {
            throw new Exception("[updateExposicion] No existe la exposicion.", 1);
        }
    }

    /*
     * Publicar exposiciones para que sean visibles
     *
     * @param int $ids ids de los exposiciones
     * @return boolean
     */
    public function publishExposicion($ids) {
        $query = sprintf("update %sexposiciones set publicado=1 where idexposicion in (%s)", BDPREFIX, $this->db->secure_field($ids));
        $r = $this->db->execute($query);
        if($r) {
            return true;
        } else {
            throw new Exception("[publishExposicion] Error en la query: ".$query, 1);
        }
    }

    /*
     * Despublicar exposiciones para que no sean visibles
     *
     * @param int $ids ids de los exposiciones
     * @return boolean
     */
    public function unpublishExposicion($ids) {
        $query = sprintf("update %sexposiciones set publicado=0 where idexposicion in (%s)", BDPREFIX, $this->db->secure_field($ids));
        $r = $this->db->execute($query);
        if($r) {
            return true;
        } else {
            throw new Exception("[unpublishExposicion] Error en la query: ".$query, 1);
        }
    }

    /*
     * Relacionar una exposicion con un museo
     *
     * @param array $exposicion
     * @param int $exposicion['idlugar']
     * @param int $exposicion['idexposicion']
     * @param date $exposicion['inicio']
     * @param time $exposicion['fin']
     * @param string $exposicion['precio']
     * @param string $exposicion['permanente']
     * @return boolean
     */
    public function addHora($exposicion, $type) {
        if(!$this->checkRelLugarExposicion($exposicion,$type)) {
            switch($type){
                case "museo":
                    $tabla = "museos_has_exposiciones";
                    $idlugar = "idmuseo";
                    break;
                case "local":
                    $tabla = "locales_has_exposiciones";
                    $idlugar = "idlocal";
                    break;
                case "teatro":
                    $tabla = "teatros_has_exposiciones";
                    $idlugar = "idteatro";
                    break;
                case "lugar":
                    $tabla = "lugares_has_exposiciones";
                    unset($exposicion["idlugar"]);
                    break;
            }
            $fields = "";
            $values = "";
            foreach($exposicion as $key => $value) {
                if(!empty($fields)) {
                    $fields = $fields.",";
                }
                if($key == "idlugar") {
                    $key = $idlugar;
                }
                $fields = $fields.$key;
                if(!empty($values)) {
                    $values = $values.",";
                }
                $values = $values."'".$this->db->secure_field($value)."'";
            }
            $query = sprintf("insert into %s (%s) VALUES (%s)", BDPREFIX.$tabla, $fields, $values);
            $r = $this->db->execute($query);
            if($r) {
                return true;
            } else {
                throw new Exception("[addHora] Error en la query: ".$query, 1);
            }
        } else {
            throw new Exception("[addHora] Ya existe la relacion.", 1);
        }
    }

    /*
     * Coger las horas de una exposicion (opcionalmente) con un museo
     *
     * @param array $exposicion
     * @param int $exposicion['idlugar']
     * @param int (opcional) $exposicion['idexposicion']
     * @return array
     */
    public function listHoras($exposicion) {
        $result = array("horasMuseo" => array(),"horasLocal" => array(), "horasAuditorio" => array(), "horasLugar" => array());

        //Museos
        $query = sprintf("select me.*, m.nombre, m.direccion, m.localidad, m.coordenadas from %smuseos_has_exposiciones me, %smuseos m where me.idexposicion = %d and m.idmuseo = me.idmuseo", BDPREFIX, BDPREFIX, $this->db->secure_field($exposicion["idexposicion"]));
        if(isset($exposicion["idmuseo"]) && !empty($exposicion["idmuseo"])) {
            $query .= " and me.idmuseo = ".$this->db->secure_field($exposicion["idlugar"]);
        }
        $query .= " order by me.idmuseo, me.inicio, me.fin";
        $r = $this->db->query($query);
        if($r) {
            while($fin = $this->db->fetch($r)) {
                $result["horasMuseo"][] = $fin;
            }
        } else {
            throw new Exception("[listHoras] Error en la query: ".$query, 1);
        }
        //Locales
        $query = sprintf("select lc.*, l.nombre, l.direccion, l.localidad, l.coordenadas from %slocales_has_exposiciones lc, %slocales l where lc.idexposicion = %d and l.idlocal = lc.idlocal", BDPREFIX, BDPREFIX, $this->db->secure_field($exposicion["idexposicion"]));
        if(isset($exposicion["idlugar"]) && !empty($exposicion["idlugar"])) {
            $query .= " and lc.idlocal = ".$this->db->secure_field($exposicion["idlugar"]);
        }
        $query .= " order by lc.idlocal, inicio, fin";
        $r = $this->db->query($query);
        if($r) {
            while($hora = $this->db->fetch($r)) {
                $result["horasLocal"][] = $hora;
            }
        } else {
            throw new Exception("[listHoras] Error en la query: ".$query, 1);
        }
        //Auditorios
        $query = sprintf("select tc.*, t.nombre, t.direccion, t.localidad, t.coordenadas from %steatros_has_exposiciones tc, %steatros t where tc.idexposicion = %d and t.idteatro = tc.idteatro", BDPREFIX, BDPREFIX, $this->db->secure_field($exposicion["idexposicion"]));
        if(isset($exposicion["idlugar"]) && !empty($exposicion["idlugar"])) {
            $query .= " and lc.idteatro = ".$this->db->secure_field($exposicion["idlugar"]);
        }
        $query .= " order by tc.idteatro, inicio, fin";
        $r = $this->db->query($query);
        if($r) {
            while($hora = $this->db->fetch($r)) {
                $result["horasAuditorio"][] = $hora;
            }
        } else {
            throw new Exception("[listHoras] Error en la query: ".$query, 1);
        }
        //Lugar
        $query = sprintf("select lc.* from %slugares_has_exposiciones lc where lc.idexposicion = %d order by inicio, fin", BDPREFIX, $this->db->secure_field($exposicion["idexposicion"]));
        $r = $this->db->query($query);
        if($r) {
            while($hora = $this->db->fetch($r)) {
                $result["horasLugar"][] = $hora;
            }
        } else {
            throw new Exception("[listHoras] Error en la query: ".$query, 1);
        }

        return $result;
    }

    /*
     * Eliminar la relacion de una exposicion con un museo
     *
     * @param array $exposicion
     * @param int $exposicion['idlugar']
     * @param int $exposicion['idexposicion']
     * @param date $exposicion['inicio']
     * @param time $exposicion['fin']
     * @param string $type especifica con que tipo de museo se relaciona ese finrio
     * @return boolean
     */
    public function deleteHora($exposicion,$type) {
        if($this->checkRelLugarExposicion($exposicion,$type)) {
            switch($type){
                case "museo":
                    $tabla = "museos_has_exposiciones";
                    $idlugar = "idmuseo = ".$this->db->secure_field($exposicion["idlugar"]);
                    break;
                case "local":
                    $tabla = "locales_has_exposiciones";
                    $idlugar = "idlocal = ".$this->db->secure_field($exposicion["idlugar"]);
                    break;
                case "teatro":
                    $tabla = "teatros_has_exposiciones";
                    $idlugar = "idteatro = ".$this->db->secure_field($exposicion["idlugar"]);
                    break;
                case "lugar":
                    $tabla = "lugares_has_exposiciones";
                    $idlugar = "1";
                    break;
            }
            $query = sprintf("delete from %s where idexposicion = %d and %s", BDPREFIX.$tabla, $this->db->secure_field($exposicion["idexposicion"]), $idlugar);
            $r = $this->db->execute($query);
            if($r) {
                return true;
            } else {
                throw new Exception("[delHora] Error en la query: ".$query, 1);
            }
        } else {
            throw new Exception("[delHora] No existe la relacion entre el lugar y la exposicion.", 1);
        }
    }

    /*
     * Actualizar una relacion de una exposicion con un museo
     *
     * @param array $exposicion
     * @param int $exposicion['idlugar']
     * @param int $exposicion['idexposicion']
     * @param date $exposicion['inicio']
     * @param time $exposicion['fin']
     * @param string $exposicion['precio']
     * @param string $exposicion['permanente']
     * @param string $type especifica con que tipo de museo se relaciona ese finrio
     * @return boolean
     */
    public function updateHora($exposicion, $type) {
        if($this->checkRelLugarExposicion($exposicion, $type)) {
            switch($type){
                case "museo":
                    $tabla = "museos_has_exposiciones";
                    $idlugar = "idmuseo = ".$this->db->secure_field($exposicion["idlugar"]);
                    break;
                case "local":
                    $tabla = "locales_has_exposiciones";
                    $idlugar = "idlocal = ".$this->db->secure_field($exposicion["idlugar"]);
                    break;
                case "teatro":
                    $tabla = "teatros_has_exposiciones";
                    $idlugar = "idteatro = ".$this->db->secure_field($exposicion["idlugar"]);
                    break;
                case "lugar":
                    $tabla = "lugares_has_exposiciones";
                    $idlugar = "1";
                    break;
            }
            $fields = "";
            $exclude = array("idlugar","idexposicion");
            foreach($exposicion as $key => $value) {
                if(!in_array($key, $exclude)) {
                    if(!empty($fields)) {
                        $fields = $fields.",";
                    }
                    $fields = $fields.$key."='".$this->db->secure_field($value)."'";
                }
            }
            $query = sprintf("update %s set %s where idexposicion = %d and %s", BDPREFIX.$tabla, $fields, $this->db->secure_field($exposicion["idexposicion"]), $idlugar);
            $r = $this->db->execute($query);
            if($r) {
                return true;
            } else {
                throw new Exception("[updateHora] Error en la query: ".$query, 1);
            }
        } else {
            throw new Exception("[updateHora] No existe la relacion.", 1);
        }
    }

    /*
     * Comprueba si existe la exposicion
     *
     * @param string $field campo por el que comprobar
     * @param string $value valor que se va a comprobar en el campo
     * @return int|false
     */
    private function checkExposicion($field, $value) {
        $query = sprintf("select e.* from %sexposiciones e where e.%s = '%s'", BDPREFIX, $this->db->secure_field($field), $this->db->secure_field($value));
        $r = $this->db->query($query);

        if($this->db->count() > 0) {
            return $this->db->row(0);
        } else {
            return false;
        }
    }

    /*
     * Comprueba si existe la exposicion en un horario y museo/teatro determinados
     *
     * @param array $exposicion
     * @param int $exposicion['idlugar']
     * @param int $exposicion['idexposicion']
     * @param date $exposicion['inicio']
     * @param time $exposicion['fin']
     * @param string $type especifica con que tipo de lugar se relaciona esa exposicion 
     * @return int|false
     */
    private function checkRelLugarExposicion($exposicion, $type) {
        switch($type){
            case "museo":
                $tabla = "museos_has_exposiciones";
                $idlugar = "idmuseo = ".$this->db->secure_field($exposicion["idlugar"]);
                break;
            case "local":
                $tabla = "locales_has_exposiciones";
                $idlugar = "idlocal = ".$this->db->secure_field($exposicion["idlugar"]);
                break;
            case "teatro":
                $tabla = "teatros_has_exposiciones";
                $idlugar = "idteatro = ".$this->db->secure_field($exposicion["idlugar"]);
                break;
            case "lugar":
                $tabla = "lugares_has_exposiciones";
                $idlugar = "1";
                break;
        }
        $query = sprintf("select * from %s where idexposicion = %d and %s", BDPREFIX.$tabla, $this->db->secure_field($exposicion["idexposicion"]), $idlugar);
        $r = $this->db->query($query);

        if($this->db->count() > 0) {
            return $this->db->row(0);
        } else {
            return false;
        }
    }
}
// END
