<?php
/**
 * Clase con metodos para operaciones con locales
 *
 * @package API
 * @author Trasno
 */
class LocalItem {
    protected $db;

    /**
     * Constructor
     *
     * @return void
     * @author trasno
     */
    public function __construct(Db $db) {
        $this->db = $db;
    }

    /**
     * Coge todos los locales
     *
     * @param array $options
     * @param int $options["limit"] (opcional) cuantos registros devolver
     * @param int $options["start"] (opcional) a partir de donde (posicion) se devuelve el listado
     * @param string $options["filter"] (opcional) cadena de filtro para nombre de pelicula.
     * @param string $options["order"] (opcional) campo por el que ordenar el listado.
     * @return array
     */
    public function listLocales($options) {
        $limit = "";
        if(!empty($options["limit"])) {
            $limit = "limit ".$options["start"].",".$options["limit"];
        }
        if(!empty($options["filter"])) {
            $options["filter"] = "where ".$options["filter"];
        }
        $query = sprintf("select l.* from %slocales l %s order by l.%s %s", BDPREFIX, $options["filter"], $this->db->secure_field($options["order"]), $this->db->secure_field($limit));
        $r = $this->db->query($query);

        $result = array();
        while($local = $this->db->fetch($r)) {
            $result[] = $local;
        }
        foreach($result as $k => $v) {
            $result[$k]["entradas"] = $this->getEntradas($local["idlocal"]);
        }
        return $result;
    }

    /**
     * Coge un local y todos sus datos (salas, servicios...)
     *
     * @param int $idlocal id del local
     * @return array|false
     */
     public function dataLocal($idlocal) {
        if(!empty($idlocal)) {
            $query = sprintf("select l.* from %slocales l where l.idlocal = %d", BDPREFIX, $this->db->secure_field($idlocal));
            $r = $this->db->query($query);
            if($this->db->count($r) > 0) {
                $local = $this->db->fetch($r);
                $local["entradas"] = $this->getEntradas($local["idlocal"]);
                return array(0 => $local);
            } else {
                return false;
            }
        } else {
            throw new Exception("Parametro incorrecto", 1);
        }
    }

    /*
     * Eliminar un local
     *
     * @param int $idlocal id del local
     * @return boolean
     */
    public function deleteLocal($idlocal) {
        if(!empty($idlocal)) {
            $query = sprintf("delete from %slocales where idlocal = %d",BDPREFIX, $this->db->secure_field($idlocal));
            $r = $this->db->execute($query);
            if($r) {
                deleteAllExtImages("img/locales/".$idlocal);
                deleteAllExtImages("img/locales/original/".$idlocal);
                return true;
            }
        }
        return false;
    }

    /*
     * Insertar un local
     *
     * @param array $local datos del local
     * @param string $local['nombre'] nombre del local
     * @param string $local['direccion']
     * @param string $local['localidad']
     * @param string $local['provincia']
     * @param int $local['codigopostal']
     * @param string $local['coordenadas']
     * @param string $local['telefono']
     * @param string $local['email']
     * @param string $local['web']
     * @param string $local['twitter']
     * @param string $local['facebook']
     * @param string $local['google']
     * @param string $local['otrasocial']
     * @param string $local['notas']
     * @param string $local['descripcion']
     * @param string $local['imagen']
     * @return int
     */
    public function addLocal($local) {
        if(!$this->checkLocal("nombre", $local["nombre"])) {
            $fields = "";
            $values = "";
            foreach($local as $key => $value) {
                if(!empty($fields)) {
                    $fields = $this->db->secure_field($fields).",";
                }
                $fields = $fields.$key;
                if(!empty($values)) {
                    $values = $values.",";
                }
                $values = $values."'".$this->db->secure_field($value)."'";
            }
            $query = sprintf("insert into %slocales (%s, creado) VALUES (%s, NOW())", BDPREFIX, $fields, $values);
            $r = $this->db->execute($query);
            if($r) {
                $id = $this->db->last_id();
                return $id;
            } else {
                throw new Exception("[addLocal] Error en la query: ".$query, 1);
            }
        } else {
            throw new Exception("[addLocal] Ya existe el local.", 1);
        }
    }

    /*
     * Actualiza los campos de un local
     *
     * @param array $local datos del local
     * @param int $local['idlocal'] identificador del local
     * @param string $local['nombre'] nombre del local
     * @param string $local['direccion']
     * @param string $local['localidad']
     * @param string $local['provincia']
     * @param int $local['codigopostal']
     * @param string $local['coordenadas']
     * @param string $local['telefono']
     * @param string $local['email']
     * @param string $local['web']
     * @param string $local['twitter']
     * @param string $local['facebook']
     * @param string $local['google']
     * @param string $local['otrasocial']
     * @param string $local['notas']
     * @param string $local['descripcion']
     * @param string $local['imagen']
     * @return boolean
     */
    public function updateLocal($local) {
        if($this->checkLocal("idlocal", $local["idlocal"])) {
            $fields = "";
            foreach($local as $key => $value) {
                if($key != "idlocal") {
                    if(!empty($fields)) {
                        $fields = $fields.",";
                    }
                    $fields = $fields.$key."='".$this->db->secure_field($value)."'";
                }
            }
            $query = sprintf("update %slocales set %s, actualizado = NOW() where idlocal = %d", BDPREFIX, $fields, $this->db->secure_field($local["idlocal"]));
            $r = $this->db->execute($query);
            if($r) {
                return true;
            } else {
                throw new Exception("[updateLocal] Error en la query: ".$query, 1);
            }
        } else {
            throw new Exception("[updateLocal] No existe el local.", 1);
        }
    }

    /*
     * Publicar locales para que sean visibles
     *
     * @param int $ids ids de los locales
     * @return boolean
     */
    public function publishLocal($ids) {
        $query = sprintf("update %slocales set publicado=1 where idlocal in (%s)", BDPREFIX, $this->db->secure_field($ids));
        $r = $this->db->execute($query);
        if($r) {
            return true;
        } else {
            throw new Exception("[publishLocal] Error en la query: ".$query, 1);
        }
    }

    /*
     * Despublicar locales para que no sean visibles
     *
     * @param int $ids ids de los locales
     * @return boolean
     */
    public function unpublishLocal($ids) {
        $query = sprintf("update %slocales set publicado=0 where idlocal in (%s)", BDPREFIX, $this->db->secure_field($ids));
        $r = $this->db->execute($query);
        if($r) {
            return true;
        } else {
            throw new Exception("[unpublishLocal] Error en la query: ".$query, 1);
        }
    }

    /*
     * Relacionar un sitio de entradas con un local
     *
     * @param int $idlocal id del local
     * @param int $identradas id del sitio de entradas
     * @return boolean
     */
    public function setEntradas($idlocal, $identradas) {
        if(!$this->checkRelEntradas($idlocal, $identradas)) {
            $query = sprintf("insert into %slocales_has_entradas (idlocal,identradas) VALUES (%d,%d)", BDPREFIX, $this->db->secure_field($idlocal), $this->db->secure_field($identradas));
            $r = $this->db->execute($query);
            if($r) {
                return true;
            } else {
                throw new Exception("[setEntradas] Error en la query: ".$query, 1);
            }
        } else {
            throw new Exception("[setEntradas] Ya existe la relacion entre el local y el sitio de entradas.", 1);
        }
    }

    /*
     * Relacionar un sitio de entradas con un local
     *
     * @param int $idlocal id del local
     * @return array
     */
    public function getEntradas($idlocal) {
        $query = sprintf("select le.identradas from %slocales_has_entradas le where le.idlocal = %d", BDPREFIX, $this->db->secure_field($idlocal));
        $r = $this->db->query($query);
        if($r) {
            $result = array();
            while($local_entradas = $this->db->fetch($r)) {
                $result[] = $local_entradas;
            }
            return $result;
        } else {
            throw new Exception("[getEntradas] Error en la query: ".$query, 1);
        }
    }

    /*
     * Eliminar la relacion de un sitio de entradas con un local
     *
     * @param int $idlocal id del local
     * @param int $identradas id del sitio de entradas
     * @return boolean
     */
    public function delEntradas($idlocal, $identradas = 0) {
        if(empty($identradas) || $this->checkRelEntradas($idlocal, $identradas)) {
            $query = sprintf("delete from %slocales_has_entradas where idlocal = %d", BDPREFIX, $this->db->secure_field($idlocal));
            if(empty($identradas)) {
                $query .= " and identradas = ".$this->db->secure_field($identradas);
            }
            $r = $this->db->execute($query);
            if($r) {
                return true;
            } else {
                throw new Exception("[delEntradas] Error en la query: ".$query, 1);
            }
        } else {
            throw new Exception("[delEntradas] No existe la relacion entre el local y el sitio de entradas.", 1);
        }
    }

    /*
     * Comprueba si existe el local
     *
     * @param string $field campo por el que comprobar
     * @param string $value valor que se va a comprobar en el campo
     * @return int|false
     */
    private function checkLocal($field, $value) {
        $query = sprintf("select l.* from %slocales l where l.%s = '%s'", BDPREFIX, $this->db->secure_field($field), $this->db->secure_field($value));
        $r = $this->db->query($query);

        if($this->db->count() > 0) {
            return $this->db->row(0);
        } else {
            return false;
        }
    }

    /*
     * Comprueba si existe la relacion local-entradas
     *
     * @param int $idlocal id del local
     * @param int $identradas id del sitio de entradas
     * @return boolean
     */
    private function checkRelEntradas($idlocal, $identradas) {
        $query = sprintf("select le.* from %slocales_has_entradas le where le.idlocal = %d and le.identradas = %d", BDPREFIX, $this->db->secure_field($idlocal), $this->db->secure_field($identradas));
        $r = $this->db->query($query);

        if($this->db->count() > 0) {
            return true;
        } else {
            return false;
        }
    }

    /*
     * Comprueba si existe la relacion local-promociones
     *
     * @param int $idlocal id del local
     * @param int $idpromocion id de la promocion
     * @return boolean
     */
    private function checkRelPromos($idlocal, $idpromocion) {
        $query = sprintf("select lp.* from %slocales_has_promociones lp where lp.idlocal = %d and lp.idpromocion = %d", BDPREFIX, $this->db->secure_field($idlocal), $this->db->secure_field($idpromocion));
        $r = $this->db->query($query);

        if($this->db->count() > 0) {
            return true;
        } else {
            return false;
        }
    }
}
// END