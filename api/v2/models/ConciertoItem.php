<?php
/**
 * Clase con metodos para operaciones con conciertos
 *
 * @package API
 * @author Trasno
 */
class ConciertoItem {
    protected $db;

    /**
     * Constructor
     *
     * @return void
     * @author trasno
     */
    public function __construct(Db $db) {
        $this->db = $db;
    }

    /**
     * Listado de conciertos
     *
     * @param array $options
     * @param int $options["limit"] (opcional) cuantos registros devolver
     * @param int $options["start"] (opcional) a partir de donde (posicion) se devuelve el listado
     * @param string $options["filter"] (opcional) cadena de filtro
     * @param string $options["order"] (opcional) campo por el que ordenar el listado.
     * @return array
     */
    public function listConciertos($options) {
        $limit = "";
        if(!empty($options["limit"])) {
            $limit = "limit ".$options["start"].",".$options["limit"];
        }
        if(!empty($options["filter"])) {
            $options["filter"] = "where ".$options["filter"];
        }
        $query = sprintf(
        "select c.idconcierto, c.nombre, lc.nombre as lugar, genero, poster, fecha, infantil, agotado, cancelado, todoeldia, publicado, reserva from %sconciertos c join (
            select idconcierto, l.nombre, fecha, agotado, cancelado, todoeldia from %slocales_has_conciertos lhc left join %slocales l on l.idlocal = lhc.idlocal union
            select idconcierto, t.nombre, fecha, agotado, cancelado, todoeldia from %steatros_has_conciertos thc left join %steatros t on t.idteatro = thc.idteatro union
            select idconcierto, cc.nombre, fecha, agotado, cancelado, todoeldia from %scines_has_conciertos chc left join %scines cc on cc.idcine = chc.idcine union
            select idconcierto, lugar, fecha, agotado, cancelado, todoeldia from %slugares_has_conciertos union
            select idconcierto, m.nombre, fecha, agotado, cancelado, todoeldia from %smuseos_has_conciertos mhc left join %smuseos m on m.idmuseo = mhc.idmuseo
        ) as lc on lc.idconcierto = c.idconcierto %s order by %s %s",
        BDPREFIX, BDPREFIX, BDPREFIX, BDPREFIX, BDPREFIX, BDPREFIX, BDPREFIX, BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"], $options["order"],$this->db->secure_field($limit));
        $r = $this->db->query($query);
        $result = array();
        while($concierto = $this->db->fetch($r)) {
            $result[] = $concierto;
        }

        return $result;
    }

    /**
     * Coge un concierto y todos sus datos (locales, horarios, info...)
     *
     * @param int $idconcierto id de el concierto
     * @return array|false
     */
    public function dataConcierto($idconcierto) {
        if(!empty($idconcierto)) {
            $query = sprintf("select c.* from %sconciertos c where c.idconcierto = %d", BDPREFIX, $this->db->secure_field($idconcierto));
            $r = $this->db->query($query);
            if($this->db->count($r) > 0) {
                return array(0 => $this->db->fetch($r));
            } else {
                return false;
            }
        } else {
            throw new Exception("Parametro incorrecto", 1);
        }
    }

    /*
     * Eliminar un concierto
     *
     * @param int $idconcierto id de el concierto
     * @return boolean
     */
    public function deleteConcierto($idconcierto) {
        if(!empty($idconcierto)) {
            $query = sprintf("delete from %slocales_has_conciertos where idconcierto = %d",BDPREFIX, $this->db->secure_field($idconcierto));
            $r = $this->db->execute($query);
            $query = sprintf("delete from %steatros_has_conciertos where idconcierto = %d",BDPREFIX, $this->db->secure_field($idconcierto));
            $r = $this->db->execute($query);
            $query = sprintf("delete from %scines_has_conciertos where idconcierto = %d",BDPREFIX, $this->db->secure_field($idconcierto));
            $r = $this->db->execute($query);
            $query = sprintf("delete from %slugares_has_conciertos where idconcierto = %d",BDPREFIX, $this->db->secure_field($idconcierto));
            $r = $this->db->execute($query);
            $query = sprintf("delete from %smuseos_has_conciertos where idconcierto = %d",BDPREFIX, $this->db->secure_field($idconcierto));
            $r = $this->db->execute($query);
            if($r) {
                $query = sprintf("delete from %sconciertos where idconcierto = %d",BDPREFIX, $this->db->secure_field($idconcierto));
                $r = $this->db->execute($query);
                deleteAllExtImages("img/conciertos/poster/".$idconcierto);
                deleteAllExtImages("img/conciertos/poster/thumbs/".$idconcierto);
                deleteAllExtImages("img/conciertos/poster/original/".$idconcierto);
                deleteAllExtImages("img/conciertos/fanart/".$idconcierto);
                return true;
            }
        }
        return false;
    }

    /*
     * Insertar un concierto
     *
     * @param array $concierto datos del concierto
     * @param string $concierto['nombre'] nombre de el concierto
     * @param int $concierto['duracion'] duracion en minutos
     * @param string $concierto['sinopsis']
     * @param string $concierto['genero']
     * @param string $concierto['video']
     * @param string $concierto['poster']
     * @param string $concierto['fanart']
     * @return int
     */
    public function addConcierto($concierto) {
        if(!$this->checkConcierto("nombre", $concierto["nombre"])) {
            $fields = "";
            $values = "";
            foreach($concierto as $key => $value) {
                if(!empty($fields)) {
                    $fields = $fields.",";
                }
                $fields = $fields.$key;
                if(!empty($values)) {
                    $values = $values.",";
                }
                $values = $values."'".$this->db->secure_field($value)."'";
            }
            $now = date("Y-m-d H:i:s", strtotime("now"));
            $query = sprintf("insert into %sconciertos (%s, creado) VALUES (%s, '%s')", BDPREFIX, $fields, $values, $now);
            $r = $this->db->execute($query);
            if($r) {
                $id = $this->db->last_id();
                return $id;
            } else {
                throw new Exception("[addConcierto] Error en la query: ".$query, 1);
            }
        } else {
            throw new Exception("[addConcierto] Ya existe el concierto.", 1);
        }
    }

    /*
     * Actualiza los campos de un concierto
     *
     * @param array $concierto datos del concierto
     * @param int $concierto['idconcierto'] identificador de el concierto
     * @param string $concierto['nombre'] nombre de el concierto
     * @param int $concierto['duracion'] duracion en minutos
     * @param string $concierto['sinopsis']
     * @param string $concierto['genero']
     * @param string $concierto['video']
     * @param string $concierto['poster']
     * @param string $concierto['fanart']
     * @return boolean
     */
    public function updateConcierto($concierto) {
        if($this->checkConcierto("idconcierto", $concierto["idconcierto"])) {
            $fields = "";
            foreach($concierto as $key => $value) {
                if($key != "idconcierto") {
                    if(!empty($fields)) {
                        $fields = $fields.",";
                    }
                    $fields = $fields.$key."='".$this->db->secure_field($value)."'";
                }
            }
            $now = date("Y-m-d H:i:s", strtotime("now"));
            $query = sprintf("update %sconciertos set %s, actualizado = '%s' where idconcierto = %d", BDPREFIX, $fields, $now, $this->db->secure_field($concierto["idconcierto"]));
            $r = $this->db->execute($query);
            if($r) {
                return true;
            } else {
                throw new Exception("[updateConcierto] Error en la query: ".$query, 1);
            }
        } else {
            throw new Exception("[updateConcierto] No existe el concierto.", 1);
        }
    }

    /*
     * Publicar conciertos para que sean visibles
     *
     * @param int $ids ids de los conciertos
     * @return boolean
     */
    public function publishConcierto($ids) {
        $query = sprintf("update %sconciertos set publicado=1 where idconcierto in (%s)", BDPREFIX, $this->db->secure_field($ids));
        $r = $this->db->execute($query);
        if($r) {
            return true;
        } else {
            throw new Exception("[publishConcierto] Error en la query: ".$query, 1);
        }
    }

    /*
     * Despublicar conciertos para que no sean visibles
     *
     * @param int $ids ids de los conciertos
     * @return boolean
     */
    public function unpublishConcierto($ids) {
        $query = sprintf("update %sconciertos set publicado=0 where idconcierto in (%s)", BDPREFIX, $this->db->secure_field($ids));
        $r = $this->db->execute($query);
        if($r) {
            return true;
        } else {
            throw new Exception("[unpublishConcierto] Error en la query: ".$query, 1);
        }
    }

    /*
     * Relacionar un concierto con un local
     *
     * @param array $concierto
     * @param int $concierto['idlugar']
     * @param int $concierto['idconcierto']
     * @param datetime $concierto['fecha']
     * @param string $concierto['urlcompra']
     * @param string $concierto['precio']
     * @param string $concierto['precioadelantada']
     * @return boolean
     */
    public function addHora($concierto, $type) {
        if(!$this->checkRelLugarConcierto($concierto,$type)) {
            switch($type){
                case "local":
                    $tabla = "locales_has_conciertos";
                    $idlugar = "idlocal";
                    break;
                case "teatro":
                    $tabla = "teatros_has_conciertos";
                    $idlugar = "idteatro";
                    break;
                case "cine":
                    $tabla = "cines_has_conciertos";
                    $idlugar = "idcine";
                    break;
                case "lugar":
                    $tabla = "lugares_has_conciertos";
                    unset($concierto["idlugar"], $concierto["duracion"]);
                    break;
                case "museo":
                    $tabla = "museos_has_conciertos";
                    $idlugar = "idmuseo";
                    break;
            }
            $fields = "";
            $values = "";
            foreach($concierto as $key => $value) {
                if(!empty($fields)) {
                    $fields = $fields.",";
                }
                if($key == "idlugar") {
                    $key = $idlugar;
                }
                $fields = $fields.$key;
                if(!empty($values)) {
                    $values = $values.",";
                }
                $values = $values."'".$this->db->secure_field($value)."'";
            }
            $query = sprintf("insert into %s (%s) VALUES (%s)", BDPREFIX.$tabla, $fields, $values);
            $r = $this->db->execute($query);
            if($r) {
                return true;
            } else {
                throw new Exception("[addHora] Error en la query: ".$query, 1);
            }
        } else {
            throw new Exception("[addHora] Ya existe la relacion.", 1);
        }
    }

    /*
     * Coger las horas de un concierto (opcionalmente) con una sala y con un local
     *
     * @param array $concierto
     * @param int $concierto['idlugar']
     * @param int (opcional) $concierto['idconcierto']
     * @return array
     */
    public function listHoras($concierto) {
        $result = array("horasLocal" => array(), "horasAuditorio" => array(), "horasCine" => array(), "horasLugar" => array(), "horasMuseo" => array());

        //Locales
        $query = sprintf("select lc.*, l.nombre, l.direccion, l.localidad, l.coordenadas from %slocales_has_conciertos lc, %slocales l where lc.idconcierto = %d and l.idlocal = lc.idlocal", BDPREFIX, BDPREFIX, $this->db->secure_field($concierto["idconcierto"]));
        if(isset($concierto["idlugar"]) && !empty($concierto["idlugar"])) {
            $query .= " and lc.idlocal = ".$this->db->secure_field($concierto["idlugar"]);
        }
        $query .= " order by lc.idlocal, lc.fecha";
        $r = $this->db->query($query);
        if($r) {
            while($hora = $this->db->fetch($r)) {
                $result["horasLocal"][] = $hora;
            }
        } else {
            throw new Exception("[listHoras] Error en la query: ".$query, 1);
        }
        //Auditorios
        $query = sprintf("select tc.*, t.nombre, t.direccion, t.localidad, t.coordenadas from %steatros_has_conciertos tc, %steatros t where tc.idconcierto = %d and t.idteatro = tc.idteatro", BDPREFIX, BDPREFIX, $this->db->secure_field($concierto["idconcierto"]));
        if(isset($concierto["idlugar"]) && !empty($concierto["idlugar"])) {
            $query .= " and lc.idteatro = ".$this->db->secure_field($concierto["idlugar"]);
        }
        $query .= " order by tc.idteatro, tc.fecha";
        $r = $this->db->query($query);
        if($r) {
            while($hora = $this->db->fetch($r)) {
                $result["horasAuditorio"][] = $hora;
            }
        } else {
            throw new Exception("[listHoras] Error en la query: ".$query, 1);
        }
        //Cines
        $query = sprintf("select cc.*, c.nombre, s.numero, c.direccion, c.localidad, c.coordenadas from %scines_has_conciertos cc, %scines c, %ssalas s where cc.idconcierto = %d and c.idcine = cc.idcine and s.idsala = cc.idsala", BDPREFIX, BDPREFIX, BDPREFIX, $this->db->secure_field($concierto["idconcierto"]));
        if(isset($concierto["idsala"]) && !empty($concierto["idsala"])) {
            $query .= " and cc.idsala = ".$this->db->secure_field($concierto["idsala"]);
        }
        if(isset($concierto["idlugar"]) && !empty($concierto["idlugar"])) {
            $query .= " and cc.idcine = ".$this->db->secure_field($concierto["idlugar"]);
        }
        $query .= " order by cc.idcine, cc.fecha";
        $r = $this->db->query($query);
        if($r) {
            while($hora = $this->db->fetch($r)) {
                $result["horasCine"][] = $hora;
            }
        } else {
            throw new Exception("[listHoras] Error en la query: ".$query, 1);
        }
        //Lugar
        $query = sprintf("select lc.* from %slugares_has_conciertos lc where lc.idconcierto = %d order by lc.fecha", BDPREFIX, $this->db->secure_field($concierto["idconcierto"]));
        $r = $this->db->query($query);
        if($r) {
            while($hora = $this->db->fetch($r)) {
                $result["horasLugar"][] = $hora;
            }
        } else {
            throw new Exception("[listHoras] Error en la query: ".$query, 1);
        }
        //Museos
        $query = sprintf("select mc.*, m.nombre, m.direccion, m.localidad, m.coordenadas from %smuseos_has_conciertos mc, %smuseos m where mc.idconcierto = %d and m.idmuseo = mc.idmuseo", BDPREFIX, BDPREFIX, $this->db->secure_field($concierto["idconcierto"]));
        if(isset($concierto["idlugar"]) && !empty($concierto["idlugar"])) {
            $query .= " and mc.idmuseo = ".$this->db->secure_field($concierto["idlugar"]);
        }
        $query .= " order by mc.idmuseo, mc.fecha";
        $r = $this->db->query($query);
        if($r) {
            while($hora = $this->db->fetch($r)) {
                $result["horasMuseo"][] = $hora;
            }
        } else {
            throw new Exception("[listHoras] Error en la query: ".$query, 1);
        }

        return $result;
    }

    /*
     * Eliminar la relacion de un concierto con un local
     *
     * @param array $concierto
     * @param int $concierto['idlugar']
     * @param int $concierto['idconcierto']
     * @param datetime $concierto['fecha']
     * @param string $type especifica con que tipo de local se relaciona ese horario
     * @return boolean
     */
    public function deleteHora($concierto,$type) {
        if($this->checkRelLugarConcierto($concierto,$type)) {
            switch($type){
                case "local":
                    $tabla = "locales_has_conciertos";
                    $idlugar = "idlocal = ".$this->db->secure_field($concierto["idlugar"]);
                    break;
                case "teatro":
                    $tabla = "teatros_has_conciertos";
                    $idlugar = "idteatro = ".$this->db->secure_field($concierto["idlugar"]);
                    break;
                case "cine":
                    $tabla = "cines_has_conciertos";
                    $idlugar = "idcine = ".$this->db->secure_field($concierto["idlugar"]);
                    break;
                case "lugar":
                    $tabla = "lugares_has_conciertos";
                    $idlugar = "1";
                    break;
                case "museo":
                    $tabla = "museos_has_conciertos";
                    $idlugar = "idmuseo = ".$this->db->secure_field($concierto["idlugar"]);
                    break;
            }
            $query = sprintf("delete from %s where idconcierto = %d and %s and fecha = '%s'", BDPREFIX.$tabla, $this->db->secure_field($concierto["idconcierto"]), $idlugar, $this->db->secure_field($concierto["fecha"]));
            $r = $this->db->execute($query);
            if($r) {
                return true;
            } else {
                throw new Exception("[delHora] Error en la query: ".$query, 1);
            }
        } else {
            throw new Exception("[delHora] No existe la relacion.", 1);
        }
    }

    /*
     * Actualizar una relacion de un concierto con un local/teatro
     *
     * @param array $concierto
     * @param int $concierto['idlugar']
     * @param int $concierto['idconcierto']
     * @param date $concierto['fecha']
     * @param string $concierto['urlcompra']
     * @param string $concierto['precio']
     * @param string $concierto['precioanticipada']
     * @param string $type especifica con que tipo de local se relaciona ese horario
     * @return boolean
     */
    public function updateHora($concierto, $type) {
        if($this->checkRelLugarConcierto($concierto, $type)) {
            switch($type){
                case "local":
                    $tabla = "locales_has_conciertos";
                    $idlugar = "idlocal = ".$this->db->secure_field($concierto["idlugar"]);
                    break;
                case "teatro":
                    $tabla = "teatros_has_conciertos";
                    $idlugar = "idteatro = ".$this->db->secure_field($concierto["idlugar"]);
                    break;
                case "cine":
                    $tabla = "cines_has_conciertos";
                    $idlugar = "idcine = ".$this->db->secure_field($concierto["idlugar"]);
                    break;
                case "lugar":
                    $tabla = "lugares_has_conciertos";
                    $idlugar = "1";
                    unset($concierto["duracion"]);
                    break;
                case "museo":
                    $tabla = "museos_has_conciertos";
                    $idlugar = "idmuseo = ".$this->db->secure_field($concierto["idlugar"]);
                    break;
            }
            $fields = "";
            $exclude = array("idlugar","idconcierto","fecha");
            foreach($concierto as $key => $value) {
                if(!in_array($key, $exclude)) {
                    if(!empty($fields)) {
                        $fields = $fields.",";
                    }
                    $fields = $fields.$key."='".$this->db->secure_field($value)."'";
                }
            }
            $query = sprintf("update %s set %s where idconcierto = %d and %s and fecha = '%s'", BDPREFIX.$tabla, $fields, $this->db->secure_field($concierto["idconcierto"]), $idlugar, $this->db->secure_field($concierto["fecha"]));
            if(isset($concierto["idsala"])) {
                $query .= " and idsala = ".$this->db->secure_field($concierto["idsala"]);
            }
            $r = $this->db->execute($query);
            if($r) {
                return true;
            } else {
                throw new Exception("[updateHora] Error en la query: ".$query, 1);
            }
        } else {
            throw new Exception("[updateHora] No existe la relacion.", 1);
        }
    }

    /*
     * Comprueba si existe el concierto
     *
     * @param string $field campo por el que comprobar
     * @param string $value valor que se va a comprobar en el campo
     * @return int|false
     */
    private function checkConcierto($field, $value) {
        $query = sprintf("select c.* from %sconciertos c where c.%s = '%s'", BDPREFIX, $this->db->secure_field($field), $this->db->secure_field($value));
        $r = $this->db->query($query);

        if($this->db->count() > 0) {
            return $this->db->row(0);
        } else {
            return false;
        }
    }

    /*
     * Comprueba si existe el concierto en un horario y local/teatro determinados
     *
     * @param array $concierto
     * @param int $concierto['idlugar']
     * @param int $concierto['idconcierto']
     * @param datetime $concierto['fecha']
     * @param string $type especifica con que tipo de local se relaciona ese horario
     * @return int|false
     */
    private function checkRelLugarConcierto($concierto, $type) {
        switch($type){
            case "local":
                $tabla = "locales_has_conciertos";
                $idlugar = "idlocal = ".$this->db->secure_field($concierto["idlugar"]);
                break;
            case "teatro":
                $tabla = "teatros_has_conciertos";
                $idlugar = "idteatro = ".$this->db->secure_field($concierto["idlugar"]);
                break;
            case "cine":
                $tabla = "cines_has_conciertos";
                $idlugar = "idcine = ".$this->db->secure_field($concierto["idlugar"]);
                break;
            case "lugar":
                $tabla = "lugares_has_conciertos";
                $idlugar = "1";
                break;
            case "museo":
                $tabla = "museos_has_conciertos";
                $idlugar = "idmuseo = ".$this->db->secure_field($concierto["idlugar"]);
                break;
        }
        $query = sprintf("select * from %s where idconcierto = %d and %s and fecha = '%s'", BDPREFIX.$tabla, $this->db->secure_field($concierto["idconcierto"]), $idlugar, $this->db->secure_field($concierto["fecha"]));
        $r = $this->db->query($query);

        if($this->db->count() > 0) {
            return $this->db->row(0);
        } else {
            return false;
        }
    }
}
// END