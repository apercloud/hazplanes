<?php
/**
 * Clase con metodos para operaciones con cursos
 *
 * @package API
 * @author Trasno
 */
class CursoItem {
    protected $db;

    /**
     * Constructor
     *
     * @return void
     * @author trasno
     */
    public function __construct(Db $db) {
        $this->db = $db;
    }

    /**
     * Listado de cursos
     *
     * @param array $options
     * @param int $options["limit"] (opcional) cuantos registros devolver
     * @param int $options["start"] (opcional) a partir de donde (posicion) se devuelve el listado
     * @param string $options["filter"] (opcional) cadena de filtro
     * @param string $options["order"] (opcional) campo por el que ordenar el listado.
     * @return array
     */
    public function listCursos($options) {
        $limit = "";
        if(!empty($options["limit"])) {
            $limit = "limit ".$options["start"].",".$options["limit"];
        }
        if(!empty($options["filter"])) {
            $options["filter"] = "where ".$options["filter"];
        }
        $query = sprintf(
        "select c.idcurso, c.nombre, lc.nombre as lugar, tipo, poster, fecha, infantil, agotado, cancelado, todoeldia, publicado, reserva from %scursos c join (
            select idcurso, l.nombre, fecha, agotado, cancelado, todoeldia from %slocales_has_cursos lhc left join %slocales l on l.idlocal = lhc.idlocal union
            select idcurso, t.nombre, fecha, agotado, cancelado, todoeldia from %steatros_has_cursos thc left join %steatros t on t.idteatro = thc.idteatro union
            select idcurso, cc.nombre, fecha, agotado, cancelado, todoeldia from %scines_has_cursos chc left join %scines cc on cc.idcine = chc.idcine union
            select idcurso, lugar, fecha, agotado, cancelado, todoeldia from %slugares_has_cursos union
            select idcurso, m.nombre, fecha, agotado, cancelado, todoeldia from %smuseos_has_cursos mhc left join %smuseos m on m.idmuseo = mhc.idmuseo
        ) lc on lc.idcurso = c.idcurso %s order by %s %s",
        BDPREFIX, BDPREFIX, BDPREFIX, BDPREFIX, BDPREFIX, BDPREFIX, BDPREFIX, BDPREFIX, BDPREFIX, BDPREFIX, $options["filter"], $options["order"],$this->db->secure_field($limit));
        //error_log($query);
        $r = $this->db->query($query);

        $result = array();
        while($cursos = $this->db->fetch($r)) {
            $result[] = $cursos;
        }

        return $result;
    }

    /**
     * Coge un curso y todos sus datos
     *
     * @param int $idcurso id del curso
     * @return array|false
     */
    public function dataCursos($idcurso) {
        if(!empty($idcurso)) {
            $query = sprintf("select e.* from %scursos e where e.idcurso = %d", BDPREFIX, $this->db->secure_field($idcurso));
            $r = $this->db->query($query);
            if($this->db->count($r) > 0) {
                return array(0 => $this->db->fetch($r));
            } else {
                return false;
            }
        } else {
            throw new Exception("Parametro incorrecto", 1);
        }
    }

    /*
     * Eliminar un curso
     *
     * @param int $idcurso id del curso
     * @return boolean
     */
    public function deleteCursos($idcurso) {
        if(!empty($idcurso)) {
            $query = sprintf("delete from %slocales_has_cursos where idcurso = %d",BDPREFIX, $this->db->secure_field($idcurso));
            $r = $this->db->execute($query);
            $query = sprintf("delete from %steatros_has_cursos where idcurso = %d",BDPREFIX, $this->db->secure_field($idcurso));
            $r = $this->db->execute($query);
            $query = sprintf("delete from %scines_has_cursos where idcurso = %d",BDPREFIX, $this->db->secure_field($idcurso));
            $r = $this->db->execute($query);
            $query = sprintf("delete from %slugares_has_cursos where idcurso = %d",BDPREFIX, $this->db->secure_field($idcurso));
            $r = $this->db->execute($query);
            $query = sprintf("delete from %smuseos_has_cursos where idcurso = %d",BDPREFIX, $this->db->secure_field($idcurso));
            $r = $this->db->execute($query);
            if($r) {
                $query = sprintf("delete from %scursos where idcurso = %d",BDPREFIX, $this->db->secure_field($idcurso));
                $r = $this->db->execute($query);
                deleteAllExtImages("img/cursos/poster/".$idcurso);
                deleteAllExtImages("img/cursos/poster/thumbs/".$idcurso);
                deleteAllExtImages("img/cursos/poster/original/".$idcurso);
                return true;
            }
        }
        return false;
    }

    /*
     * Insertar un curso
     *
     * @param array $cursos datos del curso
     * @param string $cursos['nombre']
     * @param string $cursos['lugar']
     * @param string $cursos['tipo'] tipo de curso
     * @param string $cursos['descripcion']
     * @param string $cursos['notas']
     * @param datetime $cursos['fecha']
     * @return int
     */
    public function addCursos($cursos) {
        if(!$this->checkCursos("nombre like '".$this->db->secure_field($cursos["nombre"])."'")) {
            $fields = "";
            $values = "";
            foreach($cursos as $key => $value) {
                if(!empty($fields)) {
                    $fields = $fields.",";
                }
                $fields = $fields.$key;
                if(!empty($values)) {
                    $values = $values.",";
                }
                $values = $values."'".$this->db->secure_field($value)."'";
            }
            $now = date("Y-m-d H:i:s", strtotime("now"));
            $query = sprintf("insert into %scursos (%s, creado) VALUES (%s, '%s')", BDPREFIX, $fields, $values, $now);
            $r = $this->db->execute($query);
            if($r) {
                $id = $this->db->last_id();
                return $id;
            } else {
                throw new Exception("[addCursos] Error en la query: ".$query, 1);
            }
        } else {
            throw new Exception("[addCursos] Ya existe el curso.", 1);
        }
    }

    /*
     * Actualiza los campos de un curso
     *
     * @param array $cursos datos del curso
     * @param int $cursos['idcurso'] identificador del curso
     * @param string $cursos['nombre']
     * @param string $cursos['lugar']
     * @param string $cursos['tipo'] tipo de curso
     * @param string $cursos['descripcion']
     * @param string $cursos['notas']
     * @param datetime $cursos['fecha']
     * @return boolean
     */
    public function updateCursos($cursos) {
        if($this->checkCursos("idcurso = ".$cursos["idcurso"])) {
            $fields = "";
            foreach($cursos as $key => $value) {
                if($key != "idcurso") {
                    if(!empty($fields)) {
                        $fields = $fields.",";
                    }
                    $fields = $fields.$key."='".$this->db->secure_field($value)."'";
                }
            }
            $now = date("Y-m-d H:i:s", strtotime("now"));
            $query = sprintf("update %scursos set %s, actualizado = '%s' where idcurso = %d", BDPREFIX, $fields, $now, $this->db->secure_field($cursos["idcurso"]));
            $r = $this->db->execute($query);
            if($r) {
                return true;
            } else {
                throw new Exception("[updateCursos] Error en la query: ".$query, 1);
            }
        } else {
            throw new Exception("[updateCursos] No existe la cursos.", 1);
        }
    }

    /*
     * Publicar cursos para que sean visibles
     *
     * @param int $ids ids de las webs de cursos
     * @return boolean
     */
    public function publishCursos($ids) {
        $query = sprintf("update %scursos set publicado=1 where idcurso in (%s)", BDPREFIX, $this->db->secure_field($ids));
        $r = $this->db->execute($query);
        if($r) {
            return true;
        } else {
            throw new Exception("[publishCursos] Error en la query: ".$query, 1);
        }
    }

    /*
     * Despublicar cursos para que no sean visibles
     *
     * @param int $ids ids de las webs de cursos
     * @return boolean
     */
    public function unpublishCursos($ids) {
        $query = sprintf("update %scursos set publicado=0 where idcurso in (%s)", BDPREFIX, $this->db->secure_field($ids));
        $r = $this->db->execute($query);
        if($r) {
            return true;
        } else {
            throw new Exception("[unpublishCursos] Error en la query: ".$query, 1);
        }
    }

    /*
     * Relacionar un curso con una fecha
     *
     * @param array $curso
     * @param int $curso['idcurso']
     * @param datetime $pelicula['fecha']
     * @param int $pelicula['duracion']
     * @param string $curso['precio']
     * @param string $curso['lugar']
     * @return boolean
     */
    public function addHora($curso, $type) {
        if(!$this->checkRelFechaCurso($curso, $type)) {
            switch($type){
                case "local":
                    $tabla = "locales_has_cursos";
                    $idlugar = "idlocal";
                    break;
                case "teatro":
                    $tabla = "teatros_has_cursos";
                    $idlugar = "idteatro";
                    break;
                case "cine":
                    $tabla = "cines_has_cursos";
                    $idlugar = "idcine";
                    break;
                case "lugar":
                    $tabla = "lugares_has_cursos";
                    unset($curso["idlugar"], $curso["duracion"]);
                    break;
                case "museo":
                    $tabla = "museos_has_cursos";
                    $idlugar = "idmuseo";
                    break;
            }
            $fields = "";
            $values = "";
            foreach($curso as $key => $value) {
                if(!empty($fields)) {
                    $fields = $fields.",";
                }
                if($key == "idlugar") {
                    $key = $idlugar;
                }
                $fields = $fields.$key;
                if(!empty($values)) {
                    $values = $values.",";
                }
                $values = $values."'".$this->db->secure_field($value)."'";
            }
            $query = sprintf("insert into %s (%s) VALUES (%s)", BDPREFIX.$tabla, $fields, $values);
            $r = $this->db->execute($query);
            if($r) {
                return true;
            } else {
                throw new Exception("[addHora] Error en la query: ".$query, 1);
            }
        } else {
            throw new Exception("[addHora] Ya existe la relacion.", 1);
        }
    }

    /*
     * Coger las horas de un curso
     *
     * @param array $curso
     * @param int $curso['idlugar']
     * @param int (opcional) $curso['idcurso']
     * @return array
     */
    public function listHoras($curso) {
        $result = array("horasLocal" => array(), "horasAuditorio" => array(), "horasCine" => array(), "horasLugar" => array(), "horasMuseo" => array());

        //Locales
        $query = sprintf("select lc.*, l.nombre, l.direccion, l.localidad, l.coordenadas from %slocales_has_cursos lc, %slocales l where lc.idcurso = %d and l.idlocal = lc.idlocal", BDPREFIX, BDPREFIX, $this->db->secure_field($curso["idcurso"]));
        if(isset($curso["idlugar"]) && !empty($curso["idlugar"])) {
            $query .= " and lc.idlocal = ".$this->db->secure_field($curso["idlugar"]);
        }
        $query .= " order by lc.idlocal, lc.fecha";
        $r = $this->db->query($query);
        if($r) {
            while($hora = $this->db->fetch($r)) {
                $result["horasLocal"][] = $hora;
            }
        } else {
            throw new Exception("[listHoras] Error en la query: ".$query, 1);
        }
        //Auditorios
        $query = sprintf("select tc.*, t.nombre, t.direccion, t.localidad, t.coordenadas from %steatros_has_cursos tc, %steatros t where tc.idcurso = %d and t.idteatro = tc.idteatro", BDPREFIX, BDPREFIX, $this->db->secure_field($curso["idcurso"]));
        if(isset($curso["idlugar"]) && !empty($curso["idlugar"])) {
            $query .= " and lc.idteatro = ".$this->db->secure_field($curso["idlugar"]);
        }
        $query .= " order by tc.idteatro, tc.fecha";
        $r = $this->db->query($query);
        if($r) {
            while($hora = $this->db->fetch($r)) {
                $result["horasAuditorio"][] = $hora;
            }
        } else {
            throw new Exception("[listHoras] Error en la query: ".$query, 1);
        }
        //Cines
        $query = sprintf("select cc.*, c.nombre, s.numero, c.direccion, c.localidad, c.coordenadas from %scines_has_cursos cc, %scines c, %ssalas s where cc.idcurso = %d and c.idcine = cc.idcine and s.idsala = cc.idsala", BDPREFIX, BDPREFIX, BDPREFIX, $this->db->secure_field($curso["idcurso"]));
        if(isset($curso["idsala"]) && !empty($curso["idsala"])) {
            $query .= " and cc.idsala = ".$this->db->secure_field($curso["idsala"]);
        }
        if(isset($curso["idlugar"]) && !empty($curso["idlugar"])) {
            $query .= " and cc.idcine = ".$this->db->secure_field($curso["idlugar"]);
        }
        $query .= " order by cc.idcine, cc.fecha";
        $r = $this->db->query($query);
        if($r) {
            while($hora = $this->db->fetch($r)) {
                $result["horasCine"][] = $hora;
            }
        } else {
            throw new Exception("[listHoras] Error en la query: ".$query, 1);
        }
        //Lugar
        $query = sprintf("select le.* from %slugares_has_cursos le where le.idcurso = %d order by le.fecha", BDPREFIX, $this->db->secure_field($curso["idcurso"]));
        $r = $this->db->query($query);
        if($r) {
            while($hora = $this->db->fetch($r)) {
                $result["horasLugar"][] = $hora;
            }
        } else {
            throw new Exception("[listHoras] Error en la query: ".$query, 1);
        }
        //Museos
        $query = sprintf("select mc.*, m.nombre, m.direccion, m.localidad, m.coordenadas from %smuseos_has_cursos mc, %smuseos m where mc.idcurso = %d and m.idmuseo = mc.idmuseo", BDPREFIX, BDPREFIX, $this->db->secure_field($curso["idcurso"]));
        if(isset($curso["idlugar"]) && !empty($curso["idlugar"])) {
            $query .= " and mc.idmuseo = ".$this->db->secure_field($curso["idlugar"]);
        }
        $query .= " order by mc.idmuseo, mc.fecha";
        $r = $this->db->query($query);
        if($r) {
            while($hora = $this->db->fetch($r)) {
                $result["horasMuseo"][] = $hora;
            }
        } else {
            throw new Exception("[listHoras] Error en la query: ".$query, 1);
        }

        return $result;
    }

    /*
     * Eliminar la relacion de un curso con una fecha
     *
     * @param array $curso
     * @param int $curso['idlugar']
     * @param int $curso['idcurso']
     * @param datetime $curso['fecha']
     * @param string $type especifica con que tipo de local se relaciona ese horario
     * @return boolean
     */
    public function deleteHora($curso,$type) {
        if($this->checkRelFechaCurso($curso,$type)) {
            switch($type){
                case "local":
                    $tabla = "locales_has_cursos";
                    $idlugar = "idlocal = ".$this->db->secure_field($curso["idlugar"]);
                    break;
                case "teatro":
                    $tabla = "teatros_has_cursos";
                    $idlugar = "idteatro = ".$this->db->secure_field($curso["idlugar"]);
                    break;
                case "cine":
                    $tabla = "cines_has_cursos";
                    $idlugar = "idcine = ".$this->db->secure_field($curso["idlugar"]);
                    break;
                case "lugar":
                    $tabla = "lugares_has_cursos";
                    $idlugar = "1";
                    break;
                case "museo":
                    $tabla = "museos_has_cursos";
                    $idlugar = "idmuseo = ".$this->db->secure_field($curso["idlugar"]);
                    break;
            }
            $query = sprintf("delete from %s where idcurso = %d and %s and fecha = '%s'", BDPREFIX.$tabla, $this->db->secure_field($curso["idcurso"]), $idlugar, $this->db->secure_field($curso["fecha"]));
            $r = $this->db->execute($query);
            if($r) {
                return true;
            } else {
                throw new Exception("[delHora] Error en la query: ".$query, 1);
            }
        } else {
            throw new Exception("[delHora] No existe la relacion.", 1);
        }
    }

    /*
     * Actualizar una relacion de un curso con una hora
     *
     * @param array $curso
     * @param int $curso['idlugar']
     * @param int $curso['idcurso']
     * @param date $curso['fecha']
     * @param string $curso['precio']
     * @param string $curso['precioanticipada']
     * @param int $curso['duracion'] solo si es en un lugar
     * @param string $curso['lugar'] solo si es en un lugar
     * @param string $curso['coordenadas'] solo si es en un lugar
     * @param string $type especifica con que tipo de local se relaciona ese horario
     * @return boolean
     */
    public function updateHora($curso, $type) {
        if($this->checkRelFechaCurso($curso, $type)) {
            switch($type){
                case "local":
                    $tabla = "locales_has_cursos";
                    $idlugar = "idlocal = ".$this->db->secure_field($curso["idlugar"]);
                    break;
                case "teatro":
                    $tabla = "teatros_has_cursos";
                    $idlugar = "idteatro = ".$this->db->secure_field($curso["idlugar"]);
                    break;
                case "cine":
                    $tabla = "cines_has_cursos";
                    $idlugar = "idcine = ".$this->db->secure_field($curso["idlugar"]);
                    break;
                case "lugar":
                    $tabla = "lugares_has_cursos";
                    $idlugar = "1";
                    unset($curso["duracion"]);
                    break;
                case "museo":
                    $tabla = "museos_has_cursos";
                    $idlugar = "idmuseo = ".$this->db->secure_field($curso["idlugar"]);
                    break;
            }
            $fields = "";
            $exclude = array("idlugar","idcurso","fecha");
            foreach($curso as $key => $value) {
                if(!in_array($key, $exclude)) {
                    if(!empty($fields)) {
                        $fields = $fields.",";
                    }
                    $fields = $fields.$key."='".$this->db->secure_field($value)."'";
                }
            }
            $query = sprintf("update %s set %s where idcurso = %d and %s and fecha = '%s'", BDPREFIX.$tabla, $fields, $this->db->secure_field($curso["idcurso"]), $idlugar, $this->db->secure_field($curso["fecha"]));
            if(isset($curso["idsala"])) {
                $query .= " and idsala = ".$this->db->secure_field($curso["idsala"]);
            }
            $r = $this->db->execute($query);
            if($r) {
                return true;
            } else {
                throw new Exception("[updateHora] Error en la query: ".$query, 1);
            }
        } else {
            throw new Exception("[updateHora] Ya existe la relacion.", 1);
        }
    }

    /*
     * Comprueba si existe el curso
     *
     * @param string $field campo por el que comprobar
     * @param string $value valor que se va a comprobar en el campo
     * @return int|false
     */
    private function checkCursos($field) {
        $query = sprintf("select c.* from %scursos c where %s", BDPREFIX, $field);
        $r = $this->db->query($query);

        if($this->db->count() > 0) {
            return $this->db->row(0);
        } else {
            return false;
        }
    }

    /*
     * Comprueba si existe el curso en un horario y local/teatro determinados
     *
     * @param array $curso
     * @param int $curso['idlugar']
     * @param int $curso['idcurso']
     * @param date $curso['fecha']
     * @param string $type especifica con que tipo de local se relaciona ese horario
     * @return int|false
     */
    private function checkRelFechaCurso($curso, $type) {
        switch($type){
            case "local":
                $tabla = "locales_has_cursos";
                $idlugar = "idlocal = ".$this->db->secure_field($curso["idlugar"]);
                break;
            case "teatro":
                $tabla = "teatros_has_cursos";
                $idlugar = "idteatro = ".$this->db->secure_field($curso["idlugar"]);
                break;
            case "cine":
                $tabla = "cines_has_cursos";
                $idlugar = "idcine = ".$this->db->secure_field($curso["idlugar"]);
                break;
            case "lugar":
                $tabla = "lugares_has_cursos";
                $idlugar = "1";
                break;
            case "museo":
                $tabla = "museos_has_cursos";
                $idlugar = "idmuseo = ".$this->db->secure_field($curso["idlugar"]);
                break;
        }
        $query = sprintf("select * from %s where idcurso = %d and %s and fecha = '%s'", BDPREFIX.$tabla, $this->db->secure_field($curso["idcurso"]), $idlugar, $this->db->secure_field($curso["fecha"]));
        $r = $this->db->query($query);

        if($this->db->count() > 0) {
            return $this->db->row(0);
        } else {
            return false;
        }
    }
}
// END