<?php
/**
 * Clase con metodos para operaciones con base de datos
 *
 * @package API
 * @author Trasno
 */
class Db {
    public $dbc;
    public $result;

    /**
     * Constructor
     *
     * @return void
     * @author trasno
     */
    public function __construct() {
        if(BDTYPE == "mysql") {
            $this->dbc = new mysqli(BDSERVER,BDUSER,BDPASS,BDNAME);
            if($this->dbc->connect_error) {
                throw new Exception("No se puede conectar con la base de datos.", 1);
            }
            $this->dbc->query("set names 'utf8'");
            $this->dbc->query("SET time_zone = '+2:00'");
        } else {
            throw new Exception("El tipo de base de datos no es correcto", 1);
        }
    }

    /**
     * Funcion de ejecucion de comandos SQL (INSERT, UPDATE...). ¡¡¡NO SELECT!!!
     *
     * @param string $query cadena con la operacion a realizar sobre la base de datos.
     * @return resulset $this->result identificador de consulta o resultset.
     */
    public function execute($query) {
        if(BDTYPE == "mysql" && strtolower(substr($query, 0, 6)) != "select") {
            if($this->result = $this->dbc->query($query)) {
                return $this->result;
            } else {
                throw new Exception("No se puede ejecutar la consulta (".$query."): ".$this->dbc->error, 1);
            }
        } else {
            throw new Exception("El tipo de base de datos no es correcto o la consulta es una select (utilizar query())", 1);
        }
    }

    /*
     * Funcion de consulta de base de datos (solo SELECT)
     *
     * @param string $query cadena con la operacion a realizar sobre la base de datos.
     * @return resulset $this->result identificador de consulta o resultset.
     */
    public function query($query) {
        if(BDTYPE == "mysql" && strtolower(substr($query, 0, 6)) == "select") {
            if($this->result = $this->dbc->query($query)) {
                return $this->result;
            } else {
            	//logFile($query);
                throw new Exception("No se puede ejecutar la consulta (".$query."): ".$this->dbc->error, 1);
            }
        } else {
            throw new Exception("El tipo de base de datos no es correcto o la consulta no es una select (utilizar execute())", 1);
        }
    }

    /*
     * Funcion que devuelve una fila del resulset pasado en forma de array asociativo
     *
     * @param resulset $this->result identificador de consulta o resultset.
     * @return array $array array asociativo con los datos de una fila de la consulta.
     */
    public function fetch() {
        if(BDTYPE == "mysql") {
            $array = $this->result->fetch_array(MYSQLI_ASSOC);
            return $array;
        } else {
            throw new Exception("El tipo de base de datos no es correcto", 1);
        }
    }

    /*
     * Funcion que devuelve el numero de resultados del resulset
     *
     * @param resulset $this->result identificador de consulta o resultset.
     * @return int $num numero de filas en el resultado de la consulta.
     */
    public function count() {
        if(BDTYPE == "mysql") {
            $num = $this->result->num_rows;
            return $num;
        } else {
            throw new Exception("El tipo de base de datos no es correcto", 1);
        }
    }

    /*
     * Funcion que devuelve un campo
     *
     * @param resulset $this->result identificador de consulta o resultset.
     * @param int $field numero de campo de la consulta.
     * @return string $res cadena con el contenido del campo especificado.
     */
    public function row($fieldNum) {
        if(BDTYPE == "mysql") {
            $row = $this->result->fetch_row();
            return $row[$fieldNum];
        } else {
            throw new Exception("El tipo de base de datos no es correcto", 1);
        }
    }

    /*
     * Funcion que devuelve el ultimo id que se ha insertado
     *
     * @return int $id id del ultimo elemento insertado
     */
    public function last_id() {
        if(BDTYPE == "mysql") {
            $id = $this->dbc->insert_id;
            return $id;
        } else {
            throw new Exception("El tipo de base de datos no es correcto", 1);
        }
    }

    /*
     * Funcion que trata la informacion a utilizar en la base de datos para evitar vulnerabilidades
     *
     * @param mixed $field datos a tratar.
     * @return mixed $this->dbc->real_escape_string($field) datos tratados para poder usados en una consulta SQL
     */
    public function secure_field($field) {
        if(BDTYPE == "mysql") {
            // Address Magic Quotes.
            if (ini_get('magic_quotes_gpc')) {
                $field = stripslashes($field);
            }
            return $this->dbc->real_escape_string($field);
        } else {
            throw new Exception("El tipo de base de datos no es correcto", 1);
        }
    }
}