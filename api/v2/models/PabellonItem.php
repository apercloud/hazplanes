<?php
/**
 * Clase con metodos para operaciones con pabellones
 *
 * @package API
 * @author Trasno
 */
class PabellonItem {
    protected $db;

    /**
     * Constructor
     *
     * @return void
     * @author trasno
     */
    public function __construct(Db $db) {
        $this->db = $db;
    }

    /**
     * Coge todos los pabellones
     *
     * @param array $options
     * @param int $options["limit"] (opcional) cuantos registros devolver
     * @param int $options["start"] (opcional) a partir de donde (posicion) se devuelve el listado
     * @param string $options["filter"] (opcional) cadena de filtro
     * @param string $options["order"] (opcional) campo por el que ordenar el listado.
     * @return array
     */
    public function listPabellones($options) {
        $limit = "";
        if(!empty($options["limit"])) {
            $limit = "limit ".$options["start"].",".$options["limit"];
        }
        if(!empty($options["filter"])) {
            $options["filter"] = "where ".$options["filter"];
        }
        $query = sprintf("select l.* from %spabellones l %s order by l.%s %s", BDPREFIX, $options["filter"], $this->db->secure_field($options["order"]), $this->db->secure_field($limit));
        $r = $this->db->query($query);

        $result = array();
        while($pabellon = $this->db->fetch($r)) {
            $result[] = $pabellon;
        }
        foreach($result as $k => $v) {
            //$result[$k]["promos"] = $this->getPromociones($pabellon["idpabellon"]);
            //$result[$k]["entradas"] = $this->getEntradas($pabellon["idpabellon"]);
        }
        return $result;
    }

    /**
     * Coge un pabellon y todos sus datos (salas, servicios...)
     *
     * @param int $idpabellon id del pabellon
     * @return array|false
     */
     public function dataPabellon($idpabellon) {
        if(!empty($idpabellon)) {
            $query = sprintf("select l.* from %spabellones l where l.idpabellon = %d", BDPREFIX, $this->db->secure_field($idpabellon));
            $r = $this->db->query($query);
            if($this->db->count($r) > 0) {
                $pabellon = $this->db->fetch($r);
                //$pabellon["promos"] = $this->getPromociones($pabellon["idpabellon"]);
                //$pabellon["entradas"] = $this->getEntradas($pabellon["idpabellon"]);
                return array(0 => $pabellon);
            } else {
                return false;
            }
        } else {
            throw new Exception("Parametro incorrecto", 1);
        }
    }

    /*
     * Eliminar un pabellon
     *
     * @param int $idpabellon id del pabellon
     * @return boolean
     */
    public function deletePabellon($idpabellon) {
        if(!empty($idpabellon)) {
            $query = sprintf("delete from %spabellones where idpabellon = %d",BDPREFIX, $this->db->secure_field($idpabellon));
            $r = $this->db->execute($query);
            if($r) {
                deleteAllExtImages("img/pabellones/".$idpabellon);
                deleteAllExtImages("img/pabellones/original/".$idpabellon);
                return true;
            }
        }
        return false;
    }

    /*
     * Insertar un pabellon
     *
     * @param array $pabellon datos del pabellon
     * @param string $pabellon['nombre'] nombre del pabellon
     * @param string $pabellon['direccion']
     * @param string $pabellon['localidad']
     * @param string $pabellon['provincia']
     * @param int $pabellon['codigopostal']
     * @param string $pabellon['coordenadas']
     * @param string $pabellon['telefono']
     * @param string $pabellon['email']
     * @param string $pabellon['web']
     * @param string $pabellon['twitter']
     * @param string $pabellon['facebook']
     * @param string $pabellon['google']
     * @param string $pabellon['otrasocial']
     * @param string $pabellon['notas']
     * @param string $pabellon['descripcion']
     * @param string $pabellon['imagen']
     * @return int
     */
    public function addPabellon($pabellon) {
        if(!$this->checkPabellon("nombre", $pabellon["nombre"])) {
            $fields = "";
            $values = "";
            foreach($pabellon as $key => $value) {
                if(!empty($fields)) {
                    $fields = $this->db->secure_field($fields).",";
                }
                $fields = $fields.$key;
                if(!empty($values)) {
                    $values = $values.",";
                }
                $values = $values."'".$this->db->secure_field($value)."'";
            }
            $now = date("Y-m-d H:i:s", strtotime("now"));
            $query = sprintf("insert into %spabellones (%s, creado) VALUES (%s, '%s')", BDPREFIX, $fields, $values, $now);
            $r = $this->db->execute($query);
            if($r) {
                $id = $this->db->last_id();
                return $id;
            } else {
                throw new Exception("[addPabellon] Error en la query: ".$query, 1);
            }
        } else {
            throw new Exception("[addPabellon] Ya existe el pabellon.", 1);
        }
    }

    /*
     * Actualiza los campos de un pabellon
     *
     * @param array $pabellon datos del pabellon
     * @param int $pabellon['idpabellon'] identificador del pabellon
     * @param string $pabellon['nombre'] nombre del pabellon
     * @param string $pabellon['direccion']
     * @param string $pabellon['localidad']
     * @param string $pabellon['provincia']
     * @param int $pabellon['codigopostal']
     * @param string $pabellon['coordenadas']
     * @param string $pabellon['telefono']
     * @param string $pabellon['email']
     * @param string $pabellon['web']
     * @param string $pabellon['twitter']
     * @param string $pabellon['facebook']
     * @param string $pabellon['google']
     * @param string $pabellon['otrasocial']
     * @param string $pabellon['notas']
     * @param string $pabellon['descripcion']
     * @param string $pabellon['imagen']
     * @return boolean
     */
    public function updatePabellon($pabellon) {
        if($this->checkPabellon("idpabellon", $pabellon["idpabellon"])) {
            $fields = "";
            foreach($pabellon as $key => $value) {
                if($key != "idpabellon") {
                    if(!empty($fields)) {
                        $fields = $fields.",";
                    }
                    $fields = $fields.$key."='".$this->db->secure_field($value)."'";
                }
            }
            $now = date("Y-m-d H:i:s", strtotime("now"));
            $query = sprintf("update %spabellones set %s, actualizado = '%s' where idpabellon = %d", BDPREFIX, $fields, $now, $this->db->secure_field($pabellon["idpabellon"]));
            $r = $this->db->execute($query);
            if($r) {
                return true;
            } else {
                throw new Exception("[updatePabellon] Error en la query: ".$query, 1);
            }
        } else {
            throw new Exception("[updatePabellon] No existe el pabellon.", 1);
        }
    }

    /*
     * Publicar pabellones para que sean visibles
     *
     * @param int $ids ids de los pabellones
     * @return boolean
     */
    public function publishPabellon($ids) {
        $query = sprintf("update %spabellones set publicado=1 where idpabellon in (%s)", BDPREFIX, $this->db->secure_field($ids));
        $r = $this->db->execute($query);
        if($r) {
            return true;
        } else {
            throw new Exception("[publishPabellon] Error en la query: ".$query, 1);
        }
    }

    /*
     * Despublicar pabellones para que no sean visibles
     *
     * @param int $ids ids de los pabellones
     * @return boolean
     */
    public function unpublishPabellon($ids) {
        $query = sprintf("update %spabellones set publicado=0 where idpabellon in (%s)", BDPREFIX, $this->db->secure_field($ids));
        $r = $this->db->execute($query);
        if($r) {
            return true;
        } else {
            throw new Exception("[unpublishPabellon] Error en la query: ".$query, 1);
        }
    }

    /*
     * Votar pabellones
     *
     * @param int $idlugar id del pabellon
     * @param int $points puntos que le dan
     * @return boolean
     */
    public function votePabellon($idlugar,$points) {
        $query = sprintf("update %spabellones set numvotos=numvotos+1, puntos=puntos+%d where idpabellon = %d", BDPREFIX, $this->db->secure_field($points), $this->db->secure_field($idlugar));
        $r = $this->db->execute($query);
        if($r) {
            return true;
        } else {
            throw new Exception("[votePabellon] Error en la query: ".$query, 1);
        }
    }

    /*
     * Comprueba si existe el pabellon
     *
     * @param string $field campo por el que comprobar
     * @param string $value valor que se va a comprobar en el campo
     * @return int|false
     */
    private function checkPabellon($field, $value) {
        $query = sprintf("select l.* from %spabellones l where l.%s = '%s'", BDPREFIX, $this->db->secure_field($field), $this->db->secure_field($value));
        $r = $this->db->query($query);

        if($this->db->count() > 0) {
            return $this->db->row(0);
        } else {
            return false;
        }
    }

    /*
     * Comprueba si existe la relacion pabellon-entradas
     *
     * @param int $idpabellon id del pabellon
     * @param int $identradas id del sitio de entradas
     * @return boolean
     */
    private function checkRelEntradas($idpabellon, $identradas) {
        $query = sprintf("select le.* from %spabellones_has_entradas le where le.idpabellon = %d and le.identradas = %d", BDPREFIX, $this->db->secure_field($idpabellon), $this->db->secure_field($identradas));
        $r = $this->db->query($query);

        if($this->db->count() > 0) {
            return true;
        } else {
            return false;
        }
    }

    /*
     * Comprueba si existe la relacion pabellon-promociones
     *
     * @param int $idpabellon id del pabellon
     * @param int $idpromocion id de la promocion
     * @return boolean
     */
    private function checkRelPromos($idpabellon, $idpromocion) {
        $query = sprintf("select lp.* from %spabellones_has_promociones lp where lp.idpabellon = %d and lp.idpromocion = %d", BDPREFIX, $this->db->secure_field($idpabellon), $this->db->secure_field($idpromocion));
        $r = $this->db->query($query);

        if($this->db->count() > 0) {
            return true;
        } else {
            return false;
        }
    }
}
// END