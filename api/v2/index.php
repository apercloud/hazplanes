<?php
include_once("../../includes/config.inc.php");
include_once("models/Db.php");
include_once("models/General.php");
include_once("models/CineItem.php");
include_once("models/ConciertoItem.php");
include_once("models/CursoItem.php");
include_once("models/DeporteItem.php");
include_once("models/EntradasItem.php");
include_once("models/EventoItem.php");
include_once("models/ExposicionItem.php");
include_once("models/LocalItem.php");
include_once("models/MuseoItem.php");
include_once("models/ObraItem.php");
include_once("models/PabellonItem.php");
include_once("models/PeliculaItem.php");
include_once("models/PromocionItem.php");
include_once("models/SalaItem.php");
include_once("models/ServiciosItem.php");
include_once("models/TeatroItem.php");

//file_put_contents(BASE_URI."temp/logfile.txt", $_REQUEST["agenda"]);
//wrap the whole thing in a try-catch block to catch any wayward exceptions!
//error_log("hoy ");
try {
    //get all of the parameters in the POST/GET request
    $params = $_REQUEST;

    //get the controller and format it correctly so the first
    //letter is always capitalized
    $controller = ucfirst(strtolower($params['controller']));
    unset($params["controller"]);

    //get the action and format it correctly so all the
    //letters are not capitalized, and append 'Action'
    $action = strtolower($params['action']).'Action';
    unset($params["action"]);

    //check if the controller exists. if not, throw an exception
    if( file_exists("controllers/{$controller}.php") ) {
        include_once "controllers/{$controller}.php";
    } else {
        throw new Exception('Controller is invalid.');
    }

    $db = new Db();
    //create a new instance of the controller, and pass
    //it the parameters from the request
    $controller = new $controller($params,$db);

    //check if the action exists in the controller. if not, throw an exception.
    if( method_exists($controller, $action) === false ) {
        throw new Exception('Action is invalid.');
    }

    //execute the action
    $result['data'] = $controller->$action();
    $result['success'] = true;

} catch( Exception $e ) {
    //catch any exceptions and report the problem
    $result = array();
    $result['success'] = false;
    $result['errormsg'] = $e->getMessage();
}

//echo the result of the API call
echo json_encode($result);
exit();
