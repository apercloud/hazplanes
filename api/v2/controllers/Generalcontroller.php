<?php
/**
 * Clase controlador de general
 *
 * @package API
 * @author Trasno
 */
class GeneralController {
    private $params;
    private $general;
    private $General;

    /**
     * Constructor
     *
     * @return void
     * @author trasno
     */
    public function __construct($params, Db $db) {
        $this->params = $params;
        $this->General = new General($db);
    }

    /**
     * Destructor
     *
     * @return void
     * @author trasno
     */
    public function __destruct() {
        $this->General = null;
    }

    /**
     * Crear general
     *
     * @return int
     */
    public function createAction() {
        $result = "";

        return $result;
    }

    /**
     * Recuperar general(s)
     *
     * @return array
     */
    public function readAction() {
        $data = "";


        if(isset($this->params["ayuntamientos"])) {
            $options = array();
            $data = $this->General->listAyuntamientos($options);
        } elseif(isset($this->params["agenda"])) {
            $options = array(
                "limit" => 0,
                "start" => 0,
                "filter" => "fecha >= NOW()",
                "order" => "fecha, nombre",
                );
            $options = array_merge($options,$this->params);
            $data = $this->General->listAgendaHoy($options);
        } elseif(isset($this->params["sharelinks"])) {
            $options = array(
                "limit" => 0,
                "start" => 0,
                "filter" => "fecha >= NOW()",
                "order" => "fecha, nombre",
                );
            $options = array_merge($options,$this->params);
            $data = $this->General->listShareLinksHoy($options);
        } elseif(isset($this->params["agendaMap"])) {
            $options = array(
                "limit" => 0,
                "start" => 0,
                "filter" => "fecha >= NOW()",
                "order" => "fecha, nombre",
                );
            $options = array_merge($options,$this->params);
            $data = $this->General->listAgendaHoyCoord($options);
        } elseif(isset($this->params["reserva"])) {
            $options = array(
                "limit" => 5,
                "start" => 0,
                "filter" => "fecha >= now() and reserva = 1",
                "order" => "limitereserva, fecha, nombre",
                );
            $options = array_merge($options,$this->params);
            $data = $this->General->listNovedades($options);
        } else {
            if(isset($this->params["novedades"]) && !empty($this->params["novedades"])) {
                if(isset($this->params["limite"]) && !empty($this->params["limite"])) {
                    $limit = $this->params["limite"];
                } else {
                    $limit = 25;
                }
                if($this->params["novedades"] == "creado") {
                    $options = array(
                        "limit" => $limit,
                        "start" => 0,
                        "filter" => "creado IS NOT NULL",
                        "order" => "creado desc",
                        );
                    $data = $this->General->listNovedades($options);
                } elseif($this->params["novedades"] == "actualizado") {
                    $options = array(
                        "limit" => $limit,
                        "start" => 0,
                        "filter" => "actualizado IS NOT NULL",
                        "order" => "actualizado desc",
                        );
                    $data = $this->General->listNovedades($options);
                }
            } elseif(isset($this->params["tipolugar"]) && !empty($this->params["tipolugar"])) {
                $options = array(
                    "limit" => 0,
                    "start" => 0,
                    "filter" => "fecha >= NOW()",
                    "order" => "fecha, nombre",
                    );
                $options = array_merge($options,$this->params);
                $data = $this->General->listNextEvents($options);
            }
        }

        return $data;
    }

    /**
     * Actualizar general
     *
     * @return boolean
     */
    public function updateAction() {
        $result = "";

        return $result;
    }

    /**
     * Eliminar general
     *
     * @return boolean
     */
    public function deleteAction() {
        $result = "";

        return $result;
    }
}
?>
