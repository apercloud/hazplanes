<?php

function sec_session_start() {
    $session_name = 'sec_session_id'; //Configura un nombre de sesión personalizado
    $secure = false; //Configura en verdadero (true) si utilizas https
    $httponly = true; //Esto detiene que javascript sea capaz de accesar la identificación de la sesión.
    ini_set('session.use_only_cookies', 1); //Forza a las sesiones a sólo utilizar cookies.
    $cookieParams = session_get_cookie_params(); //Obtén params de cookies actuales.
    session_set_cookie_params($cookieParams["lifetime"], $cookieParams["path"], $cookieParams["domain"], $secure, $httponly);
    session_name($session_name); //Configura el nombre de sesión al configurado arriba.
    session_start(); //Inicia la sesión php
    session_regenerate_id(true); //Regenera la sesión, borra la previa.
}

function page404() {
    header("location: ".BASE_URL."404");
    exit;
}

function logFile($data) {
    file_put_contents(BASE_URI."temp/logfile.txt", $data);
}

function pageAccessControl($admin = 0) {

    if($admin) {
        if(empty($_SESSION)) {
            if(isset($_COOKIE["user"]) && !empty($_COOKIE["user"])) {
                $_SESSION["user"] = $_COOKIE["user"];
            }
        }
        // Redirect if this page was accessed directly or without permissions:
        if (!defined('BASE_URL') || empty($_SESSION)) {

            // Need the BASE_URL, defined in the config file:
            require_once ('includes/config.inc.php');

            // Redirect to the index page:
            $url = BASE_URL . 'index.php?p=loginadmin';
            if (!headers_sent()) {
                header ("Location: $url");
                exit;
            } else {
                echo "<script>window.location.href='".$url."';</script>";
            }

        } // End of defined() IF.
    } else {
        // Redirect if this page was accessed directly:
        if (!defined('BASE_URL')) {

            // Need the BASE_URL, defined in the config file:
            require_once ('../includes/config.inc.php');

            // Redirect to the index page:
            $url = BASE_URL . 'index.php';
            if (!headers_sent()) {
                header ("Location: $url");
                exit;
            } else {
                echo "<script>window.location.href='".$url."';</script>";
            }

        } // End of defined() IF.
    }

}

function calculaDuracion($duracion) {
    if(is_numeric($duracion)) {
        $hour = floor($duracion / 60);
        $min = $duracion % 60;

        if($min > 0) {
            if($hour > 1) {
                $result = $hour." horas y ".$min." minutos";
            } else if($hour == 1) {
                $result = $hour." hora y ".$min." minutos";
            } else {
                $result = $min." minutos";
            }
        } else {
            if($hour > 1) {
                $result = $hour." horas";
            } else if($hour == 1) {
                $result = $hour." hora";
            }
        }
    } else {
        $result = $duracion;
    }

    return $result;
}

function tiempo_transcurrido($fecha) {
    if(empty($fecha)) {
        return "No hay fecha";
    }

    $intervalos = array("segundo", "minuto", "hora", "día", "semana", "mes", "año");
    $duraciones = array("60","60","24","7","4.35","12");

    $ahora = strtotime("now");
    $Fecha_Unix = strtotime($fecha);

    if(empty($Fecha_Unix)) {
        return "Fecha incorrecta";
    }
    if($ahora > $Fecha_Unix) {
        $diferencia     = $ahora - $Fecha_Unix;
        $tiempo         = "Hace";
    } else {
        $diferencia     = $Fecha_Unix - $ahora;
        $tiempo         = "Dentro de";
    }
    for($j = 0; $diferencia >= $duraciones[$j] && $j < count($duraciones)-1; $j++) {
        $diferencia /= $duraciones[$j];
    }

    $diferencia = round($diferencia);

    if($diferencia != 1) {
        $intervalos[5].="e"; //meses... la magia del español
        $intervalos[$j].= "s";
    }

    return "$tiempo $diferencia $intervalos[$j]";
}

function urlAmigable($cadena) {
    // Tranformamos todo a minusculas
    $url = strtolower($cadena);

    //Rememplazamos caracteres especiales latinos
    $find = array('á', 'é', 'í', 'ó', 'ú', 'ñ', 'ü');
    $repl = array('a', 'e', 'i', 'o', 'u', 'n', 'u');
    $url = str_replace ($find, $repl, $url);

    // Añadimos los guiones
    $find = array(' ', '&', '\r\n', '\n', '+');
    $url = str_replace ($find, '-', $url);

    // Eliminamos y Reemplazamos demás caracteres especiales
    $find = array('/[^a-z0-9\-<>]/', '/[\-]+/', '/<[^>]*>/');
    $repl = array('', '-', '');
    $url = preg_replace ($find, $repl, $url);

    return $url;
}

function getShortURL($postData) {
    $curlObj = curl_init();

    $jsonData = json_encode($postData);

    //Server Key
    $apiKey = 'AIzaSyCyN2ufzwUOxz_nKao3DZ1ntpuOZLl9yGU';

    curl_setopt($curlObj, CURLOPT_URL, 'https://www.googleapis.com/urlshortener/v1/url?key='.$apiKey);
    curl_setopt($curlObj, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curlObj, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($curlObj, CURLOPT_HEADER, 0);
    curl_setopt($curlObj, CURLOPT_HTTPHEADER, array('Content-type:application/json'));
    curl_setopt($curlObj, CURLOPT_POST, 1);
    curl_setopt($curlObj, CURLOPT_POSTFIELDS, $jsonData);

    $response = curl_exec($curlObj);

    //change the response json string to object
    $json = json_decode($response);
    curl_close($curlObj);

    return $json;
}

function paginacion($pag,$total,$url,$order = "") {
    ?>
    <div class='paginacion'>
        <table style='width: 300px; margin: auto;'>
        <tr>
            <td style='width: 100px;'>
                <?php if($pag > 0) { ?>
                <a href="index.php?p=<?php echo $url; ?>&pag=<?php echo $pag-1; echo !empty($order)?"&order=".$order:""; ?>"><< Anterior</a>
                <?php } ?>
            </td>
            <td style='width: 100px;'>
                <?php if($total > 49) { ?>
                <a href="index.php?p=<?php echo $url; ?>&pag=<?php echo $pag+1; echo !empty($order)?"&order=".$order:""; ?>">Siguiente >></a>
                <?php } ?>
            </td>
        </tr>
        </table>
    </div>
    <?php
}

function infoHoras($d) {
    global $db;
    global $dias;
    ?>
    <h3>Horarios <span style="font-weight: normal; font-size: 12px;">(click en la fecha para ver)</span></h3>
    <div id="info-horas">
        <?php
        $eventname = $d["nombre"];
		if(isset($d["idpelicula"])) {
			$url = "pelicula/".$d["idpelicula"];
			$eventtype = "Película";
		} else if(isset($d["idconcierto"])) {
			$url = "concierto/".$d["idconcierto"];
			$eventtype = "Concierto";
		} else if(isset($d["idexposicion"])) {
			$url = "exposicion/".$d["idexposicion"];
			$eventtype = "Exposición";
		} else if(isset($d["idobrateatro"])) {
			$url = "obra/".$d["idobrateatro"];
			$eventtype = "Teatro";
		} else if(isset($d["idcurso"])) {
			$url = "curso/".$d["idcurso"];
			$eventtype = "Formación";
		} else if(isset($d["ideventos"])) {
			$url = "evento/".$d["ideventos"];
			$eventtype = "Evento";
		} else if(isset($d["iddeporte"])) {
			$url = "competicion/".$d["iddeporte"];
			$eventtype = "Deporte";
		}
		?>
		<span itemprop="url" style='display:none;'>https://hazplanes.com/<?php echo $url."-".urlAmigable($eventname); ?></span>
		<?php

        $datetime2 = date_create("today");
        //Horarios
        $list = $listTemp = array();
        $idlocales = $idlocalesTemp = array();
        $tipohorarios = array("horasLocal", "horasAuditorio", "horasCine", "horasLugar", "horasMuseo", "horasPabellon");
        $clt = array(); //Aqui guardamos la direccion de todos los cines, locales y teatros para mostrarla luego
        foreach($tipohorarios as $th) {
            if(isset($d[$th])) {
                switch($th) {
                    case "horasCine":
                        $idtipo = "idcine";
                        $cine = new Cines("",$db);
                        $allCines = $cine->readAction();
                        foreach($allCines as $v) {
                            $clt[$v["nombre"]] = $v["direccion"].", ".$v["codigopostal"]." ".$v["localidad"];
                        }
                        break;
                    case "horasLocal":
                        $idtipo = "idlocal";
                        $local = new Locales("",$db);
                        $allLocales = $local->readAction();
                        foreach($allLocales as $v) {
                            $clt[$v["nombre"]] = $v["direccion"].", ".$v["codigopostal"]." ".$v["localidad"];
                        }
                        break;
                    case "horasAuditorio":
                        $idtipo = "idteatro";
                        $teatro = new Teatros("",$db);
                        $allTeatros = $teatro->readAction();
                        foreach($allTeatros as $v) {
                            $clt[$v["nombre"]] = $v["direccion"].", ".$v["codigopostal"]." ".$v["localidad"];
                        }
                        break;
                    case "horasLugar":
                        $idtipo = "idlu";
                        break;
                    case "horasMuseo":
                        $idtipo = "idmuseo";
                        $museo = new Museos("",$db);
                        $allMuseos = $museo->readAction();
                        foreach($allMuseos as $v) {
                            $clt[$v["nombre"]] = $v["direccion"].", ".$v["codigopostal"]." ".$v["localidad"];
                        }
                        break;
                    case "horasPabellon":
                        $idtipo = "idpabellon";
                        $pabellon = new Pabellones("",$db);
                        $allPabellones = $pabellon->readAction();
                        foreach($allPabellones as $v) {
                            $clt[$v["nombre"]] = $v["direccion"].", ".$v["codigopostal"]." ".$v["localidad"];
                        }
                        break;
                }
                foreach($d[$th] as $k => $hora) {
                    if($idtipo == "idlu") {
                        $hora["nombre"] = $hora["lugar"]." (".$hora["localidad"].")";
                    }
                    $datetime1 = date_create($hora["fecha"]);
                    //$h = date("d-m-Y",strtotime($hora["fecha"]));
                    $h = strtotime(substr($hora["fecha"],0,10));
                    $horaevento = date("H:i",strtotime($hora["fecha"]));
					if($th == "horasCine") {
                        $temp = array(
                            "sala" => $hora["numero"],
                            "3d" => $hora["3d"]? "3D":"",
                            "vo" => $hora["vo"]? "V.O.S.E.":"",
                            "urlcompra" => $hora["urlcompra"]);
                    } else if($th == "horasLugar") {
                        $temp = array(
                            "precioanticipada" => $hora["precioanticipada"],
                            "duracion" => !empty($hora["duracion"])?$hora["duracion"]:"--");
                    } else {
                        $temp = array(
                            "precioanticipada" => $hora["precioanticipada"],
                            "urlcompra" => $hora["urlcompra"]);
                    }
					if(isset($hora["vo"])) {
						$temp["vo"] = $hora["vo"];
					}
                    $temp2 = array(
                            "th"=> $th,
                            "hora" => $horaevento,
                            "agotado" => $hora["agotado"]? "Si":"No",
                            "cancelado" => $hora["cancelado"]? "Si":"No",
                            "tododia" => $hora["todoeldia"],
                            "precio" => $hora["precio"]);
                    if($datetime1 >= $datetime2) {
                        if(!isset($list[$h][$hora["nombre"]])) {
                            $list[$h][$hora["nombre"]] = array();
                        }
                        $list[$h][$hora["nombre"]][] = array_merge($temp,$temp2);
                        if(isset($hora[$idtipo])) {
                            $idlocales[$hora["nombre"]] = $hora[$idtipo];
                        }
                    } else {
                        if(!isset($listTemp[$h][$hora["nombre"]])) {
                            $listTemp[$h][$hora["nombre"]] = array();
                        }
                        $listTemp[$h][$hora["nombre"]][] = array_merge($temp,$temp2);
                        if(isset($hora[$idtipo])) {
                            $idlocalesTemp[$hora["nombre"]] = $hora[$idtipo];
                        }
                    }
                }
            }
        }
        if(count($list) < 1) {
            $list = $listTemp;
            $idlocales = $idlocalesTemp;
        }
        if(count($list) > 0) {
            $cont = 0;
            $hoy = date("d-m-Y", strtotime("now"));
            $mañana = date("d-m-Y", strtotime("+1 day"));
            ksort($list);
            foreach($list as $fecha => $locales) {
                $cont++;
                $display = "";
                $fecha = date("d-m-Y",$fecha);
                if(($cont < 2 && $fecha > $hoy) || $fecha == $hoy) {
                    $display = " style='display: block;'";
                }
                $timevar = date("Y-m-d",strtotime($fecha));
                if($timevar == $hoy) {
                    $f = "Hoy";
                } elseif($timevar == $mañana) {
                    $f = "Mañana";
                } else {
                    $f = $fecha;
                }
                $f = $dias[date("N",strtotime($fecha))-1]." (".$f.")";
                ?>
                <div class="or-spacer">
                    <div class="mask"></div>
                    <span><i><?php echo $f; ?></i></span>
                </div>
                <?php
                echo "<div class='info-horas-fecha-detail' ".(!empty($display)?$display:"").">";
                $nombre = "";
                foreach($locales as $local => $horas) {
                    if($horas[0]["th"] == "horasLocal") {
                        $page = "local";
                    } else if($horas[0]["th"] == "horasAuditorio") {
                        $page = "auditorio";
                    } else if($horas[0]["th"] == "horasCine") {
                        $page = "cine";
                    } else if($horas[0]["th"] == "horasMuseo") {
                        $page = "museo";
                    } else if($horas[0]["th"] == "horasPabellon") {
                        $page = "pabellon";
                    } else {
                        $page = "";
                    }
                    if($nombre != $local) {
                        echo "<div class='celllocal' itemprop='location'><span>".$local."</span> ";
                        if($horas[0]["th"] != "horasLugar") {
                            echo "<font size='2'>(<span>".$clt[$local]."</span>)</font> ";
                            echo "<a href='".BASE_URL.$page."/".$idlocales[$local]."-".urlAmigable($local)."' class='nota'>+ info</a>";
                        }
                        echo "</div>";
                        echo "<div class='hourtable'>";
                    }
                    $temp = array();
                    foreach($horas as $k => $d2) {
                        if(date("G",strtotime($d2["hora"])) < 6 && (date("H:i:s",strtotime($d2["hora"])) != "00:00:00" || $page == "cine")) {
                            array_push($temp, $d2);
                            unset($horas[$k]);
                        }
                    }
                    $horas = array_merge($horas, $temp);
                    foreach($horas as $hora) {
                        $classInfo = "";
                        if($hora["agotado"] == "Si") {
                            $classInfo = "agotado";
                        }
                        if($hora["cancelado"] == "Si") {
                            $classInfo = "cancelado";
                        }
                        echo "<div class='row'>";
                        if($hora["tododia"]) {
                            $horaevento = "Todo el día";
                            if($hora["hora"] != "00:00") {
                                $horaevento .= " desde las ".$hora["hora"];
                            }
                        } else {
                            $horaevento = $hora["hora"];
                            if($horaevento == "00:00" && $page != "cine") {
                                $horaevento = "Sin determinar";
                            }
                        }

						//Poner boton de recordatorio en Google Calendar
						$year = date("Y",strtotime($fecha));
						$inicio_invierno = strtotime("last Sunday April $year");
    					$fin_invierno = strtotime("last Sunday November $year");
						$thisDay = strtotime(date("d-m-Y",strtotime($fecha)));
    					if($thisDay > $inicio_invierno && $thisDay <= $fin_invierno) {
    						$gcalHoraIni = date("His",strtotime($hora["hora"]." -2 hour"));
							$gcalHoraFin = date("His",strtotime($hora["hora"]." -1 hour"));
						} else {
							$gcalHoraIni = date("His",strtotime($hora["hora"]." -1 hour"));
							$gcalHoraFin = date("His",strtotime($hora["hora"]." -0 hour"));
    					}
						echo "<a href='https://www.google.com/calendar/render?action=TEMPLATE&text=(".$eventtype.")+".$eventname."+-+HazPlanes&dates=".date("Ymd",strtotime($fecha))."T".$gcalHoraIni."Z/".date("Ymd",strtotime($fecha))."T".$gcalHoraFin."Z&ctz=Europe/Madrid&details=Más+información:+https://hazplanes.com/".$url."-".urlAmigable($eventname)."&location=".$local."' target='_blank'><div class='cellSetAlarm'><img src='img/interface/alarm.png' alt='Guardar en Google Calendar title='Guardar en Google Calendar'/></div></a>";
						//Fin boton de recordatorio en Google Calendar

						//Quitamos la sala porque en el nuevo reservaentradas es un coñazo coger esta info y tampoco hace tanta falta
                        /*if($page == "cine") {
                            echo "    <div class='cellsala'><span>Sala ".$hora["sala"]."</span></div>";
                        }*/
                        echo "    <div class='cellhora ".$classInfo."'><time itemprop='startDate' datetime='".$timevar."T".$hora["hora"]."'>".$horaevento."</time></div>";
                        $libre = false;
                        $text = $text2 = "";
                        if($hora["precio"] == "Entrada libre") {
                            $libre = true;
                        } else {
                            if(empty($hora["precio"]) || $hora["precio"] === "0") {
                                if($hora["precio"] !== "0" && $hora["precio"] !== 0) {
                                    $text = "";
                                } else {
                                    $text = "Gratis";
                                    $libre = true;
                                }
                            } else {
                                if(is_numeric(substr($hora["precio"],-1))) {
                                    $text = $hora["precio"]." €";
                                } else {
                                    $text = $hora["precio"];
                                    if($hora["precio"] == "Gratis" || $hora["precio"] == "Entrada libre") {
                                        $libre = true;
                                    }
                                }
                            }
                            if(!empty($text)) {
                                echo "    <div class='cellprecio' style='min-width: 64px;'>";
                                echo $text;
                                echo "</div>";
                            }
                        }
                        if($page != "cine") {
                            if(empty($hora["precioanticipada"]) || $hora["precioanticipada"] == "0" || $hora["precioanticipada"] == "Entrada libre") {
                                if($hora["precioanticipada"] !== "0" && $hora["precioanticipada"] !== 0) {
                                    $text2 = "";
                                } else {
                                    $text2 = "Anticipada: Gratis";
                                    $libre = true;
                                }
                            } else {
                                if(is_numeric(substr($hora["precioanticipada"],-1))) {
                                    $text2 = "Anticipada: ".$hora["precioanticipada"]." €";
                                } else {
                                    $text2 = "Anticipada: ".$hora["precioanticipada"];
                                    if($hora["precioanticipada"] == "Gratis" || $hora["precioanticipada"] == "Entrada libre") {
                                        $libre = true;
                                    }
                                }
                            }
                            if(!empty($text2)) {
                                echo "    <div class='cellprecio' style='min-width: 250px;'>";
                                echo $text2;
                                echo "</div>";
                            }
                        }
                        if($horas[0]["th"] == "horasLugar") {
                            echo "    <div class='cellduracion'><span itemprop='duration' content='PT".(floor($hora["duracion"] / 60))."H".($hora["duracion"] % 60)."M'>".calculaDuracion($hora["duracion"])." (".$hora["duracion"]." min)</span></div>";
                        } else {
                            if($libre) {
                                echo "  <div class='cellentradas'><span>Entrada libre</span></div>";
                            } else {
                                if(empty($hora["urlcompra"])) {
                                    if(empty($text) && empty($text2)) {
                                        echo "  <div class='cellentradas'>Precio desconocido</div>";
                                    } else {
                                        echo "  <div class='cellentradas'><span>Taquilla</div>";
                                    }
                                } else {
                                    echo "  <div class='cellentradas'><a href='".$hora["urlcompra"]."' target='_blank'><img src='img/interface/ticketadd-26.png' alt='Comprar entrada'/></a></div>";
                                }
                            }
                        }
                        if(isset($hora["3d"])) {
                            echo "    <div class='cell3d'>".$hora["3d"]."</div>";
						}
						if(isset($hora["vo"])) {
                            echo "    <div class='cellvose'>".($hora["vo"]?"V.O.S.E":"")."</div>";
                        }
                        echo "</div>";
						?>
						<script type="application/ld+json">
						{
						  "@context": "http://schema.org",
						  "@type": "Event",
						  "name": "<?php echo addslashes($eventname); ?>",
						  "startDate" : "<?php echo $timevar."T".$hora["hora"]; ?>",
						  "url" : "https://hazplanes.com/<?php echo $url."-".urlAmigable($eventname); ?>",
						  "description" : "<?php echo isset($d["sinopsis"])?preg_replace('/\s+/', ' ',addslashes(htmlspecialchars_decode($d["sinopsis"]))):preg_replace('/\s+/', ' ',addslashes(htmlspecialchars_decode($d["descripcion"]))); ?>",
						  "image" : "https://hazplanes.com/<?php echo $d["poster"]; ?>",
						  "location" : {
						    "@type" : "Place",
						    "name" : "<?php echo addslashes($local); ?>",
                        	<?php if($horas[0]["th"] != "horasLugar") { ?>
                        		"address" : "<?php echo addslashes($clt[$local]); ?>",
                        		"url" : "<?php echo BASE_URL.$page."/".$idlocales[$local]."-".urlAmigable($local); ?>"
                    		<?php } else { ?>
                    			"address" : "<?php echo $local; ?>"
                    		<?php } ?>
						  }
						}
						</script>
						<?php
                    }
                    if($nombre != $local) {
                        echo "</div>";
                    }
                    $nombre = $local;
                }
                echo "</div>";
            }
        }
        ?>
        <p style='padding: 0 20px; text-align: right;'>
            <span class='agotado' style='height:30px; width:30px; display: inline-block; vertical-align: middle'></span> Agotado
            <span class='cancelado' style='height:30px; width:30px; display: inline-block; vertical-align: middle'></span> Cancelado
        </p>
    </div>
    <?php
}

function infoMapLugares($d) {
    if(isset($d["horasLugar"]) && count($d["horasLugar"]) > 0) {
        $cont = 0;
        $puntos = "";
        $norepes = array();
		$vermapa = false; 
		foreach($d["horasLugar"] as $map) {
			$mapfecha = 0;
			if(isset($map["fecha"])) {
				$mapfecha = strtotime(date("d-m-Y",strtotime($map["fecha"])));
			} elseif(isset($map["inicio"])) {
				$mapfecha = strtotime($map["inicio"]);
			}
        	if($mapfecha >= strtotime("today")) {
	            $map["coordenadas"] = trim($map["coordenadas"],"\(\)");
	            if(!in_array($map["lugar"],$norepes) && !empty($map["coordenadas"])) {
	                $norepes[] = $map["lugar"];
	                $cont++;
	                $coord = explode(",",$map["coordenadas"]);
	                ?>
	                <h3><img src="https://maps.google.com/mapfiles/kml/paddle/<?php echo $cont; ?>.png" height="40" alt="Marcador <?php echo $cont; ?>"/> <?php echo $map["lugar"];?> <span class='nota'><a style='color:rgba(158, 28, 28, 1);' href="https://www.google.es/maps/@<?php echo $coord[0] ?>,<?php echo trim($coord[1]) ?>,16z" target="_blank">Abrir en Google Maps</a></span></h3>
	                <?php
	                $puntos .= "setLugarPoints('".$coord[0]."','".$coord[1]."',$cont);";
	            }
				$vermapa = true;
			}
        }
		if($vermapa) {
	        ?>
	        <div id="map_canvas"></div>
	        <script>
	            $(document).ready(function() {
	                initViewClear();
	                <?php echo $puntos; ?>
	            });
	        </script>
	        <?php
		}
    }
}

function lugarNextEvents($d, $tipo, $id, $prev = false) {
    global $db;
    global $display;

    if(!$prev) {
        $options = array(
            "filter" => "fecha >= now() and publicado = 1",
            "tipolugar" => $tipo,
            "idlugar" => $d[$id]
            );
    } else {
        $options = array(
            "filter" => "fecha < now() and publicado = 1",
            "order" => "fecha desc",
            "tipolugar" => $tipo,
            "idlugar" => $d[$id],
            "limit" => 24
        );
    }
    $general = new GeneralController($options,$db);
    $data = $general->readAction();
    foreach($data as $k => $da) {
        $ids[] = $da["nombre"];
        $img = "";
        $type = $da["tipoevento"];
        if($type == "deportes") {
            $img = "img/deportes/tipos/mix.png";
            if(!empty($da["genero"])) {
                $options2 = array("read" => "datatipodeporte", "idtipodeporte" => $da["genero"]);
                $tipodeporte = new Deportes($options2,$db);
                $d2 = $tipodeporte->readAction();
                if(!empty($d2)) {
                    $d2 = $d2[0];
                    if(!empty($d2["poster"])) {
                        $img = "img/deportes/tipos/".$d2["poster"];
                    }
                    $da["genero"] = $d2["nombre"];
                } else {
                    $da["genero"] = "Sin especificar";
                }
            } else {
                $da["genero"] = "Sin especificar";
            }
        }
        if(!empty($da["poster"])) {
            if(strpos($da["poster"],'/') !== false) {
                $img = explode("/",$da["poster"]);
                $img[4] = $img[3];
                $img[3] = "thumbs";
                $img = implode("/",$img);
            }
        }
        if(!$prev) {
            $datetime1 = date_create($da["fecha"]);
            $datetime2 = date_create("now");
            $interval = date_diff($datetime1, $datetime2);
            $faltaFormat = "";
            $faltaText = "";
            if($interval->format('%m') > 0) {
                if($interval->format('%m') > 1) {
                    $faltaText = "Faltan";
                    $faltaFormat = "%m meses ";
                } else {
                    $faltaText = "Falta";
                    $faltaFormat = "%m mes ";
                }
            }
            if($interval->format('%d') > 0) {
                if($interval->format('%d') > 1) {
                    if($faltaText == "") {
                        $faltaText = "Faltan";
                    }
                    $faltaFormat .= "%d días ";
                } else {
                    if($faltaText == "") {
                        $faltaText = "Falta";
                    }
                    $faltaFormat .= "%d día ";
                }
            }
            $faltaFormat .= "%h:%I";
            if($faltaText == "") {
                if($interval->format('%h') > 1) {
                    $faltaText = "Faltan";
                } else if($interval->format('%h') < 1 && $interval->format('%I') > 1) {
                    $faltaText = "Faltan";
                } else {
                    $faltaText = "Falta";
                }
            }
        } else {
            $da["hora"] = date("d-m-Y H:i",strtotime($da["fecha"]));
        }
        $imgnull = "";
        switch($type) {
            case "peliculas":
                $class = "film";
                $imgnull = "null-film.png";
                $type = "PELICULA";
                if(!$prev) {
                    $da["hora"] = "Primera sesión: ".date("H:i",strtotime($da["fecha"]))." (".$faltaText.": ".$interval->format($faltaFormat).")";
                }
                $link = "pelicula/";
            break;
            case "conciertos":
                $class = "concert";
                $imgnull = "null-music.png";
                $type = "CONCIERTO";
                if(!$prev) {
                    $da["hora"] = "Hora: ".date("H:i",strtotime($da["fecha"]))." (".$faltaText.": ".$interval->format($faltaFormat).")";
                }
                $link = "concierto/";
            break;
            case "obrasteatro":
                $class = "theater";
                $imgnull = "null-theatre.png";
                $type = "TEATRO";
                if(!$prev) {
                    $da["hora"] = "Hora: ".date("H:i",strtotime($da["fecha"]))." (".$faltaText.": ".$interval->format($faltaFormat).")";
                }
                $link = "obra/";
            break;
            case "eventos":
                $class = "event";
                $imgnull = "null-calendar.png";
                $type = "EVENTO";
                if(!$prev) {
                    $da["hora"] = "Hora: ".date("H:i",strtotime($da["fecha"]))." (".$faltaText.": ".$interval->format($faltaFormat).")";
                }
                $link = "evento/";
            break;
            case "exposiciones":
                $class = "expo";
                $imgnull = "null-museum.png";
                $type = "EXPOSICIÓN";
                if(!$prev) {
                    $da["hora"] = "En horario del museo";
                } else {
                    $da["hora"] = "Terminó el ".date("d-m-Y",strtotime($da["fecha"]));
                }
                $link = "exposicion/";
            break;
            case "deportes":
                $class = "sport";
                $imgnull = "null-sports.png";
                $type = "DEPORTE";
                if(!$prev) {
                    $da["hora"] = "Hora: ".date("H:i",strtotime($da["fecha"]))." (".$faltaText.": ".$interval->format($faltaFormat).")";
                }
                $link = "competicion/";
            break;
            case "formacion":
                $class = "course";
                $imgnull = "null-cursos.png";
                $type = "FORMACIÓN";
                if(!$prev) {
                    $da["hora"] = "Hora: ".date("H:i",strtotime($da["fecha"]))." (".$faltaText.": ".$interval->format($faltaFormat).")";
                }
                $link = "curso/";
            break;
        }
        if(empty($img) || !file_exists($img)) {
            $img = "img/interface/".$imgnull;
        }
        if(empty($display)) {
            $display = "celda density-normal";
        }
        echo "<li class='".$display." ".$class."' data-category='".$class."' style='background: rgba(58, 58, 58, 0.9) url(../".$img.") no-repeat center'>";
        $infostatus = "";
        if($da["cancelado"]) {
            echo "<div class='ribbon-wrapper'><div class='ribbon-cancelado'>CANCELADO</div></div>";
            $infostatus = "cancelado";
        } else {
           if($da["agotado"]) {
               echo "<div class='ribbon-wrapper'><div class='ribbon-agotado'>AGOTADO</div></div>";
               $infostatus = "agotado";
           } else {
               if($da["infantil"]) {
                   echo "<div class='ribbon-wrapper'><div class='ribbon-infantil'>NIÑOS</div></div>";
                   $infostatus = "infantil";
               }
           }
        }
        echo "        <div class='grid-info ".$infostatus."'>
                    <a href='".$link.$da["id"]."-".urlAmigable($da["nombre"])."'>".$da["nombre"]."</a>
                    <br>
                    <span class='genre'>".$da["genero"]."</span>
                    <br>
                    <span class='hour'>".$da["hora"]."</span>
                    <span class='type'>".$type."</span>
                </div>
            </li>";
    }
}

?>
