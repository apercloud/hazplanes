<?php
include_once("config.inc.php");

include_once(APIPATH."models/Db.php");
include_once(APIPATH."models/General.php");
include_once(APIPATH."models/CineItem.php");
include_once(APIPATH."models/ConciertoItem.php");
include_once(APIPATH."models/CursoItem.php");
include_once(APIPATH."models/DeporteItem.php");
include_once(APIPATH."models/EntradasItem.php");
include_once(APIPATH."models/EventoItem.php");
include_once(APIPATH."models/ExposicionItem.php");
include_once(APIPATH."models/LocalItem.php");
include_once(APIPATH."models/MuseoItem.php");
include_once(APIPATH."models/ObraItem.php");
include_once(APIPATH."models/PabellonItem.php");
include_once(APIPATH."models/PeliculaItem.php");
include_once(APIPATH."models/PromocionItem.php");
include_once(APIPATH."models/SalaItem.php");
include_once(APIPATH."models/ServiciosItem.php");
include_once(APIPATH."models/TeatroItem.php");

include_once(APIPATH."controllers/Cines.php");
include_once(APIPATH."controllers/Conciertos.php");
include_once(APIPATH."controllers/Cursos.php");
include_once(APIPATH."controllers/Deportes.php");
include_once(APIPATH."controllers/Entradas.php");
include_once(APIPATH."controllers/Eventos.php");
include_once(APIPATH."controllers/Exposiciones.php");
include_once(APIPATH."controllers/Generalcontroller.php");
include_once(APIPATH."controllers/Locales.php");
include_once(APIPATH."controllers/Museos.php");
include_once(APIPATH."controllers/Obras.php");
include_once(APIPATH."controllers/Pabellones.php");
include_once(APIPATH."controllers/Peliculas.php");
include_once(APIPATH."controllers/Promociones.php");
include_once(APIPATH."controllers/Salas.php");
include_once(APIPATH."controllers/Servicios.php");
include_once(APIPATH."controllers/Teatros.php");

$db = new Db();
