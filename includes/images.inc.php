<?php
/************************************************************************
 *          Libreria de funciones para manipulacion de imagenes
 *          Modificado: 20/03/2014
 ************************************************************************/

/*
 * Funcion que escala una imagen manteniendo sus proporciones.
 * Parametros:
 *      $originalImage: ruta de la imagen original
 *      $toWidth: ancho maximo al que redimensionar
 *      $toHeight: alto maximo al que redimensionar
 * Salidas:
 *      $imageResized: la imagen redimensionada
 */
function resizeImage($originalImage,$toWidth,$toHeight){
    // Get the original geometry and calculate scales
    list($width, $height, $type) = getimagesize($originalImage);
    $xscale = $width/$toWidth;
    if(!empty($toHeight)) {
        $yscale = $height/$toHeight;
        if($yscale < 1) {
            $yscale = 1;
        }
    } else {
        $yscale = 1;
    }
    if($xscale < 1) {
        $xscale = 1;
    }
    // Recalculate new size with default ratio
    if ($yscale > $xscale){
        $new_width = round($width * (1/$yscale));
        $new_height = round($height * (1/$yscale));
    } else {
        $new_width = round($width * (1/$xscale));
        $new_height = round($height * (1/$xscale));
    }
    // Resize the original image
    $imageResized = imagecreatetruecolor($new_width, $new_height);
    if( $type == IMAGETYPE_JPEG ) {
        $imageTmp = imagecreatefromjpeg ($originalImage);
    } elseif( $type == IMAGETYPE_GIF ) {
        $imageTmp = imagecreatefromgif ($originalImage);
    } elseif( $type == IMAGETYPE_PNG ) {
        $imageTmp = imagecreatefrompng ($originalImage);
    }
    imagecopyresampled($imageResized, $imageTmp, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
    if( $type == IMAGETYPE_JPEG ) {
        imagejpeg($imageResized,$originalImage);
    } elseif( $type == IMAGETYPE_GIF ) {
        imagegif($imageResized,$originalImage);
    } elseif( $type == IMAGETYPE_PNG ) {
        imagepng($imageResized,$originalImage);
    }
    return $imageResized;
}

/*
 * Funcion que escala y recorta centrada una imagen manteniendo sus proporciones.
 * Parametros:
 *      $originalImage: ruta de la imagen original
 *      $toWidth: ancho minimo al que redimensionar
 *      $toHeight: alto minimo al que redimensionar
 * Salidas:
 *      $imageResized: la imagen redimensionada
 */
function resizeAndCropImage($originalImage,$toWidth,$toHeight) {
    // Get the original geometry and calculate scales
    list($width, $height, $type) = getimagesize($originalImage);
    $new_width = $height * $toWidth / $toHeight;
    $new_height = $width * $toHeight / $toWidth;

    if($toWidth > $width) {
        $toWidth = $width;
    }
    if($toHeight > $height) {
        $toHeight = $height;
    }

    // Resize the original image
    $imageResized = imagecreatetruecolor($toWidth, $toHeight);
    if( $type == IMAGETYPE_JPEG ) {
        $imageTmp = imagecreatefromjpeg ($originalImage);
    } elseif( $type == IMAGETYPE_GIF ) {
        $imageTmp = imagecreatefromgif ($originalImage);
    } elseif( $type == IMAGETYPE_PNG ) {
        $imageTmp = imagecreatefrompng ($originalImage);
    }
    if($new_width > $width) {
        $h_point = (($height - $new_height) / 2);
        imagecopyresampled($imageResized, $imageTmp, 0, 0, 0, $h_point, $toWidth, $toHeight, $width, $new_height);
    } else {
        $w_point = (($width - $new_width) / 2);
        imagecopyresampled($imageResized, $imageTmp, 0, 0, $w_point, 0, $toWidth, $toHeight, $new_width, $height);
    }
    if( $type == IMAGETYPE_JPEG ) {
        imagejpeg($imageResized,$originalImage);
    } elseif( $type == IMAGETYPE_GIF ) {
        imagegif($imageResized,$originalImage);
    } elseif( $type == IMAGETYPE_PNG ) {
        imagepng($imageResized,$originalImage);
    }
    return $imageResized;
}

/*
 * Funcion que sube una imagen a un album del sistema.
 * @param string $filename nombre con el que guardar la imagen temporal
 * @param file $file imagen a subir
 * @param string $type tipo de imagen subida. poster / fanart
 * @param string $dir directorio de destino, segun sea pelicula, concierto…
 * @param boolean $fromURL indica si se ha subido del escritorio o desde una url
 * @return boolean
 */
function uploadImage($filename,$file,$type,$fromURL = 0) {
    global $conf;

    if($fromURL) {
        $archivo = $file;
        $ftype = "image/jpeg";
    } else {
        $archivo = $file['tmp_name'];
        $ftype = $file['type'];
    }
    if(!empty($archivo) && $archivo != "none") {
        if ($ftype == "image/jpeg" || $ftype == "image/gif" || $ftype == "image/png") {
            switch($ftype) {
                case "image/jpeg":
                    $ext = "jpg";
                    break;
                case "image/gif":
                    $ext = "gif";
                    break;
                case "image/png":
                    $ext = "png";
                    break;
                default:
                    $ext = "jpg";
            }
            if($fromURL) {
                $nameOriginal = $file;
            } else {
                $nameOriginal = $file['name'];
            }
            $nameNew = $filename.".".$ext;
            $filecount = 0;
            while(file_exists(PATH_TEMP.$filecount."_".$nameNew)) {
                $filecount++;
            }
            $name = PATH_TEMP.$filecount."_".$nameNew;
            $nameOriginalSize = PATH_TEMP."o_".$filecount."_".$nameNew;
            if($type == "poster") {
                $nameThumb = PATH_TEMP."t_".$filecount."_".$nameNew;
            }
            if($fromURL) {
                if(!copy($archivo,$name)) {
                    $opts = array('http'=>array('header' => "User-Agent:MyAgent/1.0\r\n"));
                    $context = stream_context_create($opts);
                    if(!$imgcontent = file_get_contents($archivo,false,$context)) {
                        throw new Exception("Error de copia de imagen desde URL. Cogiendo imagen", 1);
                    }
                    if(!file_put_contents($name,$imgcontent)) {
                        throw new Exception("Error de copia de imagen desde URL. Guardando imagen", 1);
                    }
                }
            } else {
                if(!move_uploaded_file ($archivo, $name)) {
                    throw new Exception("Error de subida de imagen", 1);
                }
            }
            switch($type) {
                case "poster": //Poster
                    //Copia de la imagen para hacer el thumb
                    copy($name,$nameThumb);
                    //Copia de la imagen original
                    copy($name,$nameOriginalSize);
                    //Redimensionamos el poster
                    resizeImage($name,POSTER_W,0);
                    //Redimensionamos y recortamos la miniatura (thumbnail)
                    resizeAndCropImage($nameThumb,POSTER_TW,POSTER_TH);
                    return str_replace(PATH_TEMP, "", $name);
                case "fanart": //Fanart
                    resizeImage($name,FANART_W,FANART_H);
                    return str_replace(PATH_TEMP, "", $name);
                break;
                case "image": //Imagen
                    //Copia de la imagen original
                    copy($name,$nameOriginalSize);
                    resizeImage($name,IMAGE_W,IMAGE_H);
                    return str_replace(PATH_TEMP, "", $name);
                break;
            }
        }
    }
    return false;
}

/*
 * Funcion que elimina todas las imagenes con un numbre, eliminando todas las posibles extensiones de ese nombre
 * @param string $filename: ruta y nombre de la imagen, sin extension
 */
function deleteAllExtImages($filename) {
    $types = array(".jpg",".gif",".png");
    foreach($types as $t) {
        if(file_exists(BASE_URI.$filename.$t)) {
            unlink(BASE_URI.$filename.$t);
        }
    }
}
?>