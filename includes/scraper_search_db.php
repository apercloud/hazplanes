<?php
include_once("api.inc.php");

$debug = "";

if (isset($_POST['titulo'])) { // Forms
    $titulo = $_POST['titulo'];
} else {
    $titulo = "";
}

if(!empty($titulo)) {
    $options = array("filter" => "nombre like '%".$db->secure_field($titulo)."%'");
    $peliculasDB = new Peliculas($options,$db);
    $peliDB = $peliculasDB->readAction();
    if(count($peliDB) > 0) {
        foreach($peliDB as $k => $v) {
            $peliDB[$k]["ficha"] = "index.php?p=adminformfilm&i=".$peliDB[$k]["idpelicula"];
            $peliDB[$k]["duracion"] = (int)$peliDB[$k]["duracion"];
            $peliDB[$k]["edad"] = (int)$peliDB[$k]["edad"];
            $peliDB[$k]["poster2"] = $peliDB[$k]["poster"];
            $peliDB[$k]["fanart2"] = $peliDB[$k]["fanart"];
            $peliDB[$k]["poster"] = "";
            $peliDB[$k]["fanart"] = "";
        }
    }
    $msg = array("success" => "ok","data" => $peliDB, "debug" => $debug);
} else {
    $msg = array("success" => "ko","data" => "Título vacío", "debug" => $debug);
}

echo json_encode($msg);
?>