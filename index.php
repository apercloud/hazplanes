<?php
/*
* This is the main page.
* This page includes the configuration file,
* the templates, and any content-specific modules.
*/
// This page begins the HTML header for the site.
require_once("includes/general.inc.php");
sec_session_start();

// Require the configuration file before any PHP code:
require_once ('./includes/config.inc.php');
require_once ('./includes/api.inc.php');

// Validate what page to show:
if (isset($_GET['p'])) {
    $p = $_GET['p'];
} elseif (isset($_POST['p'])) { // Forms
    $p = $_POST['p'];
} else {
    $p = NULL;
}

// Validate what item to show:
if (isset($_GET['i'])) {
    $id = $_GET['i'];
} elseif (isset($_POST['i'])) { // Forms
    $id = $_POST['i'];
} else {
    $id = NULL;
}

if(!is_null($id) && !is_numeric($id)) {
    page404();
}

$menuSel = array();
$menuSel = array_pad($menuSel,16,"");

// Determine what page to display:
switch ($p) {
    case '404':
        $page_title = 'Elemento no encontrado';
        $css = 'mantenimiento.css';
        $p = "404";
        include_once ('./includes/headermantenimiento.php');
        include ('./modules/404.inc.php');
        include_once ('./includes/footer.php');
        exit;
        break;
    case '403':
        $page_title = 'Acceso prohibido';
        $css = 'mantenimiento.css';
        $p = "403";
        include_once ('./includes/headermantenimiento.php');
        include ('./modules/403.inc.php');
        include_once ('./includes/footer.php');
        exit;
        break;
    //Páginas públicas
    case 'novedades':
        $page = 'list.novedades.inc.php';
        $page_title = 'Novedades';
        $header = 'header.php';
        $menuSel[13] = "current";
        $menuTit = "Novedades";
        if(empty($id)) {
            $ogtitle = "Novedades";
            $ogurl = BASE_URL."novedades/";
            $ogdescription = "Descubre todos los nuevos eventos de la página y los cambios de tus eventos favoritos";
        } else {
            if($id == 1) {
                $ogtitle = "Novedades - Nuevos";
                $ogurl = BASE_URL."novedades/creado";
                $ogdescription = "Enterate de los nuevos eventos que se crean en nuestra página";
            } elseif($id == 2) {
                $ogtitle = "Novedades - Actualizaciones";
                $ogurl = BASE_URL."novedades/actualizado";
                $ogdescription = "Enterate de todos los los cambios de tus eventos favoritos";
            }
        }
        $ogimage = BASE_URL."img/screenshot.png";
        break;

    case 'conreserva':
        $page = 'list.conreserva.inc.php';
        $page_title = 'Con reserva';
        $header = 'header.php';
        $menuSel[15] = "current";
        $menuTit = "Con reserva";
        $ogtitle = "Con reserva";
        $ogurl = BASE_URL."conreserva/";
        $ogdescription = "Entérate de los eventos que necesitan reserva antes de que ya no puedas anotarte";
        $ogimage = BASE_URL."img/screenshot.png";
        break;

    case 'lugares':
        $page = 'list.lugares.inc.php';
        $page_title = 'Lugares';
        $header = 'header.php';
        $menuSel[14] = "current";
        $menuTit = "Lugares";
        $ogtitle = "Lugares";
        $ogurl = BASE_URL."lugares/";
        $ogdescription = "Descubre toda la información de los locales que realizan eventos";
        $ogimage = BASE_URL."img/screenshot.png";
        break;

    case 'cartelera':
        $page = 'list.eventos.inc.php';
        $page_title = 'Películas';
        $header = 'header.php';
        $menuSel[1] = "current";
        $menuTit = "Películas";
        $ogtitle = "Películas";
        $ogurl = BASE_URL."cartelera/";
        $ogdescription = "Toda la cartelera de los locales de Vigo";
        $ogimage = BASE_URL."img/screenshot.png";
        break;

    case 'pelicula':
        if(isset($id) && !empty($id)) {
            $options = array("idpelicula" => $id);
            $pelicula = new Peliculas($options,$db);
            $d = $pelicula->readAction();
            $d = $d[0];
            if(!isset($d["nombre"]) || !$d["publicado"]) {
                page404();
            }
            $ogtitle = $d["nombre"]." (".$d["original"].")";
            $ogurl = BASE_URL."pelicula/".$id."-".urlAmigable($d["nombre"]);
            $ogdescription = $d["sinopsis"];
            if(!empty($d["fanart"])) {
                $ogimage = BASE_URL.$d["fanart"];
            } else {
                $ogimage = BASE_URL.$d["poster"];
            }
            $page_title = $ogtitle;
        } else {
            page404();
        }
        $page = 'pelicula.inc.php';
        $header = 'header.php';
        $menuSel[1] = "current";
        $menuTit = "Cartelera";
        break;

    case 'cine':
        if(isset($id) && !empty($id)) {
            $options = array("idcine" => $id);
            $cine = new Cines($options,$db);
            $d = $cine->readAction();
            $d = $d[0];
            if(!isset($d["nombre"]) || !$d["publicado"]) {
                page404();
            }
            $ogtitle = $d["nombre"];
            $ogurl = BASE_URL."cine/".$id."-".urlAmigable($d["nombre"]);
            $ogdescription = $d["descripcion"];
            if(!empty($d["imagen"])) {
                $ogimage = BASE_URL.$d["imagen"];
            } else {
                $ogimage = BASE_URL."img/screenshot.png";
            }
            $page_title = $ogtitle;
        } else {
            page404();
        }
        $page = 'cine.inc.php';
        $header = 'header.php';
        $menuSel[14] = "current";
        $menuTit = "Cartelera";
        break;

    case 'conciertos':
        $page = 'list.eventos.inc.php';
        $page_title = 'Conciertos';
        $header = 'header.php';
        $menuSel[2] = "current";
        $menuTit = "Conciertos";
        $ogtitle = "Conciertos";
        $ogurl = BASE_URL."conciertos/";
        $ogdescription = "Todos los conciertos que hay y habrá en Vigo";
        $ogimage = BASE_URL."img/screenshot.png";
        break;

    case 'concierto':
        if(isset($id) && !empty($id)) {
            $options = array("idconcierto" => $id);
            $concierto = new Conciertos($options,$db);
            $d = $concierto->readAction();
            $d = $d[0];
            if(!isset($d["nombre"]) || !$d["publicado"]) {
                page404();
            }
            $ogtitle = $d["nombre"];
            $ogurl = BASE_URL."concierto/".$id."-".urlAmigable($d["nombre"]);
            $ogdescription = $d["sinopsis"];
            if(!empty($d["fanart"])) {
                $ogimage = BASE_URL.$d["fanart"];
            } else {
                $ogimage = BASE_URL.$d["poster"];
            }
            $page_title = $ogtitle;
        } else {
            page404();
        }
        $page = 'concierto.inc.php';
        $header = 'header.php';
        $menuSel[2] = "current";
        $menuTit = "Conciertos";
        break;

    case 'local':
        if(isset($id) && !empty($id)) {
            $options = array("idlocal" => $id);
            $local = new Locales($options,$db);
            $d = $local->readAction();
            $d = $d[0];
            if(!isset($d["nombre"]) || !$d["publicado"]) {
                page404();
            }
            $ogtitle = $d["nombre"];
            $ogurl = BASE_URL."local/".$id."-".urlAmigable($d["nombre"]);
            $ogdescription = $d["descripcion"];
            if(!empty($d["imagen"])) {
                $ogimage = BASE_URL.$d["imagen"];
            } else {
                $ogimage = BASE_URL."img/screenshot.png";
            }
            $page_title = $ogtitle;
        } else {
            page404();
        }
        $page = 'local.inc.php';
        $header = 'header.php';
        $menuSel[14] = "current";
        $menuTit = "Local";
        break;

    case 'obrasteatro':
        $page = 'list.eventos.inc.php';
        $page_title = 'Espectáculos';
        $header = 'header.php';
        $menuSel[3] = "current";
        $menuTit = "Espectáculos";
        $ogtitle = "Espectáculos";
        $ogurl = BASE_URL."obrasteatro/";
        $ogdescription = "Todas las actuaciones teatrales que hay y habrá en Vigo";
        $ogimage = BASE_URL."img/screenshot.png";
        break;

    case 'obra':
        if(isset($id) && !empty($id)) {
            $options = array("idobrateatro" => $id);
            $obra = new Obras($options,$db);
            $d = $obra->readAction();
            $d = $d[0];
            if(!isset($d["nombre"]) || !$d["publicado"]) {
                page404();
            }
            $ogtitle = $d["nombre"];
            $ogurl = BASE_URL."obra/".$id."-".urlAmigable($d["nombre"]);
            $ogdescription = $d["sinopsis"];
            if(!empty($d["fanart"])) {
                $ogimage = BASE_URL.$d["fanart"];
            } else {
                $ogimage = BASE_URL.$d["poster"];
            }
            $page_title = $ogtitle;
        } else {
            page404();
        }
        $page = 'obra.inc.php';
        $header = 'header.php';
        $menuSel[3] = "current";
        $menuTit = "Teatro";
        break;

    case 'auditorio':
        if(isset($id) && !empty($id)) {
            $options = array("idteatro" => $id);
            $teatro = new Teatros($options,$db);
            $d = $teatro->readAction();
            $d = $d[0];
            if(!isset($d["nombre"]) || !$d["publicado"]) {
                page404();
            }
            $ogtitle = $d["nombre"];
            $ogurl = BASE_URL."auditorio/".$id."-".urlAmigable($d["nombre"]);
            $ogdescription = $d["descripcion"];
            if(!empty($d["imagen"])) {
                $ogimage = BASE_URL.$d["imagen"];
            } else {
                $ogimage = BASE_URL."img/screenshot.png";
            }
            $page_title = $ogtitle;
        } else {
            page404();
        }
        $page = 'auditorio.inc.php';
        $header = 'header.php';
        $menuSel[14] = "current";
        $menuTit = "Teatro";
        break;

    case 'eventos':
        $page = 'list.eventos.inc.php';
        $page_title = 'Eventos';
        $header = 'header.php';
        $menuSel[4] = "current";
        $menuTit = "Eventos";
        $ogtitle = "Eventos";
        $ogurl = BASE_URL."eventos/";
        $ogdescription = "Todos los eventos variados que hay y habrá en Vigo";
        $ogimage = BASE_URL."img/screenshot.png";
        break;

    case 'evento':
        if(isset($id) && !empty($id)) {
            $options = array("ideventos" => $id);
            $evento = new Eventos($options,$db);
            $d = $evento->readAction();
            $d = $d[0];
            if(!isset($d["nombre"]) || !$d["publicado"]) {
                page404();
            }
            $ogtitle = $d["nombre"];
            $ogurl = BASE_URL."evento/".$id."-".urlAmigable($d["nombre"]);
            $ogdescription = $d["descripcion"];
            if(!empty($d["fanart"])) {
                $ogimage = BASE_URL.$d["fanart"];
            } else {
                $ogimage = BASE_URL.$d["poster"];
            }
            $page_title = $ogtitle;
        } else {
            page404();
        }
        $page = 'evento.inc.php';
        $header = 'header.php';
        $menuSel[4] = "current";
        $menuTit = "Eventos";
        break;

    case 'exposiciones':
        $page = 'list.eventos.inc.php';
        $page_title = 'Exposiciones';
        $header = 'header.php';
        $menuSel[5] = "current";
        $menuTit = "Exposiciones";
        $ogtitle = "Exposiciones";
        $ogurl = BASE_URL."exposiciones/";
        $ogdescription = "Todas las exposiciones que hay en Vigo";
        $ogimage = BASE_URL."img/screenshot.png";
        break;

    case 'exposicion':
        if(isset($id) && !empty($id)) {
            $options = array("idexposicion" => $id);
            $exposicion = new Exposiciones($options,$db);
            $d = $exposicion->readAction();
            $d = $d[0];
            if(!isset($d["nombre"]) || !$d["publicado"]) {
                page404();
            }
            $ogtitle = $d["nombre"];
            $ogurl = BASE_URL."exposicion/".$id."-".urlAmigable($d["nombre"]);
            $ogdescription = $d["descripcion"];
            if(!empty($d["fanart"])) {
                $ogimage = BASE_URL.$d["fanart"];
            } else {
                $ogimage = BASE_URL.$d["poster"];
            }
            $page_title = $ogtitle;
        } else {
            page404();
        }
        $page = 'exposicion.inc.php';
        $header = 'header.php';
        $menuSel[5] = "current";
        $menuTit = "Exposiciones";
        break;

    case 'museo':
        if(isset($id) && !empty($id)) {
            $options = array("idmuseo" => $id);
            $museo = new Museos($options,$db);
            $d = $museo->readAction();
            $d = $d[0];
            if(!isset($d["nombre"]) || !$d["publicado"]) {
                page404();
            }
            $ogtitle = $d["nombre"];
            $ogurl = BASE_URL."museo/".$id."-".urlAmigable($d["nombre"]);
            $ogdescription = $d["descripcion"];
            if(!empty($d["imagen"])) {
                $ogimage = BASE_URL.$d["imagen"];
            } else {
                $ogimage = BASE_URL."img/screenshot.png";
            }
            $page_title = $ogtitle;
        } else {
            page404();
        }
        $page = 'museo.inc.php';
        $header = 'header.php';
        $menuSel[14] = "current";
        $menuTit = "Museo";
        break;

    case 'deportes':
        $page = 'list.eventos.inc.php';
        $page_title = 'Deportes';
        $header = 'header.php';
        $menuSel[9] = "current";
        $menuTit = "Deportes";
        $ogtitle = "Deportes";
        $ogurl = BASE_URL."deportes/";
        $ogdescription = "Todas las competiciones, exhibiciones y partidos de cualquier deporte que hay y habrá en Vigo";
        $ogimage = BASE_URL."img/screenshot.png";
        break;

    case 'competicion':
        if(isset($id) && !empty($id)) {
            $options = array("iddeporte" => $id);
            $deporte = new Deportes($options,$db);
            $d = $deporte->readAction();
            $d = $d[0];
            if(!isset($d["nombre"]) || !$d["publicado"]) {
                page404();
            }
            $ogtitle = $d["nombre"];
            $ogurl = BASE_URL."competicion/".$id."-".urlAmigable($d["nombre"]);
            $ogdescription = $d["descripcion"];
            if(!empty($d["fanart"])) {
                $ogimage = BASE_URL.$d["fanart"];
            } elseif(!empty($d["poster"])) {
                $ogimage = BASE_URL.$d["poster"];
            } else {
                if(empty($d["poster"])) {
                    if(!empty($d["tipo"])) {
                        $options2 = array("read" => "datatipodeporte", "idtipodeporte" => $d["tipo"]);
                        $tipodeporte = new Deportes($options2,$db);
                        $d2 = $tipodeporte->readAction();
                        if(!empty($d2)) {
                            $d2 = $d2[0];
                            if(!empty($d2["poster"])) {
                                $img = "img/deportes/tipos/".$d2["poster"];
                            }
                            $d["tipo"] = $d2["nombre"];
                        }
                    }
                } else {
                    $img = $d["poster"];
                }
                if(!isset($img) || empty($img)) {
                    $img = "img/deportes/tipos/mix.png";
                }
                $ogimage = BASE_URL.$img;
            }
            $page_title = $ogtitle;
        } else {
            page404();
        }
        $page = 'competicion.inc.php';
        $header = 'header.php';
        $menuSel[9] = "current";
        $menuTit = "Deportes";
        break;

    case 'pabellon':
        if(isset($id) && !empty($id)) {
            $options = array("idpabellon" => $id);
            $pabellon = new Pabellones($options,$db);
            $d = $pabellon->readAction();
            $d = $d[0];
            if(!isset($d["nombre"]) || !$d["publicado"]) {
                page404();
            }
            $ogtitle = $d["nombre"];
            $ogurl = BASE_URL."pabellon/".$id."-".urlAmigable($d["nombre"]);
            $ogdescription = $d["descripcion"];
            if(!empty($d["imagen"])) {
                $ogimage = BASE_URL.$d["imagen"];
            } else {
                $ogimage = BASE_URL."img/screenshot.png";
            }
            $page_title = $ogtitle;
        } else {
            page404();
        }
        $page = 'pabellon.inc.php';
        $header = 'header.php';
        $menuSel[14] = "current";
        $menuTit = "Pabellón";
        break;

    case 'infantil':
        $page = 'list.infantil.inc.php';
        $page_title = 'Infantil/Familiar';
        $header = 'header.php';
        $menuSel[11] = "current";
        $menuTit = "Infantil/Familiar";
        $ogtitle = "Infantil/Familiar";
        $ogurl = BASE_URL."infantil/";
        $ogdescription = "Todos los eventos variados, especialmente dedicados a los más pequeños, que hay y habrá en Vigo";
        $ogimage = BASE_URL."img/screenshot.png";
        break;

    case 'formacion':
        $page = 'list.eventos.inc.php';
        $page_title = 'Cursos/Charlas';
        $header = 'header.php';
        $menuSel[12] = "current";
        $menuTit = "Cursos/Charlas";
        $ogtitle = "Cursos/Charlas";
        $ogurl = BASE_URL."formacion/";
        $ogdescription = "Todos los cursos, talleres u obradoiros variados que hay y habrá en Vigo";
        $ogimage = BASE_URL."img/screenshot.png";
        break;

    case 'curso':
        if(isset($id) && !empty($id)) {
            $options = array("idcurso" => $id);
            $curso = new Cursos($options,$db);
            $d = $curso->readAction();
            $d = $d[0];
            if(!isset($d["nombre"]) || !$d["publicado"]) {
                page404();
            }
            $ogtitle = $d["nombre"];
            $ogurl = BASE_URL."curso/".$id."-".urlAmigable($d["nombre"]);
            $ogdescription = $d["descripcion"];
            if(!empty($d["poster"])) {
                $ogimage = BASE_URL.$d["poster"];
            } else {
                $ogimage = BASE_URL."img/screenshot.png";
            }
            $page_title = $ogtitle;
        } else {
            page404();
        }
        $page = 'curso.inc.php';
        $header = 'header.php';
        $menuSel[12] = "current";
        $menuTit = "Cursos";
        break;

    case 'recomendacion':
        $page = 'upload.inc.php';
        $page_title = 'Subir evento';
        $header = 'header.php';
        $menuSel[6] = "current";
        $menuTit = "Subir evento";
        $ogtitle = "Subir evento";
        $ogurl = BASE_URL."upload/";
        $ogdescription = "Envíanos tus eventos para que los podamos publicar";
        $ogimage = BASE_URL."img/screenshot.png";
        break;

    case 'contacto':
        $page = 'contacto.inc.php';
        $page_title = 'Contacto';
        $header = 'header.php';
        $menuSel[7] = "current";
        $menuTit = "Contacto";
        $ogtitle = "Contacto";
        $ogurl = BASE_URL."contacto/";
        $ogdescription = "Ponte en contacto con nosotros";
        $ogimage = BASE_URL."img/screenshot.png";
        break;

    case 'acercade':
        $page = 'acercade.inc.php';
        $page_title = 'Acerca de...';
        $header = 'header.php';
        $menuSel[8] = "current";
        $menuTit = "Acerca de…";
        $ogtitle = "Acerca de…";
        $ogurl = BASE_URL."about/";
        $ogdescription = "Descrubre más sobre los que hacemos posible esta página";
        $ogimage = BASE_URL."img/screenshot.png";
        break;

    case 'encuesta':
        $page = 'encuesta.inc.php';
        $page_title = 'Encuesta';
        $header = 'header.php';
        $menuSel[10] = "current";
        $menuTit = "Encuesta";
        $ogtitle = "Encuesta";
        $ogurl = BASE_URL."encuesta/";
        $ogdescription = "Rellena nuestra encuesta para saber en qué tenemos que mejorar";
        $ogimage = BASE_URL."img/screenshot.png";
        break;

    case 'cookies':
        $page = 'cookies.inc.php';
        $page_title = 'Política de cookies';
        $header = 'header.php';
        $menuSel[0] = "current";
        $menuTit = "Política de cookies";
        $ogtitle = "Política de cookies";
        $ogurl = BASE_URL."cookies/";
        $ogdescription = "Información de nuestra política de cookies";
        $ogimage = BASE_URL."img/screenshot.png";
        break;
    //Fin páginas públicas

    //Páginas de administración
    case 'admindashboard':
        $page = 'admin.dashboard.inc.php';
        $page_title = 'Administración';
        $header = 'headeradmin.php';
        break;

    case 'adminsharelinks':
        $page = 'admin.sharelinks.inc.php';
        $page_title = 'Social';
        $header = 'headeradmin.php';
        break;

    case 'adminscraperwebtojson':
        $page = 'admin.scraper.webtojson.inc.php';
        $page_title = 'Scraper To JSON';
        $header = 'headeradmin.php';
        break;

    case 'adminscraperjsontobd':
        $page = 'admin.scraper.jsontobd.inc.php';
        $page_title = 'JSON to BD';
        $header = 'headeradmin.php';
        break;

    case 'adminscraperjsontobdservinova':
        $page = 'admin.scraper.jsontobdServinova.inc.php';
        $page_title = 'JSON Servinova to BD';
        $header = 'headeradmin.php';
        break;

    case 'adminscraperjsonestrenostobd':
        $page = 'admin.scraper.jsonestrenostobd.inc.php';
        $page_title = 'JSON Estrenos to BD';
        $header = 'headeradmin.php';
        break;

    case 'admincontact':
        $page = 'admin.contact.inc.php';
        $page_title = 'Contacto';
        $header = 'headeradmin.php';
        break;

    case 'adminupload':
        $page = 'admin.upload.inc.php';
        $page_title = 'Recomendaciones';
        $header = 'headeradmin.php';
        break;

    case 'adminabout':
        $page = 'admin.about.inc.php';
        $page_title = 'Acerca de...';
        $header = 'headeradmin.php';
        break;

    case 'admincinema':
        $page = 'admin.main.cinema.inc.php';
        $page_title = 'Cartelera';
        $header = 'headeradmin.php';
        break;

    case 'adminconcert':
        $page = 'admin.main.concert.inc.php';
        $page_title = 'Conciertos';
        $header = 'headeradmin.php';
        break;

    case 'admineventos':
        $page = 'admin.main.eventos.inc.php';
        $page_title = 'Eventos';
        $header = 'headeradmin.php';
        break;

    case 'adminteatro':
        $page = 'admin.main.teatro.inc.php';
        $page_title = 'Teatro';
        $header = 'headeradmin.php';
        break;

    case 'adminexposiciones':
        $page = 'admin.main.exposiciones.inc.php';
        $page_title = 'Exposiciones';
        $header = 'headeradmin.php';
        break;

    case 'admindeportes':
        $page = 'admin.main.deportes.inc.php';
        $page_title = 'Deportes';
        $header = 'headeradmin.php';
        break;

    case 'admincursos':
        $page = 'admin.main.cursos.inc.php';
        $page_title = 'Formación';
        $header = 'headeradmin.php';
        break;

    //Inicio listados
    case 'adminlistcine':
        $page = 'admin.list.cine.inc.php';
        $page_title = 'Listado de cines';
        $header = 'headeradmin.php';
        break;

    case 'adminlistfilm':
        $page = 'admin.list.film.inc.php';
        $page_title = 'Listado de películas';
        $header = 'headeradmin.php';
        break;

    case 'adminlistpromo':
        $page = 'admin.list.promo.inc.php';
        $page_title = 'Listado de promociones';
        $header = 'headeradmin.php';
        break;

    case 'adminlistentradas':
        $page = 'admin.list.entradas.inc.php';
        $page_title = 'Listado de webs de venta de entradas';
        $header = 'headeradmin.php';
        break;

    case 'adminlistconcert':
        $page = 'admin.list.concert.inc.php';
        $page_title = 'Listado de conciertos';
        $header = 'headeradmin.php';
        break;

    case 'adminlisttheater':
        $page = 'admin.list.theater.inc.php';
        $page_title = 'Listado de auditorios';
        $header = 'headeradmin.php';
        break;

    case 'adminlistlocal':
        $page = 'admin.list.local.inc.php';
        $page_title = 'Listado de locales';
        $header = 'headeradmin.php';
        break;

    case 'adminlistteatro':
        $page = 'admin.list.obras.inc.php';
        $page_title = 'Listado de obras de teatro';
        $header = 'headeradmin.php';
        break;

    case 'adminlisteventos':
        $page = 'admin.list.eventos.inc.php';
        $page_title = 'Listado de eventos';
        $header = 'headeradmin.php';
        break;

    case 'adminlistexposiciones':
        $page = 'admin.list.exposiciones.inc.php';
        $page_title = 'Listado de exposiciones';
        $header = 'headeradmin.php';
        break;

    case 'adminlistmuseos':
        $page = 'admin.list.museos.inc.php';
        $page_title = 'Listado de museos';
        $header = 'headeradmin.php';
        break;

    case 'adminlistdeportes':
        $page = 'admin.list.deportes.inc.php';
        $page_title = 'Listado de competiciones';
        $header = 'headeradmin.php';
        break;

    case 'adminlistpabellones':
        $page = 'admin.list.pabellones.inc.php';
        $page_title = 'Listado de pabellones';
        $header = 'headeradmin.php';
        break;

    case 'adminlisttiposdeporte':
        $page = 'admin.list.tiposdeportes.inc.php';
        $page_title = 'Listado de tipos de deporte';
        $header = 'headeradmin.php';
        break;

    case 'adminlistcursos':
        $page = 'admin.list.cursos.inc.php';
        $page_title = 'Listado de cursos';
        $header = 'headeradmin.php';
        break;
    //Fin listados

    //Inicio formularios
    case 'adminformcine':
        $page = 'admin.form.cine.inc.php';
        $page_title = 'Formulario de cine';
        $header = 'headeradmin.php';
        break;

    case 'adminformfilm':
        $page = 'admin.form.film.inc.php';
        $page_title = 'Formulario de película';
        $header = 'headeradmin.php';
        break;

    case 'adminformpromo':
        $page = 'admin.form.promo.inc.php';
        $page_title = 'Formulario de cine';
        $header = 'headeradmin.php';
        break;

    case 'adminformentradas':
        $page = 'admin.form.entradas.inc.php';
        $page_title = 'Formulario de película';
        $header = 'headeradmin.php';
        break;

    case 'adminformconcert':
        $page = 'admin.form.concert.inc.php';
        $page_title = 'Formulario de concierto';
        $header = 'headeradmin.php';
        break;

    case 'adminformtheater':
        $page = 'admin.form.theater.inc.php';
        $page_title = 'Formulario de auditorio';
        $header = 'headeradmin.php';
        break;

    case 'adminformlocal':
        $page = 'admin.form.local.inc.php';
        $page_title = 'Formulario de local';
        $header = 'headeradmin.php';
        break;

    case 'adminformobra':
        $page = 'admin.form.obra.inc.php';
        $page_title = 'Formulario de obra de teatro';
        $header = 'headeradmin.php';
        break;

    case 'adminformevent':
        $page = 'admin.form.evento.inc.php';
        $page_title = 'Formulario de evento';
        $header = 'headeradmin.php';
        break;

    case 'adminformexpo':
        $page = 'admin.form.exposicion.inc.php';
        $page_title = 'Formulario de exposición';
        $header = 'headeradmin.php';
        break;

    case 'adminformmuseo':
        $page = 'admin.form.museo.inc.php';
        $page_title = 'Formulario de museo';
        $header = 'headeradmin.php';
        break;

    case 'adminformdeporte':
        $page = 'admin.form.competicion.inc.php';
        $page_title = 'Formulario de competición';
        $header = 'headeradmin.php';
        break;

    case 'adminformpabellon':
        $page = 'admin.form.pabellon.inc.php';
        $page_title = 'Formulario de pabellón';
        $header = 'headeradmin.php';
        break;

    case 'adminformtipodeporte':
        $page = 'admin.form.tipodeporte.inc.php';
        $page_title = 'Formulario de tipo de deporte';
        $header = 'headeradmin.php';
        break;

    case 'adminformcurso':
        $page = 'admin.form.curso.inc.php';
        $page_title = 'Formulario de curso';
        $header = 'headeradmin.php';
        break;

    //Fin formularios

    //Gestion de login
    case 'loginadmin':
        $page = 'login.inc.php';
        $page_title = 'Login';
        $header = 'headermantenimiento.php';
        // Login process
        $error = "";

        if(isset($_POST["loginbox_email"]) && isset($_POST["loginbox_password"])) {
            include_once(BASE_URI . "includes/users.inc.php");
            $email = filter_input(INPUT_POST, 'loginbox_email', FILTER_SANITIZE_EMAIL);
            if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $pass = filter_input(INPUT_POST, 'loginbox_password', FILTER_SANITIZE_STRING);
                if(isset($_POST["loginbox_remember"])) {
                    $remember = 1;
                } else {
                    $remember = 0;
                }
                $login = login($email,$pass,$remember);
                if($login == 1){
                    $url = BASE_URL . 'index.php?p=admindashboard';
                    echo "<script>window.location.href='".$url."';</script>";
                } else {
                    $error = "Datos incorrectos o el usuario no existe";
                }
            } else {
                $error = "El usuario debe ser una dirección email";
            }
        }
        break;

    case 'logout':
        unset($_SESSION);
        if(isset($_COOKIE["user"])) {
            setcookie("user", $_SESSION['username'], -(time()+60*60*24*7), "/"); //7 dias
        }
        session_destroy();
        session_start();
        $page = 'login.inc.php';
        $page_title = 'Login';
        $header = 'headermantenimiento.php';
        break;

    case 'mainmap':
	    $p = "mainmap";
        $page = 'main.map.inc.php';
        $page_title = 'HazPlanes.com - Todos los eventos de tu ciudad en un mapa';
        $header = 'header.php';
        $menuSel[0] = "current";
        $menuTit = "Hoy";
        $ogtitle = "HazPlanes.com - Todos los eventos de tu ciudad en un mapa";
        $ogurl = BASE_URL;
        $ogdescription = "Mapa de la agenda de eventos, conciertos, exposiciones, cartelera y mucho más en la ciudad de Vigo y alrededores.";
        $ogimage = BASE_URL."img/screenshot.png";
        break;

    // Default is to include the main page.
    default:
        $p = "main";
        $page = 'main.inc.php';
        $page_title = 'HazPlanes.com - Todos los eventos de tu ciudad';
        $header = 'header.php';
        $menuSel[0] = "current";
        $menuTit = "Hoy";
        $ogtitle = "HazPlanes.com - Todos los eventos de tu ciudad";
        $ogurl = BASE_URL;
        $ogdescription = "Agenda de eventos, conciertos, exposiciones, cartelera y mucho más en la ciudad de Vigo y alrededores.";
        $ogimage = BASE_URL."img/screenshot.png";
        break;

} // End of main switch.

// Make sure the file exists:
if (!file_exists('./modules/' . $page)) {
    page404();
}

//Comprobamos si está en modo mantenimiento
$mantenimiento = file_get_contents("modules/maintenancemodeBD.php");
if($mantenimiento && strpos($p,"admin") === false && !isset($_SESSION["user"])) {
    $page = 'maintenance.inc.php';
    $page_title = 'Mantenimiento de la página';
    $header = 'headermantenimiento.php';
    $footer = 'footermantenimiento.php';
} else {
    $footer = 'footer.php';
}

// Include the header file:
include_once ('./includes/'.$header);

// Include the content-specific module:
// $page is determined from the above switch.
include ('./modules/' . $page);

// Include the footer file to complete the template:
include_once ('./includes/' . $footer);

?>
