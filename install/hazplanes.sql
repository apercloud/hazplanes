SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';


-- -----------------------------------------------------
-- Table `hazplanes`.`users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `hazplanes`.`users` ;

CREATE TABLE IF NOT EXISTS `hazplanes`.`users` (
  `iduser` INT(4) NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(45) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  `name` VARCHAR(45) NULL DEFAULT NULL,
  `surname` VARCHAR(100) NULL DEFAULT NULL,
  `avatar` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`iduser`),
  UNIQUE INDEX `username_UNIQUE` (`email` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;


-- -----------------------------------------------------
-- Table `hazplanes`.`cines`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `hazplanes`.`cines` ;

CREATE TABLE IF NOT EXISTS `hazplanes`.`cines` (
  `idcine` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NOT NULL,
  `direccion` VARCHAR(255) NULL,
  `localidad` VARCHAR(15) NULL,
  `provincia` VARCHAR(15) NULL,
  `codigopostal` CHAR(5) NULL,
  `coordenadas` VARCHAR(255) NULL,
  `telefono` VARCHAR(13) NULL,
  `email` VARCHAR(255) NULL,
  `web` VARCHAR(255) NULL,
  `twitter` VARCHAR(255) NULL,
  `facebook` VARCHAR(255) NULL,
  `google` VARCHAR(255) NULL,
  `otrasocial` VARCHAR(255) NULL,
  `diaespectador` VARCHAR(10) NOT NULL,
  `notas` TEXT NULL,
  `descripcion` TEXT NULL,
  `imagen` VARCHAR(45) NULL,
  `publicado` TINYINT(1) NULL DEFAULT 0,
  `menciones` TEXT NULL,
  PRIMARY KEY (`idcine`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hazplanes`.`salas`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `hazplanes`.`salas` ;

CREATE TABLE IF NOT EXISTS `hazplanes`.`salas` (
  `idsala` INT NOT NULL AUTO_INCREMENT,
  `idcine` INT NOT NULL,
  `numero` TINYINT(2) NOT NULL,
  `capacidad` SMALLINT(3) NULL,
  `filas` TINYINT(2) NULL,
  `numerada` TINYINT(1) NULL DEFAULT 1,
  PRIMARY KEY (`idsala`, `idcine`),
  INDEX `fk_salas_cines_idx` (`idcine` ASC),
  CONSTRAINT `fk_salas_cines`
    FOREIGN KEY (`idcine`)
    REFERENCES `hazplanes`.`cines` (`idcine`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hazplanes`.`entradas`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `hazplanes`.`entradas` ;

CREATE TABLE IF NOT EXISTS `hazplanes`.`entradas` (
  `identradas` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(255) NOT NULL,
  `url` VARCHAR(255) NOT NULL,
  `email` VARCHAR(255) NULL,
  `comision` FLOAT NULL,
  `telefono` VARCHAR(13) NULL,
  `direccion` VARCHAR(100) NULL,
  `localidad` VARCHAR(45) NULL,
  `provincia` VARCHAR(45) NULL,
  `codigopostal` VARCHAR(45) NULL,
  `coordenadas` VARCHAR(255) NULL,
  `imagen` VARCHAR(45) NULL,
  `publicado` TINYINT(1) NULL DEFAULT 0,
  `menciones` TEXT NULL,
  PRIMARY KEY (`identradas`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hazplanes`.`servicios`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `hazplanes`.`servicios` ;

CREATE TABLE IF NOT EXISTS `hazplanes`.`servicios` (
  `idservicios` INT NOT NULL AUTO_INCREMENT,
  `idcine` INT NOT NULL,
  `minusvalidos` TINYINT(1) NULL DEFAULT 0,
  `HD` TINYINT(1) NULL DEFAULT 0,
  `3D` TINYINT(1) NULL DEFAULT 0,
  `parking` TINYINT(1) NULL DEFAULT 0,
  `carnetjoven` TINYINT(1) NULL DEFAULT 0,
  `carnetestudiante` TINYINT(1) NULL DEFAULT 0,
  PRIMARY KEY (`idservicios`, `idcine`),
  INDEX `fk_servicios_cines1_idx` (`idcine` ASC),
  CONSTRAINT `fk_servicios_cines1`
    FOREIGN KEY (`idcine`)
    REFERENCES `hazplanes`.`cines` (`idcine`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hazplanes`.`cines_has_entradas`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `hazplanes`.`cines_has_entradas` ;

CREATE TABLE IF NOT EXISTS `hazplanes`.`cines_has_entradas` (
  `idcine` INT NOT NULL,
  `identradas` INT NOT NULL,
  PRIMARY KEY (`idcine`, `identradas`),
  INDEX `fk_cines_has_webentradas_webentradas1_idx` (`identradas` ASC),
  INDEX `fk_cines_has_webentradas_cines1_idx` (`idcine` ASC),
  CONSTRAINT `fk_cines_has_entradas_cines1`
    FOREIGN KEY (`idcine`)
    REFERENCES `hazplanes`.`cines` (`idcine`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cines_has_entradas_entradas1`
    FOREIGN KEY (`identradas`)
    REFERENCES `hazplanes`.`entradas` (`identradas`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hazplanes`.`promociones`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `hazplanes`.`promociones` ;

CREATE TABLE IF NOT EXISTS `hazplanes`.`promociones` (
  `idpromocion` BIGINT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NOT NULL,
  `descripcion` TEXT NULL,
  `inicio` DATETIME NULL,
  `fin` DATETIME NULL,
  `imagen` VARCHAR(45) NULL,
  `publicado` TINYINT(1) NULL DEFAULT 0,
  `menciones` TEXT NULL,
  PRIMARY KEY (`idpromocion`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hazplanes`.`cines_has_promociones`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `hazplanes`.`cines_has_promociones` ;

CREATE TABLE IF NOT EXISTS `hazplanes`.`cines_has_promociones` (
  `idcine` INT NOT NULL,
  `idpromocion` BIGINT NOT NULL,
  PRIMARY KEY (`idcine`, `idpromocion`),
  INDEX `fk_cines_has_promociones_promociones1_idx` (`idpromocion` ASC),
  INDEX `fk_cines_has_promociones_cines1_idx` (`idcine` ASC),
  CONSTRAINT `fk_cines_has_promociones_cines1`
    FOREIGN KEY (`idcine`)
    REFERENCES `hazplanes`.`cines` (`idcine`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cines_has_promociones_promociones1`
    FOREIGN KEY (`idpromocion`)
    REFERENCES `hazplanes`.`promociones` (`idpromocion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hazplanes`.`peliculas`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `hazplanes`.`peliculas` ;

CREATE TABLE IF NOT EXISTS `hazplanes`.`peliculas` (
  `idpelicula` BIGINT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(200) NOT NULL,
  `original` VARCHAR(200) NULL,
  `director` TEXT NULL,
  `actores` TEXT NULL,
  `nacionalidad` VARCHAR(45) NULL,
  `distribuidora` VARCHAR(150) NULL,
  `edad` TINYINT(2) NULL,
  `duracion` SMALLINT(3) NULL,
  `genero` VARCHAR(255) NULL,
  `sinopsis` TEXT NULL,
  `trailer` VARCHAR(255) NULL,
  `poster` VARCHAR(50) NULL,
  `fanart` VARCHAR(50) NULL,
  `fechaestreno` DATE NULL,
  `web` VARCHAR(255) NULL,
  `fichaimdb` VARCHAR(255) NULL,
  `fichafa` VARCHAR(255) NULL,
  `fichatmdb` VARCHAR(255) NULL,
  `publicado` TINYINT(1) NULL DEFAULT 0,
  `menciones` TEXT NULL,
  `infantil` TINYINT(1) NULL DEFAULT 0,
  PRIMARY KEY (`idpelicula`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hazplanes`.`cines_has_peliculas`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `hazplanes`.`cines_has_peliculas` ;

CREATE TABLE IF NOT EXISTS `hazplanes`.`cines_has_peliculas` (
  `idcine` INT NOT NULL,
  `idsala` INT NOT NULL,
  `idpelicula` BIGINT NOT NULL,
  `fecha` DATETIME NOT NULL,
  `3d` TINYINT(1) NULL DEFAULT 0,
  `vo` TINYINT(1) NULL DEFAULT 0,
  `urlcompra` VARCHAR(255) NULL COMMENT 'URL de compra de entradas para la película y sesión indicadas',
  `precio` VARCHAR(10) NULL,
  `todoeldia` TINYINT(1) NULL DEFAULT 0,
  PRIMARY KEY (`idcine`, `idsala`, `idpelicula`, `fecha`),
  INDEX `fk_cines_has_peliculas_peliculas1_idx` (`idpelicula` ASC),
  INDEX `fk_cines_has_peliculas_cines1_idx` (`idcine` ASC),
  INDEX `fk_cines_has_peliculas_salas1_idx` (`idsala` ASC),
  CONSTRAINT `fk_cines_has_peliculas_cines1`
    FOREIGN KEY (`idcine`)
    REFERENCES `hazplanes`.`cines` (`idcine`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cines_has_peliculas_peliculas1`
    FOREIGN KEY (`idpelicula`)
    REFERENCES `hazplanes`.`peliculas` (`idpelicula`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cines_has_peliculas_salas1`
    FOREIGN KEY (`idsala`)
    REFERENCES `hazplanes`.`salas` (`idsala`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hazplanes`.`locales`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `hazplanes`.`locales` ;

CREATE TABLE IF NOT EXISTS `hazplanes`.`locales` (
  `idlocal` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NOT NULL,
  `direccion` VARCHAR(255) NULL,
  `localidad` VARCHAR(15) NULL,
  `provincia` VARCHAR(15) NULL,
  `codigopostal` CHAR(5) NULL,
  `coordenadas` VARCHAR(255) NULL,
  `telefono` VARCHAR(13) NULL,
  `email` VARCHAR(255) NULL,
  `web` VARCHAR(255) NULL,
  `twitter` VARCHAR(255) NULL,
  `facebook` VARCHAR(255) NULL,
  `google` VARCHAR(255) NULL,
  `otrasocial` VARCHAR(255) NULL,
  `notas` TEXT NULL,
  `descripcion` TEXT NULL,
  `imagen` VARCHAR(45) NULL,
  `publicado` TINYINT(1) NULL DEFAULT 0,
  `menciones` TEXT NULL,
  PRIMARY KEY (`idlocal`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hazplanes`.`teatros`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `hazplanes`.`teatros` ;

CREATE TABLE IF NOT EXISTS `hazplanes`.`teatros` (
  `idteatro` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NOT NULL,
  `direccion` VARCHAR(255) NULL,
  `localidad` VARCHAR(15) NULL,
  `provincia` VARCHAR(15) NULL,
  `codigopostal` CHAR(5) NULL,
  `coordenadas` VARCHAR(255) NULL,
  `telefono` VARCHAR(13) NULL,
  `email` VARCHAR(255) NULL,
  `web` VARCHAR(255) NULL,
  `twitter` VARCHAR(255) NULL,
  `facebook` VARCHAR(255) NULL,
  `google` VARCHAR(255) NULL,
  `otrasocial` VARCHAR(255) NULL,
  `notas` TEXT NULL,
  `descripcion` TEXT NULL,
  `imagen` VARCHAR(45) NULL,
  `publicado` TINYINT(1) NULL DEFAULT 0,
  `menciones` TEXT NULL,
  PRIMARY KEY (`idteatro`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hazplanes`.`conciertos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `hazplanes`.`conciertos` ;

CREATE TABLE IF NOT EXISTS `hazplanes`.`conciertos` (
  `idconcierto` BIGINT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(200) NOT NULL,
  `duracion` SMALLINT(3) NULL,
  `genero` VARCHAR(255) NULL,
  `sinopsis` TEXT NULL,
  `video` VARCHAR(255) NULL,
  `poster` VARCHAR(50) NULL,
  `fanart` VARCHAR(50) NULL,
  `publicado` TINYINT(1) NULL DEFAULT 0,
  `menciones` TEXT NULL,
  `infantil` TINYINT(1) NULL DEFAULT 0,
  PRIMARY KEY (`idconcierto`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hazplanes`.`locales_has_entradas`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `hazplanes`.`locales_has_entradas` ;

CREATE TABLE IF NOT EXISTS `hazplanes`.`locales_has_entradas` (
  `idlocal` INT NOT NULL,
  `identradas` INT NOT NULL,
  PRIMARY KEY (`idlocal`, `identradas`),
  INDEX `fk_locales_has_webentradas_webentradas1_idx` (`identradas` ASC),
  INDEX `fk_locales_has_webentradas_locales1_idx` (`idlocal` ASC),
  CONSTRAINT `fk_locales_has_entradas_locales1`
    FOREIGN KEY (`idlocal`)
    REFERENCES `hazplanes`.`locales` (`idlocal`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_locales_has_entradas_entradas1`
    FOREIGN KEY (`identradas`)
    REFERENCES `hazplanes`.`entradas` (`identradas`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hazplanes`.`locales_has_peliculas`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `hazplanes`.`locales_has_peliculas` ;

CREATE TABLE IF NOT EXISTS `hazplanes`.`locales_has_peliculas` (
  `idlocal` INT NOT NULL,
  `idpelicula` BIGINT NOT NULL,
  `fecha` DATETIME NOT NULL,
  `3d` TINYINT(1) NULL DEFAULT 0,
  `vo` TINYINT(1) NULL DEFAULT 0,
  `urlcompra` VARCHAR(255) NULL,
  `precio` VARCHAR(10) NULL,
  `todoeldia` TINYINT(1) NULL DEFAULT 0,
  PRIMARY KEY (`idlocal`, `idpelicula`, `fecha`),
  INDEX `fk_locales_has_peliculas_peliculas1_idx` (`idpelicula` ASC),
  INDEX `fk_locales_has_peliculas_locales1_idx` (`idlocal` ASC),
  CONSTRAINT `fk_locales_has_peliculas_locales1`
    FOREIGN KEY (`idlocal`)
    REFERENCES `hazplanes`.`locales` (`idlocal`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_locales_has_peliculas_peliculas1`
    FOREIGN KEY (`idpelicula`)
    REFERENCES `hazplanes`.`peliculas` (`idpelicula`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hazplanes`.`locales_has_conciertos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `hazplanes`.`locales_has_conciertos` ;

CREATE TABLE IF NOT EXISTS `hazplanes`.`locales_has_conciertos` (
  `idlocal` INT NOT NULL,
  `idconcierto` BIGINT NOT NULL,
  `fecha` DATETIME NOT NULL,
  `urlcompra` VARCHAR(255) NULL,
  `precio` VARCHAR(10) NULL,
  `precioanticipada` VARCHAR(10) NULL,
  `todoeldia` TINYINT(1) NULL DEFAULT 0,
  PRIMARY KEY (`idlocal`, `idconcierto`, `fecha`),
  INDEX `fk_locales_has_conciertos_conciertos1_idx` (`idconcierto` ASC),
  INDEX `fk_locales_has_conciertos_locales1_idx` (`idlocal` ASC),
  CONSTRAINT `fk_locales_has_conciertos_locales1`
    FOREIGN KEY (`idlocal`)
    REFERENCES `hazplanes`.`locales` (`idlocal`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_locales_has_conciertos_conciertos1`
    FOREIGN KEY (`idconcierto`)
    REFERENCES `hazplanes`.`conciertos` (`idconcierto`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hazplanes`.`teatros_has_conciertos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `hazplanes`.`teatros_has_conciertos` ;

CREATE TABLE IF NOT EXISTS `hazplanes`.`teatros_has_conciertos` (
  `idteatro` INT NOT NULL,
  `idconcierto` BIGINT NOT NULL,
  `fecha` DATETIME NOT NULL,
  `urlcompra` VARCHAR(255) NULL,
  `precio` VARCHAR(10) NULL,
  `precioanticipada` VARCHAR(10) NULL,
  `todoeldia` TINYINT(1) NULL DEFAULT 0,
  PRIMARY KEY (`idteatro`, `idconcierto`, `fecha`),
  INDEX `fk_teatros_has_conciertos_conciertos1_idx` (`idconcierto` ASC),
  INDEX `fk_teatros_has_conciertos_teatros1_idx` (`idteatro` ASC),
  CONSTRAINT `fk_teatros_has_conciertos_teatros1`
    FOREIGN KEY (`idteatro`)
    REFERENCES `hazplanes`.`teatros` (`idteatro`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_teatros_has_conciertos_conciertos1`
    FOREIGN KEY (`idconcierto`)
    REFERENCES `hazplanes`.`conciertos` (`idconcierto`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hazplanes`.`teatros_has_entradas`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `hazplanes`.`teatros_has_entradas` ;

CREATE TABLE IF NOT EXISTS `hazplanes`.`teatros_has_entradas` (
  `idteatro` INT NOT NULL,
  `identradas` INT NOT NULL,
  PRIMARY KEY (`idteatro`, `identradas`),
  INDEX `fk_teatros_has_webentradas_webentradas1_idx` (`identradas` ASC),
  INDEX `fk_teatros_has_webentradas_teatros1_idx` (`idteatro` ASC),
  CONSTRAINT `fk_teatros_has_entradas_teatros1`
    FOREIGN KEY (`idteatro`)
    REFERENCES `hazplanes`.`teatros` (`idteatro`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_teatros_has_entradas_entradas1`
    FOREIGN KEY (`identradas`)
    REFERENCES `hazplanes`.`entradas` (`identradas`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hazplanes`.`obrasteatro`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `hazplanes`.`obrasteatro` ;

CREATE TABLE IF NOT EXISTS `hazplanes`.`obrasteatro` (
  `idobrateatro` BIGINT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(200) NULL,
  `duracion` SMALLINT(3) NULL,
  `genero` VARCHAR(255) NULL COMMENT 'Obras de teatro, monólogos...',
  `sinopsis` TEXT NULL,
  `video` VARCHAR(255) NULL,
  `poster` VARCHAR(50) NULL,
  `fanart` VARCHAR(50) NULL,
  `publicado` TINYINT(1) NULL DEFAULT 0,
  `menciones` TEXT NULL,
  `infantil` TINYINT(1) NULL DEFAULT 0,
  PRIMARY KEY (`idobrateatro`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hazplanes`.`teatros_has_peliculas`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `hazplanes`.`teatros_has_peliculas` ;

CREATE TABLE IF NOT EXISTS `hazplanes`.`teatros_has_peliculas` (
  `idteatro` INT NOT NULL,
  `idpelicula` BIGINT NOT NULL,
  `fecha` DATETIME NOT NULL,
  `3d` TINYINT(1) NULL DEFAULT 0,
  `vo` TINYINT(1) NULL DEFAULT 0,
  `urlcompra` VARCHAR(255) NULL,
  `precio` VARCHAR(10) NULL,
  `todoeldia` TINYINT(1) NULL DEFAULT 0,
  PRIMARY KEY (`idteatro`, `idpelicula`, `fecha`),
  INDEX `fk_teatros_has_peliculas_peliculas1_idx` (`idpelicula` ASC),
  INDEX `fk_teatros_has_peliculas_teatros1_idx` (`idteatro` ASC),
  CONSTRAINT `fk_teatros_has_peliculas_teatros1`
    FOREIGN KEY (`idteatro`)
    REFERENCES `hazplanes`.`teatros` (`idteatro`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_teatros_has_peliculas_peliculas1`
    FOREIGN KEY (`idpelicula`)
    REFERENCES `hazplanes`.`peliculas` (`idpelicula`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hazplanes`.`teatros_has_obrasteatro`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `hazplanes`.`teatros_has_obrasteatro` ;

CREATE TABLE IF NOT EXISTS `hazplanes`.`teatros_has_obrasteatro` (
  `idteatro` INT NOT NULL,
  `idobrateatro` BIGINT NOT NULL,
  `fecha` DATETIME NOT NULL,
  `urlcompra` VARCHAR(255) NULL,
  `precio` VARCHAR(10) NULL,
  `precioanticipada` VARCHAR(10) NULL,
  `todoeldia` TINYINT(1) NULL DEFAULT 0,
  PRIMARY KEY (`idteatro`, `idobrateatro`, `fecha`),
  INDEX `fk_teatros_has_obrasteatro_obrasteatro1_idx` (`idobrateatro` ASC),
  INDEX `fk_teatros_has_obrasteatro_teatros1_idx` (`idteatro` ASC),
  CONSTRAINT `fk_teatros_has_obrasteatro_teatros1`
    FOREIGN KEY (`idteatro`)
    REFERENCES `hazplanes`.`teatros` (`idteatro`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_teatros_has_obrasteatro_obrasteatro1`
    FOREIGN KEY (`idobrateatro`)
    REFERENCES `hazplanes`.`obrasteatro` (`idobrateatro`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hazplanes`.`locales_has_obrasteatro`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `hazplanes`.`locales_has_obrasteatro` ;

CREATE TABLE IF NOT EXISTS `hazplanes`.`locales_has_obrasteatro` (
  `idlocal` INT NOT NULL,
  `idobrateatro` BIGINT NOT NULL,
  `fecha` DATETIME NOT NULL,
  `urlcompra` VARCHAR(255) NULL,
  `precio` VARCHAR(10) NULL,
  `precioanticipada` VARCHAR(10) NULL,
  `todoeldia` TINYINT(1) NULL DEFAULT 0,
  PRIMARY KEY (`idlocal`, `idobrateatro`, `fecha`),
  INDEX `fk_locales_has_obrasteatro_obrasteatro1_idx` (`idobrateatro` ASC),
  INDEX `fk_locales_has_obrasteatro_locales1_idx` (`idlocal` ASC),
  CONSTRAINT `fk_locales_has_obrasteatro_locales1`
    FOREIGN KEY (`idlocal`)
    REFERENCES `hazplanes`.`locales` (`idlocal`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_locales_has_obrasteatro_obrasteatro1`
    FOREIGN KEY (`idobrateatro`)
    REFERENCES `hazplanes`.`obrasteatro` (`idobrateatro`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hazplanes`.`eventos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `hazplanes`.`eventos` ;

CREATE TABLE IF NOT EXISTS `hazplanes`.`eventos` (
  `ideventos` BIGINT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(200) NULL,
  `tipo` VARCHAR(45) NULL,
  `web` VARCHAR(255) NULL,
  `descripcion` TEXT NULL,
  `poster` VARCHAR(50) NULL,
  `fanart` VARCHAR(50) NULL,
  `publicado` TINYINT(1) NULL DEFAULT 0,
  `menciones` TEXT NULL,
  `infantil` TINYINT(1) NULL DEFAULT 0,
  PRIMARY KEY (`ideventos`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hazplanes`.`locales_has_eventos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `hazplanes`.`locales_has_eventos` ;

CREATE TABLE IF NOT EXISTS `hazplanes`.`locales_has_eventos` (
  `idlocal` INT NOT NULL,
  `ideventos` BIGINT NOT NULL,
  `fecha` DATETIME NOT NULL,
  `precio` VARCHAR(10) NULL,
  `precioanticipada` VARCHAR(10) NULL,
  PRIMARY KEY (`idlocal`, `ideventos`, `fecha`),
  INDEX `fk_locales_has_eventos_eventos1_idx` (`ideventos` ASC),
  INDEX `fk_locales_has_eventos_locales1_idx` (`idlocal` ASC),
  CONSTRAINT `fk_locales_has_eventos_locales1`
    FOREIGN KEY (`idlocal`)
    REFERENCES `hazplanes`.`locales` (`idlocal`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_locales_has_eventos_eventos1`
    FOREIGN KEY (`ideventos`)
    REFERENCES `hazplanes`.`eventos` (`ideventos`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hazplanes`.`teatros_has_eventos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `hazplanes`.`teatros_has_eventos` ;

CREATE TABLE IF NOT EXISTS `hazplanes`.`teatros_has_eventos` (
  `idteatro` INT NOT NULL,
  `ideventos` BIGINT NOT NULL,
  `fecha` DATETIME NOT NULL,
  `precio` VARCHAR(10) NULL,
  `precioanticipada` VARCHAR(10) NULL,
  `todoeldia` TINYINT(1) NULL DEFAULT 0,
  PRIMARY KEY (`idteatro`, `ideventos`, `fecha`),
  INDEX `fk_teatros_has_eventos_eventos1_idx` (`ideventos` ASC),
  INDEX `fk_teatros_has_eventos_teatros1_idx` (`idteatro` ASC),
  CONSTRAINT `fk_teatros_has_eventos_teatros1`
    FOREIGN KEY (`idteatro`)
    REFERENCES `hazplanes`.`teatros` (`idteatro`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_teatros_has_eventos_eventos1`
    FOREIGN KEY (`ideventos`)
    REFERENCES `hazplanes`.`eventos` (`ideventos`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hazplanes`.`deportes`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `hazplanes`.`deportes` ;

CREATE TABLE IF NOT EXISTS `hazplanes`.`deportes` (
  `iddeporte` BIGINT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(150) NOT NULL,
  `descripcion` TEXT NULL,
  `notas` TEXT NULL,
  `tipo` VARCHAR(100) NULL,
  `coordenadas` VARCHAR(255) NULL,
  `poster` VARCHAR(45) NULL,
  `publicado` TINYINT(1) NULL DEFAULT 0,
  `menciones` TEXT NULL,
  `infantil` TINYINT(1) NULL DEFAULT 0,
  PRIMARY KEY (`iddeporte`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hazplanes`.`museos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `hazplanes`.`museos` ;

CREATE TABLE IF NOT EXISTS `hazplanes`.`museos` (
  `idmuseo` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NOT NULL,
  `direccion` VARCHAR(255) NULL,
  `localidad` VARCHAR(15) NULL,
  `provincia` VARCHAR(15) NULL,
  `codigopostal` CHAR(5) NULL,
  `coordenadas` VARCHAR(255) NULL,
  `telefono` VARCHAR(13) NULL,
  `email` VARCHAR(255) NULL,
  `web` VARCHAR(255) NULL,
  `twitter` VARCHAR(255) NULL,
  `facebook` VARCHAR(255) NULL,
  `google` VARCHAR(255) NULL,
  `otrasocial` VARCHAR(255) NULL,
  `notas` TEXT NULL,
  `descripcion` TEXT NULL,
  `imagen` VARCHAR(45) NULL,
  `publicado` TINYINT(1) NULL DEFAULT 0,
  `menciones` TEXT NULL,
  PRIMARY KEY (`idmuseo`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hazplanes`.`exposiciones`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `hazplanes`.`exposiciones` ;

CREATE TABLE IF NOT EXISTS `hazplanes`.`exposiciones` (
  `idexposicion` BIGINT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(200) NOT NULL,
  `descripcion` TEXT NULL,
  `genero` VARCHAR(45) NULL,
  `poster` VARCHAR(50) NULL,
  `fanart` VARCHAR(50) NULL,
  `publicado` TINYINT(1) NULL DEFAULT 0,
  `menciones` TEXT NULL,
  `infantil` TINYINT(1) NULL DEFAULT 0,
  PRIMARY KEY (`idexposicion`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hazplanes`.`lugares_has_eventos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `hazplanes`.`lugares_has_eventos` ;

CREATE TABLE IF NOT EXISTS `hazplanes`.`lugares_has_eventos` (
  `ideventos` BIGINT NOT NULL,
  `fecha` DATETIME NOT NULL,
  `precio` VARCHAR(10) NULL,
  `precioanticipada` VARCHAR(10) NULL,
  `duracion` SMALLINT NULL,
  `lugar` VARCHAR(255) NOT NULL,
  `coordenadas` VARCHAR(255) NULL,
  `todoeldia` TINYINT(1) NULL DEFAULT 0,
  PRIMARY KEY (`ideventos`, `fecha`),
  CONSTRAINT `fk_lugares_has_eventos_eventos1`
    FOREIGN KEY (`ideventos`)
    REFERENCES `hazplanes`.`eventos` (`ideventos`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hazplanes`.`museos_has_exposiciones`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `hazplanes`.`museos_has_exposiciones` ;

CREATE TABLE IF NOT EXISTS `hazplanes`.`museos_has_exposiciones` (
  `idmuseo` INT NOT NULL,
  `idexposicion` BIGINT NOT NULL,
  `permanente` TINYINT(1) NOT NULL DEFAULT 0,
  `inicio` DATE NULL,
  `fin` DATE NULL,
  `precio` VARCHAR(10) NULL,
  PRIMARY KEY (`idmuseo`, `idexposicion`),
  INDEX `fk_museos_has_exposiciones_exposiciones1_idx` (`idexposicion` ASC),
  INDEX `fk_museos_has_exposiciones_museos1_idx` (`idmuseo` ASC),
  CONSTRAINT `fk_museos_has_exposiciones_museos1`
    FOREIGN KEY (`idmuseo`)
    REFERENCES `hazplanes`.`museos` (`idmuseo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_museos_has_exposiciones_exposiciones1`
    FOREIGN KEY (`idexposicion`)
    REFERENCES `hazplanes`.`exposiciones` (`idexposicion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hazplanes`.`horarios`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `hazplanes`.`horarios` ;

CREATE TABLE IF NOT EXISTS `hazplanes`.`horarios` (
  `idhorario` INT NOT NULL AUTO_INCREMENT,
  `idlugar` INT NOT NULL,
  `tipolugar` VARCHAR(45) NOT NULL,
  `lunes` VARCHAR(30) NULL,
  `martes` VARCHAR(30) NULL,
  `miercoles` VARCHAR(30) NULL,
  `jueves` VARCHAR(30) NULL,
  `viernes` VARCHAR(30) NULL,
  `sabado` VARCHAR(30) NULL,
  `domingo` VARCHAR(30) NULL,
  `cerrado` VARCHAR(14) NULL,
  `cerradosueltos` TEXT NULL,
  PRIMARY KEY (`idhorario`, `idlugar`, `tipolugar`),
  INDEX `fk_horarios_museos1` (`idlugar` ASC),
  CONSTRAINT `fk_horarios_museos1`
    FOREIGN KEY (`idlugar`)
    REFERENCES `hazplanes`.`museos` (`idmuseo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hazplanes`.`adjuntos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `hazplanes`.`adjuntos` ;

CREATE TABLE IF NOT EXISTS `hazplanes`.`adjuntos` (
  `idenlace` INT NOT NULL AUTO_INCREMENT,
  `idevento` BIGINT NOT NULL,
  `tipoevento` VARCHAR(45) NOT NULL,
  `name` VARCHAR(45) NULL,
  `url` VARCHAR(255) NULL,
  `tipoadjunto` VARCHAR(45) NULL,
  PRIMARY KEY (`idenlace`, `tipoevento`, `idevento`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hazplanes`.`pabellones`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `hazplanes`.`pabellones` ;

CREATE TABLE IF NOT EXISTS `hazplanes`.`pabellones` (
  `idpabellon` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(255) NOT NULL,
  `direccion` VARCHAR(255) NULL,
  `localidad` VARCHAR(15) NULL,
  `provincia` VARCHAR(15) NULL,
  `codigopostal` CHAR(5) NULL,
  `coordenadas` VARCHAR(255) NULL,
  `telefono` VARCHAR(13) NULL,
  `email` VARCHAR(255) NULL,
  `web` VARCHAR(255) NULL,
  `twitter` VARCHAR(255) NULL,
  `facebook` VARCHAR(255) NULL,
  `google` VARCHAR(255) NULL,
  `otrasocial` VARCHAR(255) NULL,
  `notas` TEXT NULL,
  `descripcion` TEXT NULL,
  `imagen` VARCHAR(45) NULL,
  `publicado` TINYINT(1) NULL DEFAULT 0,
  `menciones` TEXT NULL,
  PRIMARY KEY (`idpabellon`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hazplanes`.`pabellones_has_deportes`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `hazplanes`.`pabellones_has_deportes` ;

CREATE TABLE IF NOT EXISTS `hazplanes`.`pabellones_has_deportes` (
  `iddeporte` BIGINT NOT NULL,
  `fecha` DATETIME NOT NULL,
  `precio` VARCHAR(45) NULL,
  `duracion` SMALLINT NULL,
  `lugar` VARCHAR(255) NOT NULL,
  `todoeldia` TINYINT(1) NULL DEFAULT 0,
  PRIMARY KEY (`iddeporte`, `fecha`),
  INDEX `fk_pabellones_has_deportes_deportes1_idx` (`iddeporte` ASC),
  CONSTRAINT `fk_pabellones_has_deportes_deportes1`
    FOREIGN KEY (`iddeporte`)
    REFERENCES `hazplanes`.`deportes` (`iddeporte`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hazplanes`.`tarifas`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `hazplanes`.`tarifas` ;

CREATE TABLE IF NOT EXISTS `hazplanes`.`tarifas` (
  `idtarifas` INT NOT NULL AUTO_INCREMENT,
  `idcine` INT NOT NULL,
  `normal` SMALLINT NULL DEFAULT 0,
  `diaespectador` SMALLINT NULL DEFAULT 0,
  `estudiante` SMALLINT NULL DEFAULT 0,
  PRIMARY KEY (`idtarifas`, `idcine`),
  INDEX `fk_servicios_cines1_idx` (`idcine` ASC),
  CONSTRAINT `fk_servicios_cines10`
    FOREIGN KEY (`idcine`)
    REFERENCES `hazplanes`.`cines` (`idcine`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hazplanes`.`cursos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `hazplanes`.`cursos` ;

CREATE TABLE IF NOT EXISTS `hazplanes`.`cursos` (
  `idcurso` BIGINT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(150) NOT NULL,
  `descripcion` TEXT NULL,
  `notas` TEXT NULL,
  `tipo` VARCHAR(100) NULL,
  `coordenadas` VARCHAR(255) NULL,
  `poster` VARCHAR(45) NULL,
  `publicado` TINYINT(1) NULL DEFAULT 0,
  `menciones` TEXT NULL,
  `infantil` TINYINT(1) NULL DEFAULT 0,
  PRIMARY KEY (`idcurso`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hazplanes`.`formacion_has_cursos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `hazplanes`.`formacion_has_cursos` ;

CREATE TABLE IF NOT EXISTS `hazplanes`.`formacion_has_cursos` (
  `idcurso` BIGINT NOT NULL,
  `fecha` DATETIME NOT NULL,
  `precio` VARCHAR(45) NULL,
  `duracion` SMALLINT NULL,
  `lugar` VARCHAR(255) NOT NULL,
  `todoeldia` TINYINT(1) NULL DEFAULT 0,
  PRIMARY KEY (`idcurso`, `fecha`),
  CONSTRAINT `fk_formacion_has_cursos_cursos1`
    FOREIGN KEY (`idcurso`)
    REFERENCES `hazplanes`.`cursos` (`idcurso`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
